﻿using UnityEngine.Audio;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;

    public static AudioManager instance;
    private string m_Scene;

    public int nTheme;

    private CameraGlitch CG;

    public bool T;

    // Start is called before the first frame update
    void Awake()
    {
        
        m_Scene = SceneManager.GetActiveScene().name;

        /*if (m_Scene == "MainMenu Android")
        {
            Destroy(gameObject);


            Debug.Log("MainMenu android");
                return;
        }
        else
        {*/

            if (instance == null)
            {

                instance = this;
            }

            else
            {
                Destroy(gameObject);
                return;
            }

          
        //}


        

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();

            s.source.clip = s.clip; 

            s.source.outputAudioMixerGroup = s.outputAudioMixerGroup;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;

            s.source.loop = s.loop;
        }

        CG = FindObjectOfType<CameraGlitch>();

        T = CG.CGT;
    }

    void Start()
    {

        if (!T)
        {
            Play("Theme");
        }
        else if(T)
        {
            Play("ThemeF1");
            Debug.Log("played F");
        }
        else if(CG.NT == 2)
        {
            Play("ThemeF2");
        }
        else if (CG.NT == 3)
        {
            Play("ThemeF3");
        }
            
       

        
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        

        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }

        if (PauseMenu.GameIsPaused)
        {
            s.source.pitch *= 0.5f;
        }

        s.source.Play();

        T = CG.CGT;
    }
}
