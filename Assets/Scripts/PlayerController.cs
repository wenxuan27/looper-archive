﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public Rigidbody2D rb;
    public float movespeed;
    public bool moveright;
    public bool moveleft;
    public bool moveup;
    public bool movedown;

    public float movevelocity = 10f;
    public float movevelocity2 = 7f;
    // These variables controls the movement of the player


    public bool jump;
    public float jumpheight;

    public Transform RightWallCheck;
    public Transform LeftWallCheck;
    public float WallCheckRadius;
    public LayerMask whatIsWall;
    public bool TouchingRightWall;
    private bool TouchingLeftWall;
    // These are the variables to check if the player is touching either of the wall.

    public Transform startPosition;

    public int Shard;

    private Animator anim;


    public float fallSpeed;






    public int levelIndex;


    private GameManager gameManager;

    public Vector2 position;

    private SFXManager SFXM;

    private float playerX;

    private float playerY;

    static public bool pauseCheck = true;


    public bool isPaused;


    private Vector3 startDragPosition;
    private Vector3 endDragPosition;
    private Vector3 currentPositon;
    private Vector3 worldPosition;
    private Vector3 lastPositon;

    public float fallAcc;

    private GameObject DeathParticle;
    public GameObject DeathEffect;

    private bool moveType1 = false;
    private bool moveType2 = true;
    private bool moveType3 = false;

    public Joystick joystick;
    private float horizontalMove;
    private float verticalMove;

    public bool cointMagnetOn = false;

    public bool vibrateM;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();// This sets rb as the Rigidbody2D component


        anim = GetComponent<Animator>();// This sets anim as the Animator component




        levelIndex = SceneManager.GetActiveScene().buildIndex;

        gameManager = FindObjectOfType<GameManager>();

        SFXM = FindObjectOfType<SFXManager>();

        Shard = PlayerPrefs.GetInt("ShardScore", 0);


    }

    void FixedUpdate()
    {


        TouchingRightWall = Physics2D.OverlapCircle(RightWallCheck.position, WallCheckRadius, whatIsWall); // This sets TouchingRightWall to true if the RightWallCheck is touching the Right Wall
        TouchingLeftWall = Physics2D.OverlapCircle(LeftWallCheck.position, WallCheckRadius, whatIsWall); // This sets TouchingLeftWall to true if the LeftWallCheck is touching the Left Wall.
    }


    // Update is called once per frame
    void Update()
    {
        Shard = PlayerPrefs.GetInt("ShardScore", 0);

        playerX = gameObject.transform.position.x;

        playerY = gameObject.transform.position.y;


        //rb.velocity = new Vector2(rb.velocity.x, -jumpheight);

        rb.velocity = new Vector2(rb.velocity.x, fallSpeed + 0.2f);
        /*
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-movespeed, rb.velocity.y);// This is for moving left using the left arrow

            moveleft = true;
            moveright = false;

            anim.SetBool("MoveRight", false);
            anim.SetBool("MoveLeft", true);
        }



        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(movespeed, rb.velocity.y);// This is for moving right using the right arrow

            moveright = true;
            moveleft = false;

            anim.SetBool("MoveRight", true);
            anim.SetBool("MoveLeft", false);

        }
        */
        horizontalMove = Input.GetAxis("Horizontal");
        verticalMove = Input.GetAxis("Vertical");

       // horizontalMove = joystick.Horizontal;
        //verticalMove = joystick.Vertical;

        rb.velocity = new Vector2(horizontalMove * movespeed, verticalMove* movespeed + fallSpeed + 0.2f);

        /*
        if (Input.GetKey(KeyCode.UpArrow))
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpheight);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y - jumpheight);


        }
        */



        
        if (!isPaused) {
            /*
            if (Input.touchCount > 0)
            {

                Touch touch = Input.GetTouch(0);

                Vector3 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
                touchPosition.z = 0f;
                transform.position = touchPosition;
            }

            if (Input.GetMouseButton(0))
            {

                Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                clickPosition.z = 0f;
                transform.position = clickPosition;
            }

            */

            /*
             * 
             
                if (Input.touchCount > 0)
                {

                    Touch touch = Input.GetTouch(0);

                    Vector3 screenPosition = touch.position;
                    Vector3 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
                    touchPosition.z = 0f;

                    if (!(screenPosition.x > 270 & screenPosition.y > 540)) { 
                    rb.velocity = new Vector2((touchPosition.x - playerX) * movevelocity, rb.velocity.y + (touchPosition.y - playerY) * movevelocity);
                    }


                if (rb.velocity.x > 0.5)
                    {
                        moveright = true;
                        moveleft = false;

                        anim.SetBool("MoveRight", true);
                        anim.SetBool("MoveLeft", false);
                    }

                    if (rb.velocity.x < 0.5)
                    {
                        moveleft = true;
                        moveright = false;

                        anim.SetBool("MoveRight", false);
                        anim.SetBool("MoveLeft", true);
                    }

                }
                */


            if (moveType2)
            {

                if (Input.GetMouseButton(0))
                {
                    Vector3 screenPosition = Input.mousePosition;
                    Vector3 clickPosition = Camera.main.ScreenToWorldPoint(screenPosition);

                    clickPosition.z = 0f;

                    Vector3 viewPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);



                    if (!(viewPosition.x > 0.7 & viewPosition.y > 0.85))
                    {
                        rb.velocity = new Vector2((clickPosition.x - playerX) * movevelocity, rb.velocity.y + (clickPosition.y - playerY) * movevelocity);
                    }
                }
            }


            if (moveType1)
            {

                Vector3 viewPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);

                if (!(viewPosition.x > 0.75 & viewPosition.y > 0.9))
                {
                    worldPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);

                    if (Input.GetMouseButtonDown(0))
                    {
                        StartDrag(worldPosition);
                        // StartCoroutine(onWait(0.01f));


                    }
                    else if (Input.GetMouseButton(0))
                    {

                        ContinueDrag(worldPosition);
                    }

                }

            }

            if (moveType3)
            {

                if (Input.GetMouseButton(0))
                {
                    Vector3 screenPosition = Input.mousePosition;
                    Vector3 clickPosition = Camera.main.ScreenToWorldPoint(screenPosition);

                    clickPosition.z = 0f;

                    Vector3 viewPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);

                    horizontalMove = joystick.Horizontal;
                    verticalMove = joystick.Vertical;

                    if (!(viewPosition.x > 0.7 & viewPosition.y > 0.85))
                    {
                        rb.velocity = new Vector2((horizontalMove) * movevelocity2, rb.velocity.y + (verticalMove) * movevelocity2);
                    }
                }
            }



            if (rb.velocity.x > 0.5)
            {
                moveright = true;
                moveleft = false;

                anim.SetBool("MoveRight", true);
                anim.SetBool("MoveLeft", false);
            }

            if (rb.velocity.x < 0.5)
            {
                moveleft = true;
                moveright = false;

                anim.SetBool("MoveRight", false);
                anim.SetBool("MoveLeft", true);
            }

            if (rb.velocity.y > fallSpeed)
            {
                moveup = true;
                movedown = false;

            }

            if (rb.velocity.y < fallSpeed)
            {
                movedown = true;
                moveup = false;

            }



        }



        if (TouchingRightWall || TouchingLeftWall)
        {
            anim.SetBool("TouchWall", true);// This sets the parameter "TouchWall" in animator to true when either of the player's wallcheck is touching the wall.

            SFXM.Play("Bounce");

            VibrateMobile();
        }
        else
        {
            anim.SetBool("TouchWall", false);
        }// This sets the parameter "TouchWall" in animator back to false when the player is not touching the wall anymore.





        if (rb.velocity.x < 0.5 && rb.velocity.x > -0.5)
        {
            moveleft = false;
            moveright = false;

            anim.SetBool("MoveRight", false);
            anim.SetBool("MoveLeft", false);
        }
    

    }

    private void VibrateMobile()
    {
        if(vibrateM)
        Handheld.Vibrate();
    }

    public bool randomBoolean()
{
        return (Random.Range(0f, 1f) >= 0.5f);

        
    }

    public void Death()
    {
        rb.velocity = Vector3.zero;

        DeathParticle = Instantiate(DeathEffect, gameObject.transform.position, transform.rotation);
        DeathParticle.transform.parent = gameObject.transform;

        StartCoroutine(onWait(1f));

        //gameManager.EndGame();

        

    }

private void ContinueDrag(Vector3 worldPosition)
{
    endDragPosition = worldPosition;

        Vector3 direction = endDragPosition - startDragPosition;
        direction.Normalize();
        

        // launchPreview.SetEndPoint(transform.position - direction);

        rb.velocity = new Vector2((direction.x) * movevelocity2, fallSpeed + (direction.y) * movevelocity2);

    }


    private void StartDrag(Vector3 worldPosition)
{
    startDragPosition = worldPosition;

   // launchPreview.SetStartPoint(transform.position);
}

    IEnumerator onWait(float sec)
    {
        yield return new WaitForSeconds(sec);

        
    }

    public void ChangeMoveTo1()
    {
        moveType1 = true;
        moveType2 = false;
    }

    public void ChangeMoveTo2()
    {
        moveType2 = true;
        moveType1 = false;
    }



}






