﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Highscore : MonoBehaviour
{
    public Score1 S1;

    public TextMeshProUGUI scoreCounter;
    public TextMeshProUGUI highScore;

    public PlayerController player;

    
    public TextMeshProUGUI ShardScore;




    // Start is called before the first frame update
    void Start()
    {
        S1 = scoreCounter.GetComponent<Score1>();
        highScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();

        

        FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        SetScore();

        SetShard();

        
    }

    public void SetScore()
    {
        if(S1.score1 > PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore", S1.score1);
            highScore.text = S1.score1.ToString();
        }
    }

    public void SetShard()
    {
        PlayerPrefs.SetInt("ShardScore", player.Shard);
        
    }
}
