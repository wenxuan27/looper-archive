﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCounter : MonoBehaviour
{

    public int CyclicplayedGameTimes;
    public int playedGameTimes;

    private UnityAdsTest UAT;

    private GameManager GM;
    private PauseMenu PM;



    // Start is called before the first frame update
    void Start()
    {
        UAT = FindObjectOfType<UnityAdsTest>();

        GM = FindObjectOfType<GameManager>();
        PM = FindObjectOfType<PauseMenu>();

    }

    // Update is called once per frame
    void Update()
    {
        if(CyclicplayedGameTimes >= 4)
        {

            //UAT.PlayAd();
            CyclicplayedGameTimes = 0;
        }
    }
}
