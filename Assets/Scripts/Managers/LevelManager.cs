﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{

    public GameObject currentCheckpoint;
    private PlayerController player;

    public GameObject deathParticle;
    public GameObject respawnParticle;

    public int pointPenaltyOnDeath;



    public float respawnDelay;

    private float gravityStore;

    private CameraFollow cam;

   // public HealthManager healthManager;


    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();

        cam = FindObjectOfType<CameraFollow>();

       // healthManager = FindObjectOfType<HealthManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void RespawnPlayer()
    {
        StartCoroutine("RespawnPlayerCo");
    }



    public IEnumerator RespawnPlayerCo()
    {

        Instantiate(deathParticle, player.transform.position, player.transform.rotation);
        player.enabled = false;
        player.GetComponent<Renderer>().enabled = false;

        cam.isMoving = false;


        //gravityStore = player.rb.gravityScale;
        //player.rb.gravityScale = 0f; 

        player.rb.velocity = Vector2.zero;


        //ScoreManager.AddPoints(-pointPenaltyOnDeath);
        Debug.Log("Player Respawn");


        yield return new WaitForSeconds(respawnDelay);

        //player.rb.gravityScale = gravityStore;
        player.enabled = true;
        player.GetComponent<Renderer>().enabled = true;

       // healthManager.FullHealth();

        player.transform.position = currentCheckpoint.transform.position;

       // player.knockbackCount = 0;

        //healthManager.isDead = false;

        cam.isMoving = true;

        Instantiate(respawnParticle, currentCheckpoint.transform.position, currentCheckpoint.transform.rotation);
    }
}
