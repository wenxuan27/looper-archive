﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraLifeSimple : MonoBehaviour
{

    public int nExtraLife;

    // Start is called before the first frame update
    void Start()
    {
        nExtraLife = PlayerPrefs.GetInt("ExtraLivesScore");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddExtraLife()
    {

        nExtraLife++;
        PlayerPrefs.SetInt("ExtraLivesScore", nExtraLife);
        PlayerPrefs.SetInt("ShardScore", PlayerPrefs.GetInt("ShardScore", 0)-100);
    }
}
