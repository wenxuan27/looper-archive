﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCCC : MonoBehaviour
{

    public ParticleSystem.MinMaxGradient startColor;
    public bool isCrazyColorChanging;

    private void Awake()
    {
        startColor = GetComponent<ParticleSystem.MinMaxGradient>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isCrazyColorChanging)
        {

            Color newColor = new Color(Random.value, Random.value, Random.value);


            startColor = newColor;

            Debug.Log("flashin");

        }
    }
}
