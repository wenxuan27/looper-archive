﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public GameObject Player;

    public PlayerController PC;

    private SFXManager SFXM;

    private GameManager GM;

    public Color mycol;

    Color currentcolor;
    Color nextcolor;

    public bool vibrateM;

    public ExtraLifeSimple ELS;

    private SpriteRenderer playerSR;

    // Start is called before the first frame update
    void Start()
    {
        SFXM = FindObjectOfType<SFXManager>();

        GM = FindObjectOfType<GameManager>();

        PC = FindObjectOfType<PlayerController>();

        ELS = FindObjectOfType<ExtraLifeSimple>();

        playerSR = Player.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {


    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            //Player.GetComponent<PlayerController>().Death();
            

            SFXM.Play("Death");

            VibrateMobile();

            

            //GM.EndGame();

            if (PlayerPrefs.GetInt("ExtraLivesScore", 0) <= 0 || ELS.nExtraLife <= 0) {
                PC.Death();
                StartCoroutine(OnWaitForDeath(0.1f));

                playerSR.enabled = false;

            }
            else
            {
                ELS.nExtraLife -= 1;
                PlayerPrefs.SetInt("ExtraLivesScore", ELS.nExtraLife);
            }

        }

    }

    IEnumerator OnWaitForDeath(float sec)
    {
        yield return new WaitForSeconds(sec);

        GM.EndGame();
    }

    private void VibrateMobile()
    {
        if (vibrateM)
            Handheld.Vibrate();
    }

}
