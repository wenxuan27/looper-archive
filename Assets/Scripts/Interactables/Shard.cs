﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shard : MonoBehaviour
{
    // private Controls player;

    
    private SFXManager SFXM;

    private PlayerController player;

    public bool isCoinMagnetAffected;

    private Rigidbody2D rb;

    void Start()
    {
        SFXM = FindObjectOfType<SFXManager>();

        player = FindObjectOfType<PlayerController>();

        rb = GetComponent<Rigidbody2D>();

        isCoinMagnetAffected = false;
    }

    void Update()
    {
        isCoinMagnetAffected = player.cointMagnetOn;


        if (isCoinMagnetAffected)
        {
            Vector2 velocity = new Vector2(-((transform.position.x - player.transform.position.x)), -(transform.position.y - player.transform.position.y));
            velocity.Normalize();
            Vector2 realvelocity = new Vector2(8 * velocity.x, 8* velocity.y);
            rb.velocity = realvelocity;
        }
        else
        {
            rb.velocity = new Vector2(0,0);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "P")
        {
            Destroy(gameObject);
            SFXM.Play("Coin");
            player.Shard++;
            

        }
    }

    public void SetCoinMagnetAffect(bool Set)
    {
        isCoinMagnetAffected = Set;
    }

    
}
