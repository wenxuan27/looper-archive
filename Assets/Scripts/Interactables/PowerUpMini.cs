﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PowerUpMini : MonoBehaviour
{
    public float multiplier = 0.6f;

public float duration = 3f;

private GameObject MiniParticleEffect;
public GameObject pickupEffect;

private GameObject Player;
private PlayerController playerScript;

private SFXManager SFXM;
private GameManager GM;

private SpriteRenderer playerSR;



float cduration = 8f; // duration in seconds

private float t = 0; // lerp control variable

public Color playerBlankColor = Color.white;
    private bool playerIsFlashing;

    private void Start()
{
    Player = GameObject.FindWithTag("Player");

    playerScript = FindObjectOfType<PlayerController>();

    SFXM = FindObjectOfType<SFXManager>();

    GM = FindObjectOfType<GameManager>();

    playerSR = Player.GetComponent<SpriteRenderer>();


}

private void OnTriggerEnter2D(Collider2D other)
{
    if (other.CompareTag("Player"))
    {

        StartCoroutine(PickupMini(other));

    }
}



IEnumerator PickupMini(Collider2D player)
{

    MiniParticleEffect = Instantiate(pickupEffect, transform.position, transform.rotation);
    MiniParticleEffect.transform.parent = player.transform;

    player.transform.localScale *= multiplier;

    GetComponent<SpriteRenderer>().enabled = false;
    GetComponent<Collider2D>().enabled = false;

        UpdateplayerSR(1f, playerBlankColor, "green", null);

        yield return new WaitForSeconds(duration);

    Destroy(MiniParticleEffect);

        playerIsFlashing = true;

        yield return new WaitForSeconds(2f);

        playerSR.color = playerBlankColor;

        playerIsFlashing = false;

        player.transform.localScale *= 1 / multiplier;
    Destroy(gameObject);


}



public void UpdateplayerSR(float transitionTime, Color fadeColor, string c, Action func)
{
    // playerSR.color = fadeColor;
    StartCoroutine(UpdateplayerSRcolor(transitionTime, c, func));
}

private IEnumerator UpdateplayerSRcolor(float transitionTime, string color, Action func)
{
    float t = 0.0f;

    for (t = 0.0f; t <= 1; t += Time.deltaTime / transitionTime)
    {
        if (color == "red")
        {

            //playerSR.color = new Color(t, SR.color.g, SR.color.b);

            playerSR.color = Color.Lerp(Color.white, Color.red, t);

        }

        if (color == "green")
        {
            //playerSR.color = new Color(0, t, 0);
            playerSR.color = Color.Lerp(Color.white, Color.green, t);
        }

        if (color == "blue")
        {
            //playerSR.color = new Color(0, 0, t);
            playerSR.color = Color.Lerp(Color.white, Color.blue, t);
        }

        if (color == "white")
        {
            //playerSR.color = new Color(t, t, t);
            playerSR.color = Color.Lerp(Color.green, Color.white, t);

                Debug.Log("back to white");
        }

        if (color == "yellow")
        {
            //playerSR.color = new Color(t, t, 0);
            playerSR.color = Color.Lerp(Color.white, Color.yellow, t);
        }

        yield return null;


    }

    //SR.color.a = 1;

    func?.Invoke();
}

    private void PlayerFlash()
    {
        Color P = playerSR.color;
        P.a = Mathf.Cos(30 * GM.T);
        playerSR.color = P;
    }

    private void Update()
    {
        if (playerIsFlashing)
        {
            PlayerFlash();
        }
    }
}
