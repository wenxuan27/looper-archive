﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"






#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H



extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Platform_UnityAdsReady_m5B28B9883712F3085C4039FCBF096ECC45A3F4AB(char* ___placementId0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Platform_UnityAdsDidError_mE40E0CA3CB39142951BE4A1C6472F773346B2875(int64_t ___rawError0, char* ___message1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Platform_UnityAdsDidStart_mD7069998D65EF811F25DD1CBECBCE11553A6FBCD(char* ___placementId0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Platform_UnityAdsDidFinish_m4C3C6E95036CAEC44A2AAC40339DB7F84C7374C4(char* ___placementId0, int64_t ___rawShowResult1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Platform_UnityAdsDidInitiatePurchasingCommand_mB9F9754B6FE4774AB11C0939A302BA22AA2A1170(char* ___eventString0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Platform_UnityAdsPurchasingGetProductCatalog_mF2BACA8116B55E6C310F40A666A99A0584DC23E7();
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Platform_UnityAdsPurchasingGetPurchasingVersion_mDCCAA71912ECFE481ACFA17DAD0DDF727266B231();
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Platform_UnityAdsPurchasingInitialize_m198BBCB7FB070092E2EACE91C6A89E6571F0C422();
extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[8] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Platform_UnityAdsReady_m5B28B9883712F3085C4039FCBF096ECC45A3F4AB),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Platform_UnityAdsDidError_mE40E0CA3CB39142951BE4A1C6472F773346B2875),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Platform_UnityAdsDidStart_mD7069998D65EF811F25DD1CBECBCE11553A6FBCD),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Platform_UnityAdsDidFinish_m4C3C6E95036CAEC44A2AAC40339DB7F84C7374C4),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Platform_UnityAdsDidInitiatePurchasingCommand_mB9F9754B6FE4774AB11C0939A302BA22AA2A1170),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Platform_UnityAdsPurchasingGetProductCatalog_mF2BACA8116B55E6C310F40A666A99A0584DC23E7),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Platform_UnityAdsPurchasingGetPurchasingVersion_mDCCAA71912ECFE481ACFA17DAD0DDF727266B231),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Platform_UnityAdsPurchasingInitialize_m198BBCB7FB070092E2EACE91C6A89E6571F0C422),
};
