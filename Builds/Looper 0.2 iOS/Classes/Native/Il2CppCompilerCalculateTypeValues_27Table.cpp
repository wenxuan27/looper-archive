﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// AnalogGlitch
struct AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D;
// AudioManager
struct AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23;
// CameraFollow
struct CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142;
// CameraMovement
struct CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4;
// CoinMagnet
struct CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26;
// ColorLerp
struct ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2;
// CrazyColorChange
struct CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445;
// FadeManager
struct FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63;
// FlashingTMPro
struct FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941;
// GameCounter
struct GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F;
// GameManager
struct GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89;
// GlitchEffect
struct GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83;
// GlitchFx
struct GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A;
// IPurchasingEventSender
struct IPurchasingEventSender_t989C5DEC0837CEB1860CAA88BF32857BFC2C195E;
// Joystick
struct Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153;
// LevelLoader
struct LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343;
// LevelManager
struct LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D;
// Obstacle
struct Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E;
// PauseMenu
struct PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791;
// PlayerController
struct PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85;
// PowerUP
struct PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C;
// PowerUpInvicible
struct PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112;
// PowerUpMini
struct PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26;
// RandomColorLerp
struct RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666;
// SFXManager
struct SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD;
// Score1
struct Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119;
// Sound[]
struct SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<UnityEngine.Advertisements.ShowResult>
struct Action_1_tA383B4D00F0D86F49A41D08EFE2A0C3699DB1403;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs>
struct EventHandler_1_tFE307AA906B5E143091F4E06B873B19B0461A069;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.StringReader
struct StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TMP_Dropdown
struct TMP_Dropdown_t9FB6FD74CE2463D3A4C71A5A2A6FC29162B90353;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// UnityAdsTest
struct UnityAdsTest_tB995FE7BE7D9E21056A575559495BF17F06BDA96;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AsyncOperation
struct AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F;
// UnityEngine.Audio.AudioMixerGroup
struct AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9;
// UnityEngine.Audio.AudioMixerSnapshot
struct AudioMixerSnapshot_tD274CDCB45EE39E6CAE4A36F09FA5DDB65196174;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasGroup
struct CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90;
// UnityEngine.Collider2D
struct Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Gradient
struct Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A;
// UnityEngine.Gyroscope
struct Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC;
// UnityEngine.LineRenderer
struct LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RenderTexture
struct RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6;
// UnityEngine.Resolution[]
struct ResolutionU5BU5D_t7B0EB2421A00B22819A02FE474A7F747845BED9A;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// VariableJoystick
struct VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1;

struct Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A_marshaled_com;



#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_TDBFB93D6BB16759DEFE7EDDB05C6414159913063_H
#define U3CU3EC__DISPLAYCLASS5_0_TDBFB93D6BB16759DEFE7EDDB05C6414159913063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_tDBFB93D6BB16759DEFE7EDDB05C6414159913063  : public RuntimeObject
{
public:
	// System.String AudioManager/<>c__DisplayClass5_0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tDBFB93D6BB16759DEFE7EDDB05C6414159913063, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_TDBFB93D6BB16759DEFE7EDDB05C6414159913063_H
#ifndef U3CCOINMAGNETPICKUPU3ED__17_TD8524B7B36542781DE80094F3FDE21EBD72AF670_H
#define U3CCOINMAGNETPICKUPU3ED__17_TD8524B7B36542781DE80094F3FDE21EBD72AF670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoinMagnet/<CoinMagnetPickup>d__17
struct  U3CCoinMagnetPickupU3Ed__17_tD8524B7B36542781DE80094F3FDE21EBD72AF670  : public RuntimeObject
{
public:
	// System.Int32 CoinMagnet/<CoinMagnetPickup>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CoinMagnet/<CoinMagnetPickup>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// CoinMagnet CoinMagnet/<CoinMagnetPickup>d__17::<>4__this
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26 * ___U3CU3E4__this_2;
	// UnityEngine.Collider2D CoinMagnet/<CoinMagnetPickup>d__17::player
	Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * ___player_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCoinMagnetPickupU3Ed__17_tD8524B7B36542781DE80094F3FDE21EBD72AF670, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCoinMagnetPickupU3Ed__17_tD8524B7B36542781DE80094F3FDE21EBD72AF670, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCoinMagnetPickupU3Ed__17_tD8524B7B36542781DE80094F3FDE21EBD72AF670, ___U3CU3E4__this_2)); }
	inline CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(U3CCoinMagnetPickupU3Ed__17_tD8524B7B36542781DE80094F3FDE21EBD72AF670, ___player_3)); }
	inline Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * get_player_3() const { return ___player_3; }
	inline Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier((&___player_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOINMAGNETPICKUPU3ED__17_TD8524B7B36542781DE80094F3FDE21EBD72AF670_H
#ifndef U3CFADETOBLUEU3ED__11_T2A35DAEF14F0F1CE3186738A1FA2603E9E1E9827_H
#define U3CFADETOBLUEU3ED__11_T2A35DAEF14F0F1CE3186738A1FA2603E9E1E9827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorLerp/<FadeToBlue>d__11
struct  U3CFadeToBlueU3Ed__11_t2A35DAEF14F0F1CE3186738A1FA2603E9E1E9827  : public RuntimeObject
{
public:
	// System.Int32 ColorLerp/<FadeToBlue>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ColorLerp/<FadeToBlue>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ColorLerp ColorLerp/<FadeToBlue>d__11::<>4__this
	ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2 * ___U3CU3E4__this_2;
	// System.Single ColorLerp/<FadeToBlue>d__11::<i>5__2
	float ___U3CiU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFadeToBlueU3Ed__11_t2A35DAEF14F0F1CE3186738A1FA2603E9E1E9827, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFadeToBlueU3Ed__11_t2A35DAEF14F0F1CE3186738A1FA2603E9E1E9827, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFadeToBlueU3Ed__11_t2A35DAEF14F0F1CE3186738A1FA2603E9E1E9827, ___U3CU3E4__this_2)); }
	inline ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CFadeToBlueU3Ed__11_t2A35DAEF14F0F1CE3186738A1FA2603E9E1E9827, ___U3CiU3E5__2_3)); }
	inline float get_U3CiU3E5__2_3() const { return ___U3CiU3E5__2_3; }
	inline float* get_address_of_U3CiU3E5__2_3() { return &___U3CiU3E5__2_3; }
	inline void set_U3CiU3E5__2_3(float value)
	{
		___U3CiU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADETOBLUEU3ED__11_T2A35DAEF14F0F1CE3186738A1FA2603E9E1E9827_H
#ifndef U3CONWAITU3ED__13_T44C65DD445D849BFAC53EB0B13141D1C51DB238C_H
#define U3CONWAITU3ED__13_T44C65DD445D849BFAC53EB0B13141D1C51DB238C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorLerp/<OnWait>d__13
struct  U3COnWaitU3Ed__13_t44C65DD445D849BFAC53EB0B13141D1C51DB238C  : public RuntimeObject
{
public:
	// System.Int32 ColorLerp/<OnWait>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ColorLerp/<OnWait>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single ColorLerp/<OnWait>d__13::sec
	float ___sec_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3COnWaitU3Ed__13_t44C65DD445D849BFAC53EB0B13141D1C51DB238C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3COnWaitU3Ed__13_t44C65DD445D849BFAC53EB0B13141D1C51DB238C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_sec_2() { return static_cast<int32_t>(offsetof(U3COnWaitU3Ed__13_t44C65DD445D849BFAC53EB0B13141D1C51DB238C, ___sec_2)); }
	inline float get_sec_2() const { return ___sec_2; }
	inline float* get_address_of_sec_2() { return &___sec_2; }
	inline void set_sec_2(float value)
	{
		___sec_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONWAITU3ED__13_T44C65DD445D849BFAC53EB0B13141D1C51DB238C_H
#ifndef EXAMPLE_TB7AEFF417A3F2A1E30F8931285218D37BC4954CF_H
#define EXAMPLE_TB7AEFF417A3F2A1E30F8931285218D37BC4954CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Example
struct  Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE_TB7AEFF417A3F2A1E30F8931285218D37BC4954CF_H
#ifndef U3CUPDATEFADEINU3ED__10_T59D91E61872645E158D875325C09DDE83163DC77_H
#define U3CUPDATEFADEINU3ED__10_T59D91E61872645E158D875325C09DDE83163DC77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeManager/<UpdateFadeIn>d__10
struct  U3CUpdateFadeInU3Ed__10_t59D91E61872645E158D875325C09DDE83163DC77  : public RuntimeObject
{
public:
	// System.Int32 FadeManager/<UpdateFadeIn>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object FadeManager/<UpdateFadeIn>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// FadeManager FadeManager/<UpdateFadeIn>d__10::<>4__this
	FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63 * ___U3CU3E4__this_2;
	// System.Single FadeManager/<UpdateFadeIn>d__10::transitionTime
	float ___transitionTime_3;
	// System.Action FadeManager/<UpdateFadeIn>d__10::func
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___func_4;
	// System.Single FadeManager/<UpdateFadeIn>d__10::<t>5__2
	float ___U3CtU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateFadeInU3Ed__10_t59D91E61872645E158D875325C09DDE83163DC77, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateFadeInU3Ed__10_t59D91E61872645E158D875325C09DDE83163DC77, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateFadeInU3Ed__10_t59D91E61872645E158D875325C09DDE83163DC77, ___U3CU3E4__this_2)); }
	inline FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_transitionTime_3() { return static_cast<int32_t>(offsetof(U3CUpdateFadeInU3Ed__10_t59D91E61872645E158D875325C09DDE83163DC77, ___transitionTime_3)); }
	inline float get_transitionTime_3() const { return ___transitionTime_3; }
	inline float* get_address_of_transitionTime_3() { return &___transitionTime_3; }
	inline void set_transitionTime_3(float value)
	{
		___transitionTime_3 = value;
	}

	inline static int32_t get_offset_of_func_4() { return static_cast<int32_t>(offsetof(U3CUpdateFadeInU3Ed__10_t59D91E61872645E158D875325C09DDE83163DC77, ___func_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_func_4() const { return ___func_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_func_4() { return &___func_4; }
	inline void set_func_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___func_4 = value;
		Il2CppCodeGenWriteBarrier((&___func_4), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CUpdateFadeInU3Ed__10_t59D91E61872645E158D875325C09DDE83163DC77, ___U3CtU3E5__2_5)); }
	inline float get_U3CtU3E5__2_5() const { return ___U3CtU3E5__2_5; }
	inline float* get_address_of_U3CtU3E5__2_5() { return &___U3CtU3E5__2_5; }
	inline void set_U3CtU3E5__2_5(float value)
	{
		___U3CtU3E5__2_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEFADEINU3ED__10_T59D91E61872645E158D875325C09DDE83163DC77_H
#ifndef U3CUPDATEFADEOUTU3ED__14_T378395BE6BFA0E5893A07C37EAA07F88363A8161_H
#define U3CUPDATEFADEOUTU3ED__14_T378395BE6BFA0E5893A07C37EAA07F88363A8161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeManager/<UpdateFadeOut>d__14
struct  U3CUpdateFadeOutU3Ed__14_t378395BE6BFA0E5893A07C37EAA07F88363A8161  : public RuntimeObject
{
public:
	// System.Int32 FadeManager/<UpdateFadeOut>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object FadeManager/<UpdateFadeOut>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// FadeManager FadeManager/<UpdateFadeOut>d__14::<>4__this
	FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63 * ___U3CU3E4__this_2;
	// System.Single FadeManager/<UpdateFadeOut>d__14::transitionTime
	float ___transitionTime_3;
	// System.Action FadeManager/<UpdateFadeOut>d__14::func
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___func_4;
	// System.Single FadeManager/<UpdateFadeOut>d__14::<t>5__2
	float ___U3CtU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateFadeOutU3Ed__14_t378395BE6BFA0E5893A07C37EAA07F88363A8161, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateFadeOutU3Ed__14_t378395BE6BFA0E5893A07C37EAA07F88363A8161, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateFadeOutU3Ed__14_t378395BE6BFA0E5893A07C37EAA07F88363A8161, ___U3CU3E4__this_2)); }
	inline FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_transitionTime_3() { return static_cast<int32_t>(offsetof(U3CUpdateFadeOutU3Ed__14_t378395BE6BFA0E5893A07C37EAA07F88363A8161, ___transitionTime_3)); }
	inline float get_transitionTime_3() const { return ___transitionTime_3; }
	inline float* get_address_of_transitionTime_3() { return &___transitionTime_3; }
	inline void set_transitionTime_3(float value)
	{
		___transitionTime_3 = value;
	}

	inline static int32_t get_offset_of_func_4() { return static_cast<int32_t>(offsetof(U3CUpdateFadeOutU3Ed__14_t378395BE6BFA0E5893A07C37EAA07F88363A8161, ___func_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_func_4() const { return ___func_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_func_4() { return &___func_4; }
	inline void set_func_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___func_4 = value;
		Il2CppCodeGenWriteBarrier((&___func_4), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CUpdateFadeOutU3Ed__14_t378395BE6BFA0E5893A07C37EAA07F88363A8161, ___U3CtU3E5__2_5)); }
	inline float get_U3CtU3E5__2_5() const { return ___U3CtU3E5__2_5; }
	inline float* get_address_of_U3CtU3E5__2_5() { return &___U3CtU3E5__2_5; }
	inline void set_U3CtU3E5__2_5(float value)
	{
		___U3CtU3E5__2_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEFADEOUTU3ED__14_T378395BE6BFA0E5893A07C37EAA07F88363A8161_H
#ifndef U3CLOADASYNCHRONOUSLYU3ED__4_T2440E89200DF7A02FD1803A18D9569C7912479A0_H
#define U3CLOADASYNCHRONOUSLYU3ED__4_T2440E89200DF7A02FD1803A18D9569C7912479A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelLoader/<LoadAsynchronously>d__4
struct  U3CLoadAsynchronouslyU3Ed__4_t2440E89200DF7A02FD1803A18D9569C7912479A0  : public RuntimeObject
{
public:
	// System.Int32 LevelLoader/<LoadAsynchronously>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LevelLoader/<LoadAsynchronously>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 LevelLoader/<LoadAsynchronously>d__4::sceneIndex
	int32_t ___sceneIndex_2;
	// LevelLoader LevelLoader/<LoadAsynchronously>d__4::<>4__this
	LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343 * ___U3CU3E4__this_3;
	// UnityEngine.AsyncOperation LevelLoader/<LoadAsynchronously>d__4::<operation>5__2
	AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * ___U3CoperationU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ed__4_t2440E89200DF7A02FD1803A18D9569C7912479A0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ed__4_t2440E89200DF7A02FD1803A18D9569C7912479A0, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_sceneIndex_2() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ed__4_t2440E89200DF7A02FD1803A18D9569C7912479A0, ___sceneIndex_2)); }
	inline int32_t get_sceneIndex_2() const { return ___sceneIndex_2; }
	inline int32_t* get_address_of_sceneIndex_2() { return &___sceneIndex_2; }
	inline void set_sceneIndex_2(int32_t value)
	{
		___sceneIndex_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ed__4_t2440E89200DF7A02FD1803A18D9569C7912479A0, ___U3CU3E4__this_3)); }
	inline LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CoperationU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadAsynchronouslyU3Ed__4_t2440E89200DF7A02FD1803A18D9569C7912479A0, ___U3CoperationU3E5__2_4)); }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * get_U3CoperationU3E5__2_4() const { return ___U3CoperationU3E5__2_4; }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D ** get_address_of_U3CoperationU3E5__2_4() { return &___U3CoperationU3E5__2_4; }
	inline void set_U3CoperationU3E5__2_4(AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * value)
	{
		___U3CoperationU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoperationU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASYNCHRONOUSLYU3ED__4_T2440E89200DF7A02FD1803A18D9569C7912479A0_H
#ifndef U3CRESPAWNPLAYERCOU3ED__11_TA504E021A606B04BF0A6292CB74D8142ACC388E6_H
#define U3CRESPAWNPLAYERCOU3ED__11_TA504E021A606B04BF0A6292CB74D8142ACC388E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelManager/<RespawnPlayerCo>d__11
struct  U3CRespawnPlayerCoU3Ed__11_tA504E021A606B04BF0A6292CB74D8142ACC388E6  : public RuntimeObject
{
public:
	// System.Int32 LevelManager/<RespawnPlayerCo>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LevelManager/<RespawnPlayerCo>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LevelManager LevelManager/<RespawnPlayerCo>d__11::<>4__this
	LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRespawnPlayerCoU3Ed__11_tA504E021A606B04BF0A6292CB74D8142ACC388E6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRespawnPlayerCoU3Ed__11_tA504E021A606B04BF0A6292CB74D8142ACC388E6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CRespawnPlayerCoU3Ed__11_tA504E021A606B04BF0A6292CB74D8142ACC388E6, ___U3CU3E4__this_2)); }
	inline LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESPAWNPLAYERCOU3ED__11_TA504E021A606B04BF0A6292CB74D8142ACC388E6_H
#ifndef U3CONWAITU3ED__10_T40593F43BE2EB83D23E24877C98CAE6EAC008FA1_H
#define U3CONWAITU3ED__10_T40593F43BE2EB83D23E24877C98CAE6EAC008FA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Obstacle/<onWait>d__10
struct  U3ConWaitU3Ed__10_t40593F43BE2EB83D23E24877C98CAE6EAC008FA1  : public RuntimeObject
{
public:
	// System.Int32 Obstacle/<onWait>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Obstacle/<onWait>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single Obstacle/<onWait>d__10::sec
	float ___sec_2;
	// Obstacle Obstacle/<onWait>d__10::<>4__this
	Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3ConWaitU3Ed__10_t40593F43BE2EB83D23E24877C98CAE6EAC008FA1, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3ConWaitU3Ed__10_t40593F43BE2EB83D23E24877C98CAE6EAC008FA1, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_sec_2() { return static_cast<int32_t>(offsetof(U3ConWaitU3Ed__10_t40593F43BE2EB83D23E24877C98CAE6EAC008FA1, ___sec_2)); }
	inline float get_sec_2() const { return ___sec_2; }
	inline float* get_address_of_sec_2() { return &___sec_2; }
	inline void set_sec_2(float value)
	{
		___sec_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3ConWaitU3Ed__10_t40593F43BE2EB83D23E24877C98CAE6EAC008FA1, ___U3CU3E4__this_3)); }
	inline Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONWAITU3ED__10_T40593F43BE2EB83D23E24877C98CAE6EAC008FA1_H
#ifndef U3CONPAUSEU3ED__21_TF67253F1393DCC25954C5A6D351C6611E7F69551_H
#define U3CONPAUSEU3ED__21_TF67253F1393DCC25954C5A6D351C6611E7F69551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseMenu/<onPause>d__21
struct  U3ConPauseU3Ed__21_tF67253F1393DCC25954C5A6D351C6611E7F69551  : public RuntimeObject
{
public:
	// System.Int32 PauseMenu/<onPause>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PauseMenu/<onPause>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// PauseMenu PauseMenu/<onPause>d__21::<>4__this
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3ConPauseU3Ed__21_tF67253F1393DCC25954C5A6D351C6611E7F69551, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3ConPauseU3Ed__21_tF67253F1393DCC25954C5A6D351C6611E7F69551, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3ConPauseU3Ed__21_tF67253F1393DCC25954C5A6D351C6611E7F69551, ___U3CU3E4__this_2)); }
	inline PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONPAUSEU3ED__21_TF67253F1393DCC25954C5A6D351C6611E7F69551_H
#ifndef U3CONWAITU3ED__50_T81D18DD5B09E7E2A1733614448B37B2F33623C67_H
#define U3CONWAITU3ED__50_T81D18DD5B09E7E2A1733614448B37B2F33623C67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerController/<onWait>d__50
struct  U3ConWaitU3Ed__50_t81D18DD5B09E7E2A1733614448B37B2F33623C67  : public RuntimeObject
{
public:
	// System.Int32 PlayerController/<onWait>d__50::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PlayerController/<onWait>d__50::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single PlayerController/<onWait>d__50::sec
	float ___sec_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3ConWaitU3Ed__50_t81D18DD5B09E7E2A1733614448B37B2F33623C67, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3ConWaitU3Ed__50_t81D18DD5B09E7E2A1733614448B37B2F33623C67, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_sec_2() { return static_cast<int32_t>(offsetof(U3ConWaitU3Ed__50_t81D18DD5B09E7E2A1733614448B37B2F33623C67, ___sec_2)); }
	inline float get_sec_2() const { return ___sec_2; }
	inline float* get_address_of_sec_2() { return &___sec_2; }
	inline void set_sec_2(float value)
	{
		___sec_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONWAITU3ED__50_T81D18DD5B09E7E2A1733614448B37B2F33623C67_H
#ifndef U3CPICKUPSPEEDU3ED__19_TA800DD001CB87D6A9A63B9EE3A322F9191B3B575_H
#define U3CPICKUPSPEEDU3ED__19_TA800DD001CB87D6A9A63B9EE3A322F9191B3B575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PowerUP/<PickupSpeed>d__19
struct  U3CPickupSpeedU3Ed__19_tA800DD001CB87D6A9A63B9EE3A322F9191B3B575  : public RuntimeObject
{
public:
	// System.Int32 PowerUP/<PickupSpeed>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PowerUP/<PickupSpeed>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// PowerUP PowerUP/<PickupSpeed>d__19::<>4__this
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C * ___U3CU3E4__this_2;
	// UnityEngine.Collider2D PowerUP/<PickupSpeed>d__19::player
	Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * ___player_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPickupSpeedU3Ed__19_tA800DD001CB87D6A9A63B9EE3A322F9191B3B575, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPickupSpeedU3Ed__19_tA800DD001CB87D6A9A63B9EE3A322F9191B3B575, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CPickupSpeedU3Ed__19_tA800DD001CB87D6A9A63B9EE3A322F9191B3B575, ___U3CU3E4__this_2)); }
	inline PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(U3CPickupSpeedU3Ed__19_tA800DD001CB87D6A9A63B9EE3A322F9191B3B575, ___player_3)); }
	inline Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * get_player_3() const { return ___player_3; }
	inline Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier((&___player_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPICKUPSPEEDU3ED__19_TA800DD001CB87D6A9A63B9EE3A322F9191B3B575_H
#ifndef U3CUPDATEPLAYERSRCOLORU3ED__21_TB93250B2D169A83011CF5E039BA59363896FA316_H
#define U3CUPDATEPLAYERSRCOLORU3ED__21_TB93250B2D169A83011CF5E039BA59363896FA316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PowerUP/<UpdateplayerSRcolor>d__21
struct  U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316  : public RuntimeObject
{
public:
	// System.Int32 PowerUP/<UpdateplayerSRcolor>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PowerUP/<UpdateplayerSRcolor>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String PowerUP/<UpdateplayerSRcolor>d__21::color
	String_t* ___color_2;
	// PowerUP PowerUP/<UpdateplayerSRcolor>d__21::<>4__this
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C * ___U3CU3E4__this_3;
	// System.Single PowerUP/<UpdateplayerSRcolor>d__21::transitionTime
	float ___transitionTime_4;
	// System.Action PowerUP/<UpdateplayerSRcolor>d__21::func
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___func_5;
	// System.Single PowerUP/<UpdateplayerSRcolor>d__21::<t>5__2
	float ___U3CtU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316, ___color_2)); }
	inline String_t* get_color_2() const { return ___color_2; }
	inline String_t** get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(String_t* value)
	{
		___color_2 = value;
		Il2CppCodeGenWriteBarrier((&___color_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316, ___U3CU3E4__this_3)); }
	inline PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_transitionTime_4() { return static_cast<int32_t>(offsetof(U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316, ___transitionTime_4)); }
	inline float get_transitionTime_4() const { return ___transitionTime_4; }
	inline float* get_address_of_transitionTime_4() { return &___transitionTime_4; }
	inline void set_transitionTime_4(float value)
	{
		___transitionTime_4 = value;
	}

	inline static int32_t get_offset_of_func_5() { return static_cast<int32_t>(offsetof(U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316, ___func_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_func_5() const { return ___func_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_func_5() { return &___func_5; }
	inline void set_func_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___func_5 = value;
		Il2CppCodeGenWriteBarrier((&___func_5), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316, ___U3CtU3E5__2_6)); }
	inline float get_U3CtU3E5__2_6() const { return ___U3CtU3E5__2_6; }
	inline float* get_address_of_U3CtU3E5__2_6() { return &___U3CtU3E5__2_6; }
	inline void set_U3CtU3E5__2_6(float value)
	{
		___U3CtU3E5__2_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEPLAYERSRCOLORU3ED__21_TB93250B2D169A83011CF5E039BA59363896FA316_H
#ifndef U3CPICKUPINVICIBLEU3ED__16_TB872EDB503901CEBC9AC9477300A9978C925D9BB_H
#define U3CPICKUPINVICIBLEU3ED__16_TB872EDB503901CEBC9AC9477300A9978C925D9BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PowerUpInvicible/<PickupInvicible>d__16
struct  U3CPickupInvicibleU3Ed__16_tB872EDB503901CEBC9AC9477300A9978C925D9BB  : public RuntimeObject
{
public:
	// System.Int32 PowerUpInvicible/<PickupInvicible>d__16::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PowerUpInvicible/<PickupInvicible>d__16::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// PowerUpInvicible PowerUpInvicible/<PickupInvicible>d__16::<>4__this
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112 * ___U3CU3E4__this_2;
	// UnityEngine.Collider2D PowerUpInvicible/<PickupInvicible>d__16::player
	Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * ___player_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPickupInvicibleU3Ed__16_tB872EDB503901CEBC9AC9477300A9978C925D9BB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPickupInvicibleU3Ed__16_tB872EDB503901CEBC9AC9477300A9978C925D9BB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CPickupInvicibleU3Ed__16_tB872EDB503901CEBC9AC9477300A9978C925D9BB, ___U3CU3E4__this_2)); }
	inline PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(U3CPickupInvicibleU3Ed__16_tB872EDB503901CEBC9AC9477300A9978C925D9BB, ___player_3)); }
	inline Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * get_player_3() const { return ___player_3; }
	inline Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier((&___player_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPICKUPINVICIBLEU3ED__16_TB872EDB503901CEBC9AC9477300A9978C925D9BB_H
#ifndef U3CPICKUPMINIU3ED__15_T614D69EDEC901644A4F87BB119474E3312E6A250_H
#define U3CPICKUPMINIU3ED__15_T614D69EDEC901644A4F87BB119474E3312E6A250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PowerUpMini/<PickupMini>d__15
struct  U3CPickupMiniU3Ed__15_t614D69EDEC901644A4F87BB119474E3312E6A250  : public RuntimeObject
{
public:
	// System.Int32 PowerUpMini/<PickupMini>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PowerUpMini/<PickupMini>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// PowerUpMini PowerUpMini/<PickupMini>d__15::<>4__this
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26 * ___U3CU3E4__this_2;
	// UnityEngine.Collider2D PowerUpMini/<PickupMini>d__15::player
	Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * ___player_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPickupMiniU3Ed__15_t614D69EDEC901644A4F87BB119474E3312E6A250, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPickupMiniU3Ed__15_t614D69EDEC901644A4F87BB119474E3312E6A250, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CPickupMiniU3Ed__15_t614D69EDEC901644A4F87BB119474E3312E6A250, ___U3CU3E4__this_2)); }
	inline PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(U3CPickupMiniU3Ed__15_t614D69EDEC901644A4F87BB119474E3312E6A250, ___player_3)); }
	inline Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * get_player_3() const { return ___player_3; }
	inline Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier((&___player_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPICKUPMINIU3ED__15_T614D69EDEC901644A4F87BB119474E3312E6A250_H
#ifndef U3CUPDATEPLAYERSRCOLORU3ED__17_T4A8002D750E1D34C9F3EA751E6C9F9F2357047EC_H
#define U3CUPDATEPLAYERSRCOLORU3ED__17_T4A8002D750E1D34C9F3EA751E6C9F9F2357047EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PowerUpMini/<UpdateplayerSRcolor>d__17
struct  U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC  : public RuntimeObject
{
public:
	// System.Int32 PowerUpMini/<UpdateplayerSRcolor>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PowerUpMini/<UpdateplayerSRcolor>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String PowerUpMini/<UpdateplayerSRcolor>d__17::color
	String_t* ___color_2;
	// PowerUpMini PowerUpMini/<UpdateplayerSRcolor>d__17::<>4__this
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26 * ___U3CU3E4__this_3;
	// System.Single PowerUpMini/<UpdateplayerSRcolor>d__17::transitionTime
	float ___transitionTime_4;
	// System.Action PowerUpMini/<UpdateplayerSRcolor>d__17::func
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___func_5;
	// System.Single PowerUpMini/<UpdateplayerSRcolor>d__17::<t>5__2
	float ___U3CtU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC, ___color_2)); }
	inline String_t* get_color_2() const { return ___color_2; }
	inline String_t** get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(String_t* value)
	{
		___color_2 = value;
		Il2CppCodeGenWriteBarrier((&___color_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC, ___U3CU3E4__this_3)); }
	inline PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_transitionTime_4() { return static_cast<int32_t>(offsetof(U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC, ___transitionTime_4)); }
	inline float get_transitionTime_4() const { return ___transitionTime_4; }
	inline float* get_address_of_transitionTime_4() { return &___transitionTime_4; }
	inline void set_transitionTime_4(float value)
	{
		___transitionTime_4 = value;
	}

	inline static int32_t get_offset_of_func_5() { return static_cast<int32_t>(offsetof(U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC, ___func_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_func_5() const { return ___func_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_func_5() { return &___func_5; }
	inline void set_func_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___func_5 = value;
		Il2CppCodeGenWriteBarrier((&___func_5), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC, ___U3CtU3E5__2_6)); }
	inline float get_U3CtU3E5__2_6() const { return ___U3CtU3E5__2_6; }
	inline float* get_address_of_U3CtU3E5__2_6() { return &___U3CtU3E5__2_6; }
	inline void set_U3CtU3E5__2_6(float value)
	{
		___U3CtU3E5__2_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEPLAYERSRCOLORU3ED__17_T4A8002D750E1D34C9F3EA751E6C9F9F2357047EC_H
#ifndef U3CUPDATECOLORU3ED__20_T3FCA7F437809572BBD84AAA1530D9B56D2F6F762_H
#define U3CUPDATECOLORU3ED__20_T3FCA7F437809572BBD84AAA1530D9B56D2F6F762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RandomColorLerp/<UpdateColor>d__20
struct  U3CUpdateColorU3Ed__20_t3FCA7F437809572BBD84AAA1530D9B56D2F6F762  : public RuntimeObject
{
public:
	// System.Int32 RandomColorLerp/<UpdateColor>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RandomColorLerp/<UpdateColor>d__20::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RandomColorLerp RandomColorLerp/<UpdateColor>d__20::<>4__this
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666 * ___U3CU3E4__this_2;
	// System.Action RandomColorLerp/<UpdateColor>d__20::func
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___func_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateColorU3Ed__20_t3FCA7F437809572BBD84AAA1530D9B56D2F6F762, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateColorU3Ed__20_t3FCA7F437809572BBD84AAA1530D9B56D2F6F762, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateColorU3Ed__20_t3FCA7F437809572BBD84AAA1530D9B56D2F6F762, ___U3CU3E4__this_2)); }
	inline RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_func_3() { return static_cast<int32_t>(offsetof(U3CUpdateColorU3Ed__20_t3FCA7F437809572BBD84AAA1530D9B56D2F6F762, ___func_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_func_3() const { return ___func_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_func_3() { return &___func_3; }
	inline void set_func_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___func_3 = value;
		Il2CppCodeGenWriteBarrier((&___func_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATECOLORU3ED__20_T3FCA7F437809572BBD84AAA1530D9B56D2F6F762_H
#ifndef U3CUPDATEFADEREDU3ED__24_TB4F13466E870A0C10E65D2B528B2786D59F016C2_H
#define U3CUPDATEFADEREDU3ED__24_TB4F13466E870A0C10E65D2B528B2786D59F016C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RandomColorLerp/<UpdateFadeRed>d__24
struct  U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2  : public RuntimeObject
{
public:
	// System.Int32 RandomColorLerp/<UpdateFadeRed>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RandomColorLerp/<UpdateFadeRed>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String RandomColorLerp/<UpdateFadeRed>d__24::color
	String_t* ___color_2;
	// RandomColorLerp RandomColorLerp/<UpdateFadeRed>d__24::<>4__this
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666 * ___U3CU3E4__this_3;
	// System.Single RandomColorLerp/<UpdateFadeRed>d__24::transitionTime
	float ___transitionTime_4;
	// System.Action RandomColorLerp/<UpdateFadeRed>d__24::func
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___func_5;
	// System.Single RandomColorLerp/<UpdateFadeRed>d__24::<t>5__2
	float ___U3CtU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2, ___color_2)); }
	inline String_t* get_color_2() const { return ___color_2; }
	inline String_t** get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(String_t* value)
	{
		___color_2 = value;
		Il2CppCodeGenWriteBarrier((&___color_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2, ___U3CU3E4__this_3)); }
	inline RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_transitionTime_4() { return static_cast<int32_t>(offsetof(U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2, ___transitionTime_4)); }
	inline float get_transitionTime_4() const { return ___transitionTime_4; }
	inline float* get_address_of_transitionTime_4() { return &___transitionTime_4; }
	inline void set_transitionTime_4(float value)
	{
		___transitionTime_4 = value;
	}

	inline static int32_t get_offset_of_func_5() { return static_cast<int32_t>(offsetof(U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2, ___func_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_func_5() const { return ___func_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_func_5() { return &___func_5; }
	inline void set_func_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___func_5 = value;
		Il2CppCodeGenWriteBarrier((&___func_5), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2, ___U3CtU3E5__2_6)); }
	inline float get_U3CtU3E5__2_6() const { return ___U3CtU3E5__2_6; }
	inline float* get_address_of_U3CtU3E5__2_6() { return &___U3CtU3E5__2_6; }
	inline void set_U3CtU3E5__2_6(float value)
	{
		___U3CtU3E5__2_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEFADEREDU3ED__24_TB4F13466E870A0C10E65D2B528B2786D59F016C2_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_TEC126D91916AC34E93F1B459EC4445CCDDEC7BB7_H
#define U3CU3EC__DISPLAYCLASS4_0_TEC126D91916AC34E93F1B459EC4445CCDDEC7BB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SFXManager/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_tEC126D91916AC34E93F1B459EC4445CCDDEC7BB7  : public RuntimeObject
{
public:
	// System.String SFXManager/<>c__DisplayClass4_0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tEC126D91916AC34E93F1B459EC4445CCDDEC7BB7, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_TEC126D91916AC34E93F1B459EC4445CCDDEC7BB7_H
#ifndef SOUND_TE1867C28E2BC13723E294746060427A1487DF1F1_H
#define SOUND_TE1867C28E2BC13723E294746060427A1487DF1F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sound
struct  Sound_tE1867C28E2BC13723E294746060427A1487DF1F1  : public RuntimeObject
{
public:
	// System.String Sound::name
	String_t* ___name_0;
	// UnityEngine.AudioClip Sound::clip
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___clip_1;
	// System.Single Sound::volume
	float ___volume_2;
	// System.Single Sound::pitch
	float ___pitch_3;
	// System.Boolean Sound::loop
	bool ___loop_4;
	// UnityEngine.Audio.AudioMixer Sound::output
	AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * ___output_5;
	// UnityEngine.AudioSource Sound::source
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___source_6;
	// UnityEngine.Audio.AudioMixerGroup Sound::outputAudioMixerGroup
	AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 * ___outputAudioMixerGroup_7;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_clip_1() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___clip_1)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_clip_1() const { return ___clip_1; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_clip_1() { return &___clip_1; }
	inline void set_clip_1(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___clip_1 = value;
		Il2CppCodeGenWriteBarrier((&___clip_1), value);
	}

	inline static int32_t get_offset_of_volume_2() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___volume_2)); }
	inline float get_volume_2() const { return ___volume_2; }
	inline float* get_address_of_volume_2() { return &___volume_2; }
	inline void set_volume_2(float value)
	{
		___volume_2 = value;
	}

	inline static int32_t get_offset_of_pitch_3() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___pitch_3)); }
	inline float get_pitch_3() const { return ___pitch_3; }
	inline float* get_address_of_pitch_3() { return &___pitch_3; }
	inline void set_pitch_3(float value)
	{
		___pitch_3 = value;
	}

	inline static int32_t get_offset_of_loop_4() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___loop_4)); }
	inline bool get_loop_4() const { return ___loop_4; }
	inline bool* get_address_of_loop_4() { return &___loop_4; }
	inline void set_loop_4(bool value)
	{
		___loop_4 = value;
	}

	inline static int32_t get_offset_of_output_5() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___output_5)); }
	inline AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * get_output_5() const { return ___output_5; }
	inline AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F ** get_address_of_output_5() { return &___output_5; }
	inline void set_output_5(AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * value)
	{
		___output_5 = value;
		Il2CppCodeGenWriteBarrier((&___output_5), value);
	}

	inline static int32_t get_offset_of_source_6() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___source_6)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_source_6() const { return ___source_6; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_source_6() { return &___source_6; }
	inline void set_source_6(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___source_6 = value;
		Il2CppCodeGenWriteBarrier((&___source_6), value);
	}

	inline static int32_t get_offset_of_outputAudioMixerGroup_7() { return static_cast<int32_t>(offsetof(Sound_tE1867C28E2BC13723E294746060427A1487DF1F1, ___outputAudioMixerGroup_7)); }
	inline AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 * get_outputAudioMixerGroup_7() const { return ___outputAudioMixerGroup_7; }
	inline AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 ** get_address_of_outputAudioMixerGroup_7() { return &___outputAudioMixerGroup_7; }
	inline void set_outputAudioMixerGroup_7(AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 * value)
	{
		___outputAudioMixerGroup_7 = value;
		Il2CppCodeGenWriteBarrier((&___outputAudioMixerGroup_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUND_TE1867C28E2BC13723E294746060427A1487DF1F1_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T76722F36D9DBF3157E79FEE53AA1617CF45E7C14_H
#define U3CU3EC__DISPLAYCLASS4_0_T76722F36D9DBF3157E79FEE53AA1617CF45E7C14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAudioManager/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t76722F36D9DBF3157E79FEE53AA1617CF45E7C14  : public RuntimeObject
{
public:
	// System.String UIAudioManager/<>c__DisplayClass4_0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t76722F36D9DBF3157E79FEE53AA1617CF45E7C14, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T76722F36D9DBF3157E79FEE53AA1617CF45E7C14_H
#ifndef JSON_T1233A9134669B91C5F52DAF83019662916AC1453_H
#define JSON_T1233A9134669B91C5F52DAF83019662916AC1453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.MiniJSON.Json
struct  Json_t1233A9134669B91C5F52DAF83019662916AC1453  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T1233A9134669B91C5F52DAF83019662916AC1453_H
#ifndef PARSER_TE4D03B41834709DFEADA93A7339A46C21D037CE8_H
#define PARSER_TE4D03B41834709DFEADA93A7339A46C21D037CE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.MiniJSON.Json/Parser
struct  Parser_tE4D03B41834709DFEADA93A7339A46C21D037CE8  : public RuntimeObject
{
public:
	// System.IO.StringReader UnityEngine.Advertisements.MiniJSON.Json/Parser::json
	StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * ___json_1;

public:
	inline static int32_t get_offset_of_json_1() { return static_cast<int32_t>(offsetof(Parser_tE4D03B41834709DFEADA93A7339A46C21D037CE8, ___json_1)); }
	inline StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * get_json_1() const { return ___json_1; }
	inline StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 ** get_address_of_json_1() { return &___json_1; }
	inline void set_json_1(StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * value)
	{
		___json_1 = value;
		Il2CppCodeGenWriteBarrier((&___json_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_TE4D03B41834709DFEADA93A7339A46C21D037CE8_H
#ifndef SERIALIZER_T70CBC4A159065B79BAA7B1A81C13FCDE2A6C4C18_H
#define SERIALIZER_T70CBC4A159065B79BAA7B1A81C13FCDE2A6C4C18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.MiniJSON.Json/Serializer
struct  Serializer_t70CBC4A159065B79BAA7B1A81C13FCDE2A6C4C18  : public RuntimeObject
{
public:
	// System.Text.StringBuilder UnityEngine.Advertisements.MiniJSON.Json/Serializer::builder
	StringBuilder_t * ___builder_0;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(Serializer_t70CBC4A159065B79BAA7B1A81C13FCDE2A6C4C18, ___builder_0)); }
	inline StringBuilder_t * get_builder_0() const { return ___builder_0; }
	inline StringBuilder_t ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(StringBuilder_t * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((&___builder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_T70CBC4A159065B79BAA7B1A81C13FCDE2A6C4C18_H
#ifndef U3CU3EC__DISPLAYCLASS29_0_TCBCCDC66D57704B2D5EB0935B24500D03F2E5411_H
#define U3CU3EC__DISPLAYCLASS29_0_TCBCCDC66D57704B2D5EB0935B24500D03F2E5411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Platform/<>c__DisplayClass29_0
struct  U3CU3Ec__DisplayClass29_0_tCBCCDC66D57704B2D5EB0935B24500D03F2E5411  : public RuntimeObject
{
public:
	// System.String UnityEngine.Advertisements.Platform/<>c__DisplayClass29_0::placementId
	String_t* ___placementId_0;

public:
	inline static int32_t get_offset_of_placementId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tCBCCDC66D57704B2D5EB0935B24500D03F2E5411, ___placementId_0)); }
	inline String_t* get_placementId_0() const { return ___placementId_0; }
	inline String_t** get_address_of_placementId_0() { return &___placementId_0; }
	inline void set_placementId_0(String_t* value)
	{
		___placementId_0 = value;
		Il2CppCodeGenWriteBarrier((&___placementId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS29_0_TCBCCDC66D57704B2D5EB0935B24500D03F2E5411_H
#ifndef U3CU3EC__DISPLAYCLASS30_0_T1AB6F6C5823B12DD8552585BA09B19074A2F13AA_H
#define U3CU3EC__DISPLAYCLASS30_0_T1AB6F6C5823B12DD8552585BA09B19074A2F13AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Platform/<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_t1AB6F6C5823B12DD8552585BA09B19074A2F13AA  : public RuntimeObject
{
public:
	// System.Int64 UnityEngine.Advertisements.Platform/<>c__DisplayClass30_0::rawError
	int64_t ___rawError_0;
	// System.String UnityEngine.Advertisements.Platform/<>c__DisplayClass30_0::message
	String_t* ___message_1;

public:
	inline static int32_t get_offset_of_rawError_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t1AB6F6C5823B12DD8552585BA09B19074A2F13AA, ___rawError_0)); }
	inline int64_t get_rawError_0() const { return ___rawError_0; }
	inline int64_t* get_address_of_rawError_0() { return &___rawError_0; }
	inline void set_rawError_0(int64_t value)
	{
		___rawError_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t1AB6F6C5823B12DD8552585BA09B19074A2F13AA, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS30_0_T1AB6F6C5823B12DD8552585BA09B19074A2F13AA_H
#ifndef U3CU3EC__DISPLAYCLASS31_0_T671E94813CC93F03B5DB031F8FFFACC1FD99668C_H
#define U3CU3EC__DISPLAYCLASS31_0_T671E94813CC93F03B5DB031F8FFFACC1FD99668C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Platform/<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_t671E94813CC93F03B5DB031F8FFFACC1FD99668C  : public RuntimeObject
{
public:
	// System.String UnityEngine.Advertisements.Platform/<>c__DisplayClass31_0::placementId
	String_t* ___placementId_0;

public:
	inline static int32_t get_offset_of_placementId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t671E94813CC93F03B5DB031F8FFFACC1FD99668C, ___placementId_0)); }
	inline String_t* get_placementId_0() const { return ___placementId_0; }
	inline String_t** get_address_of_placementId_0() { return &___placementId_0; }
	inline void set_placementId_0(String_t* value)
	{
		___placementId_0 = value;
		Il2CppCodeGenWriteBarrier((&___placementId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_0_T671E94813CC93F03B5DB031F8FFFACC1FD99668C_H
#ifndef U3CU3EC__DISPLAYCLASS32_0_T2BE5122D5B70F219CA03D04B51DCD66A6EA0F4DD_H
#define U3CU3EC__DISPLAYCLASS32_0_T2BE5122D5B70F219CA03D04B51DCD66A6EA0F4DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Platform/<>c__DisplayClass32_0
struct  U3CU3Ec__DisplayClass32_0_t2BE5122D5B70F219CA03D04B51DCD66A6EA0F4DD  : public RuntimeObject
{
public:
	// System.String UnityEngine.Advertisements.Platform/<>c__DisplayClass32_0::placementId
	String_t* ___placementId_0;
	// System.Int64 UnityEngine.Advertisements.Platform/<>c__DisplayClass32_0::rawShowResult
	int64_t ___rawShowResult_1;

public:
	inline static int32_t get_offset_of_placementId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t2BE5122D5B70F219CA03D04B51DCD66A6EA0F4DD, ___placementId_0)); }
	inline String_t* get_placementId_0() const { return ___placementId_0; }
	inline String_t** get_address_of_placementId_0() { return &___placementId_0; }
	inline void set_placementId_0(String_t* value)
	{
		___placementId_0 = value;
		Il2CppCodeGenWriteBarrier((&___placementId_0), value);
	}

	inline static int32_t get_offset_of_rawShowResult_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t2BE5122D5B70F219CA03D04B51DCD66A6EA0F4DD, ___rawShowResult_1)); }
	inline int64_t get_rawShowResult_1() const { return ___rawShowResult_1; }
	inline int64_t* get_address_of_rawShowResult_1() { return &___rawShowResult_1; }
	inline void set_rawShowResult_1(int64_t value)
	{
		___rawShowResult_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS32_0_T2BE5122D5B70F219CA03D04B51DCD66A6EA0F4DD_H
#ifndef PURCHASING_T6E5C314B69988BD97CB80A8480B95D7302B93DB2_H
#define PURCHASING_T6E5C314B69988BD97CB80A8480B95D7302B93DB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Purchasing
struct  Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2  : public RuntimeObject
{
public:

public:
};

struct Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields
{
public:
	// System.Type UnityEngine.Advertisements.Purchasing::s_PurchasingManagerType
	Type_t * ___s_PurchasingManagerType_0;
	// System.Boolean UnityEngine.Advertisements.Purchasing::s_Initialized
	bool ___s_Initialized_1;
	// System.Reflection.MethodInfo UnityEngine.Advertisements.Purchasing::s_PurchasingInitiatePurchaseMethodInfo
	MethodInfo_t * ___s_PurchasingInitiatePurchaseMethodInfo_2;
	// System.Reflection.MethodInfo UnityEngine.Advertisements.Purchasing::s_PurchasingGetPromoVersionMethodInfo
	MethodInfo_t * ___s_PurchasingGetPromoVersionMethodInfo_3;
	// System.Reflection.MethodInfo UnityEngine.Advertisements.Purchasing::s_PurchasingGetPromoCatalogMethodInfo
	MethodInfo_t * ___s_PurchasingGetPromoCatalogMethodInfo_4;
	// System.String UnityEngine.Advertisements.Purchasing::s_PurchasingManagerClassName
	String_t* ___s_PurchasingManagerClassName_5;
	// System.String UnityEngine.Advertisements.Purchasing::s_PurchasingInitiatePurchaseMethodName
	String_t* ___s_PurchasingInitiatePurchaseMethodName_6;
	// System.String UnityEngine.Advertisements.Purchasing::s_PurchasingGetPromoVersionMethodName
	String_t* ___s_PurchasingGetPromoVersionMethodName_7;
	// System.String UnityEngine.Advertisements.Purchasing::s_PurchasingGetPromoCatalogMethodName
	String_t* ___s_PurchasingGetPromoCatalogMethodName_8;
	// IPurchasingEventSender UnityEngine.Advertisements.Purchasing::s_Platform
	RuntimeObject* ___s_Platform_9;

public:
	inline static int32_t get_offset_of_s_PurchasingManagerType_0() { return static_cast<int32_t>(offsetof(Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields, ___s_PurchasingManagerType_0)); }
	inline Type_t * get_s_PurchasingManagerType_0() const { return ___s_PurchasingManagerType_0; }
	inline Type_t ** get_address_of_s_PurchasingManagerType_0() { return &___s_PurchasingManagerType_0; }
	inline void set_s_PurchasingManagerType_0(Type_t * value)
	{
		___s_PurchasingManagerType_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_PurchasingManagerType_0), value);
	}

	inline static int32_t get_offset_of_s_Initialized_1() { return static_cast<int32_t>(offsetof(Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields, ___s_Initialized_1)); }
	inline bool get_s_Initialized_1() const { return ___s_Initialized_1; }
	inline bool* get_address_of_s_Initialized_1() { return &___s_Initialized_1; }
	inline void set_s_Initialized_1(bool value)
	{
		___s_Initialized_1 = value;
	}

	inline static int32_t get_offset_of_s_PurchasingInitiatePurchaseMethodInfo_2() { return static_cast<int32_t>(offsetof(Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields, ___s_PurchasingInitiatePurchaseMethodInfo_2)); }
	inline MethodInfo_t * get_s_PurchasingInitiatePurchaseMethodInfo_2() const { return ___s_PurchasingInitiatePurchaseMethodInfo_2; }
	inline MethodInfo_t ** get_address_of_s_PurchasingInitiatePurchaseMethodInfo_2() { return &___s_PurchasingInitiatePurchaseMethodInfo_2; }
	inline void set_s_PurchasingInitiatePurchaseMethodInfo_2(MethodInfo_t * value)
	{
		___s_PurchasingInitiatePurchaseMethodInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_PurchasingInitiatePurchaseMethodInfo_2), value);
	}

	inline static int32_t get_offset_of_s_PurchasingGetPromoVersionMethodInfo_3() { return static_cast<int32_t>(offsetof(Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields, ___s_PurchasingGetPromoVersionMethodInfo_3)); }
	inline MethodInfo_t * get_s_PurchasingGetPromoVersionMethodInfo_3() const { return ___s_PurchasingGetPromoVersionMethodInfo_3; }
	inline MethodInfo_t ** get_address_of_s_PurchasingGetPromoVersionMethodInfo_3() { return &___s_PurchasingGetPromoVersionMethodInfo_3; }
	inline void set_s_PurchasingGetPromoVersionMethodInfo_3(MethodInfo_t * value)
	{
		___s_PurchasingGetPromoVersionMethodInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_PurchasingGetPromoVersionMethodInfo_3), value);
	}

	inline static int32_t get_offset_of_s_PurchasingGetPromoCatalogMethodInfo_4() { return static_cast<int32_t>(offsetof(Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields, ___s_PurchasingGetPromoCatalogMethodInfo_4)); }
	inline MethodInfo_t * get_s_PurchasingGetPromoCatalogMethodInfo_4() const { return ___s_PurchasingGetPromoCatalogMethodInfo_4; }
	inline MethodInfo_t ** get_address_of_s_PurchasingGetPromoCatalogMethodInfo_4() { return &___s_PurchasingGetPromoCatalogMethodInfo_4; }
	inline void set_s_PurchasingGetPromoCatalogMethodInfo_4(MethodInfo_t * value)
	{
		___s_PurchasingGetPromoCatalogMethodInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_PurchasingGetPromoCatalogMethodInfo_4), value);
	}

	inline static int32_t get_offset_of_s_PurchasingManagerClassName_5() { return static_cast<int32_t>(offsetof(Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields, ___s_PurchasingManagerClassName_5)); }
	inline String_t* get_s_PurchasingManagerClassName_5() const { return ___s_PurchasingManagerClassName_5; }
	inline String_t** get_address_of_s_PurchasingManagerClassName_5() { return &___s_PurchasingManagerClassName_5; }
	inline void set_s_PurchasingManagerClassName_5(String_t* value)
	{
		___s_PurchasingManagerClassName_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_PurchasingManagerClassName_5), value);
	}

	inline static int32_t get_offset_of_s_PurchasingInitiatePurchaseMethodName_6() { return static_cast<int32_t>(offsetof(Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields, ___s_PurchasingInitiatePurchaseMethodName_6)); }
	inline String_t* get_s_PurchasingInitiatePurchaseMethodName_6() const { return ___s_PurchasingInitiatePurchaseMethodName_6; }
	inline String_t** get_address_of_s_PurchasingInitiatePurchaseMethodName_6() { return &___s_PurchasingInitiatePurchaseMethodName_6; }
	inline void set_s_PurchasingInitiatePurchaseMethodName_6(String_t* value)
	{
		___s_PurchasingInitiatePurchaseMethodName_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_PurchasingInitiatePurchaseMethodName_6), value);
	}

	inline static int32_t get_offset_of_s_PurchasingGetPromoVersionMethodName_7() { return static_cast<int32_t>(offsetof(Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields, ___s_PurchasingGetPromoVersionMethodName_7)); }
	inline String_t* get_s_PurchasingGetPromoVersionMethodName_7() const { return ___s_PurchasingGetPromoVersionMethodName_7; }
	inline String_t** get_address_of_s_PurchasingGetPromoVersionMethodName_7() { return &___s_PurchasingGetPromoVersionMethodName_7; }
	inline void set_s_PurchasingGetPromoVersionMethodName_7(String_t* value)
	{
		___s_PurchasingGetPromoVersionMethodName_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_PurchasingGetPromoVersionMethodName_7), value);
	}

	inline static int32_t get_offset_of_s_PurchasingGetPromoCatalogMethodName_8() { return static_cast<int32_t>(offsetof(Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields, ___s_PurchasingGetPromoCatalogMethodName_8)); }
	inline String_t* get_s_PurchasingGetPromoCatalogMethodName_8() const { return ___s_PurchasingGetPromoCatalogMethodName_8; }
	inline String_t** get_address_of_s_PurchasingGetPromoCatalogMethodName_8() { return &___s_PurchasingGetPromoCatalogMethodName_8; }
	inline void set_s_PurchasingGetPromoCatalogMethodName_8(String_t* value)
	{
		___s_PurchasingGetPromoCatalogMethodName_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_PurchasingGetPromoCatalogMethodName_8), value);
	}

	inline static int32_t get_offset_of_s_Platform_9() { return static_cast<int32_t>(offsetof(Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields, ___s_Platform_9)); }
	inline RuntimeObject* get_s_Platform_9() const { return ___s_Platform_9; }
	inline RuntimeObject** get_address_of_s_Platform_9() { return &___s_Platform_9; }
	inline void set_s_Platform_9(RuntimeObject* value)
	{
		___s_Platform_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_Platform_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASING_T6E5C314B69988BD97CB80A8480B95D7302B93DB2_H
#ifndef SHOWOPTIONS_T5DB8E1867258D81478370DB18E9D5E9FA0174FB1_H
#define SHOWOPTIONS_T5DB8E1867258D81478370DB18E9D5E9FA0174FB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.ShowOptions
struct  ShowOptions_t5DB8E1867258D81478370DB18E9D5E9FA0174FB1  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::<resultCallback>k__BackingField
	Action_1_tA383B4D00F0D86F49A41D08EFE2A0C3699DB1403 * ___U3CresultCallbackU3Ek__BackingField_0;
	// System.String UnityEngine.Advertisements.ShowOptions::<gamerSid>k__BackingField
	String_t* ___U3CgamerSidU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CresultCallbackU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ShowOptions_t5DB8E1867258D81478370DB18E9D5E9FA0174FB1, ___U3CresultCallbackU3Ek__BackingField_0)); }
	inline Action_1_tA383B4D00F0D86F49A41D08EFE2A0C3699DB1403 * get_U3CresultCallbackU3Ek__BackingField_0() const { return ___U3CresultCallbackU3Ek__BackingField_0; }
	inline Action_1_tA383B4D00F0D86F49A41D08EFE2A0C3699DB1403 ** get_address_of_U3CresultCallbackU3Ek__BackingField_0() { return &___U3CresultCallbackU3Ek__BackingField_0; }
	inline void set_U3CresultCallbackU3Ek__BackingField_0(Action_1_tA383B4D00F0D86F49A41D08EFE2A0C3699DB1403 * value)
	{
		___U3CresultCallbackU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresultCallbackU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CgamerSidU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ShowOptions_t5DB8E1867258D81478370DB18E9D5E9FA0174FB1, ___U3CgamerSidU3Ek__BackingField_1)); }
	inline String_t* get_U3CgamerSidU3Ek__BackingField_1() const { return ___U3CgamerSidU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CgamerSidU3Ek__BackingField_1() { return &___U3CgamerSidU3Ek__BackingField_1; }
	inline void set_U3CgamerSidU3Ek__BackingField_1(String_t* value)
	{
		___U3CgamerSidU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgamerSidU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWOPTIONS_T5DB8E1867258D81478370DB18E9D5E9FA0174FB1_H
#ifndef UNITYENGINEAPPLICATION_T05457944E7341DF19A1837B5E14BB6FFA8E0CD21_H
#define UNITYENGINEAPPLICATION_T05457944E7341DF19A1837B5E14BB6FFA8E0CD21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.UnityEngineApplication
struct  UnityEngineApplication_t05457944E7341DF19A1837B5E14BB6FFA8E0CD21  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYENGINEAPPLICATION_T05457944E7341DF19A1837B5E14BB6FFA8E0CD21_H
#ifndef UNSUPPORTEDPLATFORM_T66201CAD897970CA608C1A3658018CFA76C53440_H
#define UNSUPPORTEDPLATFORM_T66201CAD897970CA608C1A3658018CFA76C53440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.UnsupportedPlatform
struct  UnsupportedPlatform_t66201CAD897970CA608C1A3658018CFA76C53440  : public RuntimeObject
{
public:
	// System.EventHandler`1<UnityEngine.Advertisements.FinishEventArgs> UnityEngine.Advertisements.UnsupportedPlatform::OnFinish
	EventHandler_1_tFE307AA906B5E143091F4E06B873B19B0461A069 * ___OnFinish_0;

public:
	inline static int32_t get_offset_of_OnFinish_0() { return static_cast<int32_t>(offsetof(UnsupportedPlatform_t66201CAD897970CA608C1A3658018CFA76C53440, ___OnFinish_0)); }
	inline EventHandler_1_tFE307AA906B5E143091F4E06B873B19B0461A069 * get_OnFinish_0() const { return ___OnFinish_0; }
	inline EventHandler_1_tFE307AA906B5E143091F4E06B873B19B0461A069 ** get_address_of_OnFinish_0() { return &___OnFinish_0; }
	inline void set_OnFinish_0(EventHandler_1_tFE307AA906B5E143091F4E06B873B19B0461A069 * value)
	{
		___OnFinish_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinish_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSUPPORTEDPLATFORM_T66201CAD897970CA608C1A3658018CFA76C53440_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef READYEVENTARGS_TA72D1D6B702B685AEEABEED8CB6B264A4C9F811D_H
#define READYEVENTARGS_TA72D1D6B702B685AEEABEED8CB6B264A4C9F811D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.ReadyEventArgs
struct  ReadyEventArgs_tA72D1D6B702B685AEEABEED8CB6B264A4C9F811D  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String UnityEngine.Advertisements.ReadyEventArgs::<placementId>k__BackingField
	String_t* ___U3CplacementIdU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CplacementIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReadyEventArgs_tA72D1D6B702B685AEEABEED8CB6B264A4C9F811D, ___U3CplacementIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CplacementIdU3Ek__BackingField_1() const { return ___U3CplacementIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CplacementIdU3Ek__BackingField_1() { return &___U3CplacementIdU3Ek__BackingField_1; }
	inline void set_U3CplacementIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CplacementIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplacementIdU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READYEVENTARGS_TA72D1D6B702B685AEEABEED8CB6B264A4C9F811D_H
#ifndef STARTEVENTARGS_T84C5E305550244AFFF16E687F63FF2ED19320385_H
#define STARTEVENTARGS_T84C5E305550244AFFF16E687F63FF2ED19320385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.StartEventArgs
struct  StartEventArgs_t84C5E305550244AFFF16E687F63FF2ED19320385  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String UnityEngine.Advertisements.StartEventArgs::<placementId>k__BackingField
	String_t* ___U3CplacementIdU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CplacementIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StartEventArgs_t84C5E305550244AFFF16E687F63FF2ED19320385, ___U3CplacementIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CplacementIdU3Ek__BackingField_1() const { return ___U3CplacementIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CplacementIdU3Ek__BackingField_1() { return &___U3CplacementIdU3Ek__BackingField_1; }
	inline void set_U3CplacementIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CplacementIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplacementIdU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTEVENTARGS_T84C5E305550244AFFF16E687F63FF2ED19320385_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#define LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef AXISOPTIONS_T5AC84B43FFEB20E770F1E4CF5EC2D3AADA9D18D0_H
#define AXISOPTIONS_T5AC84B43FFEB20E770F1E4CF5EC2D3AADA9D18D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AxisOptions
struct  AxisOptions_t5AC84B43FFEB20E770F1E4CF5EC2D3AADA9D18D0 
{
public:
	// System.Int32 AxisOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisOptions_t5AC84B43FFEB20E770F1E4CF5EC2D3AADA9D18D0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTIONS_T5AC84B43FFEB20E770F1E4CF5EC2D3AADA9D18D0_H
#ifndef U3CANIMU3ED__7_TEE266E252916FB7108641A4D24EF5C9F772D5E88_H
#define U3CANIMU3ED__7_TEE266E252916FB7108641A4D24EF5C9F772D5E88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlashingTMPro/<anim>d__7
struct  U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88  : public RuntimeObject
{
public:
	// System.Int32 FlashingTMPro/<anim>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object FlashingTMPro/<anim>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// FlashingTMPro FlashingTMPro/<anim>d__7::<>4__this
	FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941 * ___U3CU3E4__this_2;
	// UnityEngine.Color FlashingTMPro/<anim>d__7::<currentColor>5__2
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___U3CcurrentColorU3E5__2_3;
	// UnityEngine.Color FlashingTMPro/<anim>d__7::<invisibleColor>5__3
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___U3CinvisibleColorU3E5__3_4;
	// System.Single FlashingTMPro/<anim>d__7::<oldAnimSpeedInSec>5__4
	float ___U3ColdAnimSpeedInSecU3E5__4_5;
	// System.Single FlashingTMPro/<anim>d__7::<counter>5__5
	float ___U3CcounterU3E5__5_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88, ___U3CU3E4__this_2)); }
	inline FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CcurrentColorU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88, ___U3CcurrentColorU3E5__2_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_U3CcurrentColorU3E5__2_3() const { return ___U3CcurrentColorU3E5__2_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_U3CcurrentColorU3E5__2_3() { return &___U3CcurrentColorU3E5__2_3; }
	inline void set_U3CcurrentColorU3E5__2_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___U3CcurrentColorU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CinvisibleColorU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88, ___U3CinvisibleColorU3E5__3_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_U3CinvisibleColorU3E5__3_4() const { return ___U3CinvisibleColorU3E5__3_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_U3CinvisibleColorU3E5__3_4() { return &___U3CinvisibleColorU3E5__3_4; }
	inline void set_U3CinvisibleColorU3E5__3_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___U3CinvisibleColorU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3ColdAnimSpeedInSecU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88, ___U3ColdAnimSpeedInSecU3E5__4_5)); }
	inline float get_U3ColdAnimSpeedInSecU3E5__4_5() const { return ___U3ColdAnimSpeedInSecU3E5__4_5; }
	inline float* get_address_of_U3ColdAnimSpeedInSecU3E5__4_5() { return &___U3ColdAnimSpeedInSecU3E5__4_5; }
	inline void set_U3ColdAnimSpeedInSecU3E5__4_5(float value)
	{
		___U3ColdAnimSpeedInSecU3E5__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88, ___U3CcounterU3E5__5_6)); }
	inline float get_U3CcounterU3E5__5_6() const { return ___U3CcounterU3E5__5_6; }
	inline float* get_address_of_U3CcounterU3E5__5_6() { return &___U3CcounterU3E5__5_6; }
	inline void set_U3CcounterU3E5__5_6(float value)
	{
		___U3CcounterU3E5__5_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMU3ED__7_TEE266E252916FB7108641A4D24EF5C9F772D5E88_H
#ifndef JOYSTICKTYPE_T8F089F639C0CED5CA78941C78E1CC72EB61892B6_H
#define JOYSTICKTYPE_T8F089F639C0CED5CA78941C78E1CC72EB61892B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoystickType
struct  JoystickType_t8F089F639C0CED5CA78941C78E1CC72EB61892B6 
{
public:
	// System.Int32 JoystickType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JoystickType_t8F089F639C0CED5CA78941C78E1CC72EB61892B6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKTYPE_T8F089F639C0CED5CA78941C78E1CC72EB61892B6_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef TOKEN_T788CFC64AC09A1B3548F66D4E2ED5DC12E6258CD_H
#define TOKEN_T788CFC64AC09A1B3548F66D4E2ED5DC12E6258CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN
struct  TOKEN_t788CFC64AC09A1B3548F66D4E2ED5DC12E6258CD 
{
public:
	// System.Int32 UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TOKEN_t788CFC64AC09A1B3548F66D4E2ED5DC12E6258CD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T788CFC64AC09A1B3548F66D4E2ED5DC12E6258CD_H
#ifndef PURCHASINGEVENT_T0574DBB0BD92BE872A7FD505038250E56DF9E1C7_H
#define PURCHASINGEVENT_T0574DBB0BD92BE872A7FD505038250E56DF9E1C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.PurchasingEvent
struct  PurchasingEvent_t0574DBB0BD92BE872A7FD505038250E56DF9E1C7 
{
public:
	// System.Int32 UnityEngine.Advertisements.PurchasingEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PurchasingEvent_t0574DBB0BD92BE872A7FD505038250E56DF9E1C7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASINGEVENT_T0574DBB0BD92BE872A7FD505038250E56DF9E1C7_H
#ifndef SHOWRESULT_T2D262B4453C0ADBD99223216EB569FDDA32516B6_H
#define SHOWRESULT_T2D262B4453C0ADBD99223216EB569FDDA32516B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.ShowResult
struct  ShowResult_t2D262B4453C0ADBD99223216EB569FDDA32516B6 
{
public:
	// System.Int32 UnityEngine.Advertisements.ShowResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShowResult_t2D262B4453C0ADBD99223216EB569FDDA32516B6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWRESULT_T2D262B4453C0ADBD99223216EB569FDDA32516B6_H
#ifndef GRADIENT_T35A694DDA1066524440E325E582B01E33DE66A3A_H
#define GRADIENT_T35A694DDA1066524440E325E582B01E33DE66A3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Gradient
struct  Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Gradient::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Gradient
struct Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Gradient
struct Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // GRADIENT_T35A694DDA1066524440E325E582B01E33DE66A3A_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef PARTICLESYSTEMGRADIENTMODE_T7CCC7CC3D80C8FF5134FF94F28BBB579595E8CD5_H
#define PARTICLESYSTEMGRADIENTMODE_T7CCC7CC3D80C8FF5134FF94F28BBB579595E8CD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemGradientMode
struct  ParticleSystemGradientMode_t7CCC7CC3D80C8FF5134FF94F28BBB579595E8CD5 
{
public:
	// System.Int32 UnityEngine.ParticleSystemGradientMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemGradientMode_t7CCC7CC3D80C8FF5134FF94F28BBB579595E8CD5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMGRADIENTMODE_T7CCC7CC3D80C8FF5134FF94F28BBB579595E8CD5_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef MINMAXGRADIENT_TD94D591FCD1E394D6502774CDFC068CFA893FE6B_H
#define MINMAXGRADIENT_TD94D591FCD1E394D6502774CDFC068CFA893FE6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MinMaxGradient
struct  MinMaxGradient_tD94D591FCD1E394D6502774CDFC068CFA893FE6B 
{
public:
	// UnityEngine.ParticleSystemGradientMode UnityEngine.ParticleSystem/MinMaxGradient::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMin
	Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * ___m_GradientMin_1;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMax
	Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * ___m_GradientMax_2;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMin
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_ColorMin_3;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMax
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_ColorMax_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(MinMaxGradient_tD94D591FCD1E394D6502774CDFC068CFA893FE6B, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_GradientMin_1() { return static_cast<int32_t>(offsetof(MinMaxGradient_tD94D591FCD1E394D6502774CDFC068CFA893FE6B, ___m_GradientMin_1)); }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * get_m_GradientMin_1() const { return ___m_GradientMin_1; }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A ** get_address_of_m_GradientMin_1() { return &___m_GradientMin_1; }
	inline void set_m_GradientMin_1(Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * value)
	{
		___m_GradientMin_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GradientMin_1), value);
	}

	inline static int32_t get_offset_of_m_GradientMax_2() { return static_cast<int32_t>(offsetof(MinMaxGradient_tD94D591FCD1E394D6502774CDFC068CFA893FE6B, ___m_GradientMax_2)); }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * get_m_GradientMax_2() const { return ___m_GradientMax_2; }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A ** get_address_of_m_GradientMax_2() { return &___m_GradientMax_2; }
	inline void set_m_GradientMax_2(Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * value)
	{
		___m_GradientMax_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_GradientMax_2), value);
	}

	inline static int32_t get_offset_of_m_ColorMin_3() { return static_cast<int32_t>(offsetof(MinMaxGradient_tD94D591FCD1E394D6502774CDFC068CFA893FE6B, ___m_ColorMin_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_ColorMin_3() const { return ___m_ColorMin_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_ColorMin_3() { return &___m_ColorMin_3; }
	inline void set_m_ColorMin_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_ColorMin_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMax_4() { return static_cast<int32_t>(offsetof(MinMaxGradient_tD94D591FCD1E394D6502774CDFC068CFA893FE6B, ___m_ColorMax_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_ColorMax_4() const { return ___m_ColorMax_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_ColorMax_4() { return &___m_ColorMax_4; }
	inline void set_m_ColorMax_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_ColorMax_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MinMaxGradient
struct MinMaxGradient_tD94D591FCD1E394D6502774CDFC068CFA893FE6B_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A_marshaled_pinvoke ___m_GradientMin_1;
	Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A_marshaled_pinvoke ___m_GradientMax_2;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_ColorMin_3;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_ColorMax_4;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MinMaxGradient
struct MinMaxGradient_tD94D591FCD1E394D6502774CDFC068CFA893FE6B_marshaled_com
{
	int32_t ___m_Mode_0;
	Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A_marshaled_com* ___m_GradientMin_1;
	Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A_marshaled_com* ___m_GradientMax_2;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_ColorMin_3;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_ColorMax_4;
};
#endif // MINMAXGRADIENT_TD94D591FCD1E394D6502774CDFC068CFA893FE6B_H
#ifndef UNITYADSPURCHASINGGETPURCHASINGVERSION_T6FAC2706D072DAA39D5634C7CBC01302A42EE43B_H
#define UNITYADSPURCHASINGGETPURCHASINGVERSION_T6FAC2706D072DAA39D5634C7CBC01302A42EE43B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Platform/unityAdsPurchasingGetPurchasingVersion
struct  unityAdsPurchasingGetPurchasingVersion_t6FAC2706D072DAA39D5634C7CBC01302A42EE43B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSPURCHASINGGETPURCHASINGVERSION_T6FAC2706D072DAA39D5634C7CBC01302A42EE43B_H
#ifndef UNITYADSPURCHASINGINITIALIZE_T5D3C1F5E55EA709C504F5B479E39D92D34278768_H
#define UNITYADSPURCHASINGINITIALIZE_T5D3C1F5E55EA709C504F5B479E39D92D34278768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.Platform/unityAdsPurchasingInitialize
struct  unityAdsPurchasingInitialize_t5D3C1F5E55EA709C504F5B479E39D92D34278768  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSPURCHASINGINITIALIZE_T5D3C1F5E55EA709C504F5B479E39D92D34278768_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ANALOGGLITCH_T92C13F7DA52341788C5070C934405D92D486D86D_H
#define ANALOGGLITCH_T92C13F7DA52341788C5070C934405D92D486D86D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnalogGlitch
struct  AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single AnalogGlitch::_scanLineJitter
	float ____scanLineJitter_4;
	// System.Single AnalogGlitch::_verticalJump
	float ____verticalJump_5;
	// System.Single AnalogGlitch::_horizontalShake
	float ____horizontalShake_6;
	// System.Single AnalogGlitch::_colorDrift
	float ____colorDrift_7;
	// UnityEngine.Shader AnalogGlitch::_shader
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ____shader_8;
	// UnityEngine.Material AnalogGlitch::_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____material_9;
	// System.Single AnalogGlitch::_verticalJumpTime
	float ____verticalJumpTime_10;

public:
	inline static int32_t get_offset_of__scanLineJitter_4() { return static_cast<int32_t>(offsetof(AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D, ____scanLineJitter_4)); }
	inline float get__scanLineJitter_4() const { return ____scanLineJitter_4; }
	inline float* get_address_of__scanLineJitter_4() { return &____scanLineJitter_4; }
	inline void set__scanLineJitter_4(float value)
	{
		____scanLineJitter_4 = value;
	}

	inline static int32_t get_offset_of__verticalJump_5() { return static_cast<int32_t>(offsetof(AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D, ____verticalJump_5)); }
	inline float get__verticalJump_5() const { return ____verticalJump_5; }
	inline float* get_address_of__verticalJump_5() { return &____verticalJump_5; }
	inline void set__verticalJump_5(float value)
	{
		____verticalJump_5 = value;
	}

	inline static int32_t get_offset_of__horizontalShake_6() { return static_cast<int32_t>(offsetof(AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D, ____horizontalShake_6)); }
	inline float get__horizontalShake_6() const { return ____horizontalShake_6; }
	inline float* get_address_of__horizontalShake_6() { return &____horizontalShake_6; }
	inline void set__horizontalShake_6(float value)
	{
		____horizontalShake_6 = value;
	}

	inline static int32_t get_offset_of__colorDrift_7() { return static_cast<int32_t>(offsetof(AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D, ____colorDrift_7)); }
	inline float get__colorDrift_7() const { return ____colorDrift_7; }
	inline float* get_address_of__colorDrift_7() { return &____colorDrift_7; }
	inline void set__colorDrift_7(float value)
	{
		____colorDrift_7 = value;
	}

	inline static int32_t get_offset_of__shader_8() { return static_cast<int32_t>(offsetof(AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D, ____shader_8)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get__shader_8() const { return ____shader_8; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of__shader_8() { return &____shader_8; }
	inline void set__shader_8(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		____shader_8 = value;
		Il2CppCodeGenWriteBarrier((&____shader_8), value);
	}

	inline static int32_t get_offset_of__material_9() { return static_cast<int32_t>(offsetof(AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D, ____material_9)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__material_9() const { return ____material_9; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__material_9() { return &____material_9; }
	inline void set__material_9(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____material_9 = value;
		Il2CppCodeGenWriteBarrier((&____material_9), value);
	}

	inline static int32_t get_offset_of__verticalJumpTime_10() { return static_cast<int32_t>(offsetof(AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D, ____verticalJumpTime_10)); }
	inline float get__verticalJumpTime_10() const { return ____verticalJumpTime_10; }
	inline float* get_address_of__verticalJumpTime_10() { return &____verticalJumpTime_10; }
	inline void set__verticalJumpTime_10(float value)
	{
		____verticalJumpTime_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALOGGLITCH_T92C13F7DA52341788C5070C934405D92D486D86D_H
#ifndef AUDIOMANAGER_T67152D1A926351222F6AD0C0F2442EAE024C7D23_H
#define AUDIOMANAGER_T67152D1A926351222F6AD0C0F2442EAE024C7D23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager
struct  AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Sound[] AudioManager::sounds
	SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B* ___sounds_4;
	// System.String AudioManager::m_Scene
	String_t* ___m_Scene_6;

public:
	inline static int32_t get_offset_of_sounds_4() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___sounds_4)); }
	inline SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B* get_sounds_4() const { return ___sounds_4; }
	inline SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B** get_address_of_sounds_4() { return &___sounds_4; }
	inline void set_sounds_4(SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B* value)
	{
		___sounds_4 = value;
		Il2CppCodeGenWriteBarrier((&___sounds_4), value);
	}

	inline static int32_t get_offset_of_m_Scene_6() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23, ___m_Scene_6)); }
	inline String_t* get_m_Scene_6() const { return ___m_Scene_6; }
	inline String_t** get_address_of_m_Scene_6() { return &___m_Scene_6; }
	inline void set_m_Scene_6(String_t* value)
	{
		___m_Scene_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Scene_6), value);
	}
};

struct AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields
{
public:
	// AudioManager AudioManager::instance
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields, ___instance_5)); }
	inline AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * get_instance_5() const { return ___instance_5; }
	inline AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMANAGER_T67152D1A926351222F6AD0C0F2442EAE024C7D23_H
#ifndef CAMERAFOLLOW_T427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142_H
#define CAMERAFOLLOW_T427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFollow
struct  CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single CameraFollow::fallSpeed
	float ___fallSpeed_4;
	// System.Boolean CameraFollow::isMoving
	bool ___isMoving_5;
	// System.Single CameraFollow::xOffset
	float ___xOffset_6;
	// System.Single CameraFollow::yOffset
	float ___yOffset_7;

public:
	inline static int32_t get_offset_of_fallSpeed_4() { return static_cast<int32_t>(offsetof(CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142, ___fallSpeed_4)); }
	inline float get_fallSpeed_4() const { return ___fallSpeed_4; }
	inline float* get_address_of_fallSpeed_4() { return &___fallSpeed_4; }
	inline void set_fallSpeed_4(float value)
	{
		___fallSpeed_4 = value;
	}

	inline static int32_t get_offset_of_isMoving_5() { return static_cast<int32_t>(offsetof(CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142, ___isMoving_5)); }
	inline bool get_isMoving_5() const { return ___isMoving_5; }
	inline bool* get_address_of_isMoving_5() { return &___isMoving_5; }
	inline void set_isMoving_5(bool value)
	{
		___isMoving_5 = value;
	}

	inline static int32_t get_offset_of_xOffset_6() { return static_cast<int32_t>(offsetof(CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142, ___xOffset_6)); }
	inline float get_xOffset_6() const { return ___xOffset_6; }
	inline float* get_address_of_xOffset_6() { return &___xOffset_6; }
	inline void set_xOffset_6(float value)
	{
		___xOffset_6 = value;
	}

	inline static int32_t get_offset_of_yOffset_7() { return static_cast<int32_t>(offsetof(CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142, ___yOffset_7)); }
	inline float get_yOffset_7() const { return ___yOffset_7; }
	inline float* get_address_of_yOffset_7() { return &___yOffset_7; }
	inline void set_yOffset_7(float value)
	{
		___yOffset_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFOLLOW_T427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142_H
#ifndef CAMERAGLITCH_T766226B76251A4C6BC42452E60B8A4FFE8A4DB51_H
#define CAMERAGLITCH_T766226B76251A4C6BC42452E60B8A4FFE8A4DB51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraGlitch
struct  CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// GameCounter CameraGlitch::GC
	GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F * ___GC_4;
	// GlitchEffect CameraGlitch::GE
	GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83 * ___GE_5;
	// GlitchFx CameraGlitch::GF
	GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A * ___GF_6;
	// AnalogGlitch CameraGlitch::AG
	AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D * ___AG_7;
	// System.Int32 CameraGlitch::playedTimes
	int32_t ___playedTimes_8;
	// System.Single CameraGlitch::n1
	float ___n1_9;
	// System.Single CameraGlitch::n2
	float ___n2_10;
	// System.Single CameraGlitch::n3
	float ___n3_11;
	// System.Single CameraGlitch::n4
	float ___n4_12;
	// System.Single CameraGlitch::n5
	float ___n5_13;
	// System.Single CameraGlitch::n6
	float ___n6_14;
	// System.Single CameraGlitch::n7
	float ___n7_15;
	// System.Int32 CameraGlitch::nUntilEffects
	int32_t ___nUntilEffects_16;

public:
	inline static int32_t get_offset_of_GC_4() { return static_cast<int32_t>(offsetof(CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51, ___GC_4)); }
	inline GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F * get_GC_4() const { return ___GC_4; }
	inline GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F ** get_address_of_GC_4() { return &___GC_4; }
	inline void set_GC_4(GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F * value)
	{
		___GC_4 = value;
		Il2CppCodeGenWriteBarrier((&___GC_4), value);
	}

	inline static int32_t get_offset_of_GE_5() { return static_cast<int32_t>(offsetof(CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51, ___GE_5)); }
	inline GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83 * get_GE_5() const { return ___GE_5; }
	inline GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83 ** get_address_of_GE_5() { return &___GE_5; }
	inline void set_GE_5(GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83 * value)
	{
		___GE_5 = value;
		Il2CppCodeGenWriteBarrier((&___GE_5), value);
	}

	inline static int32_t get_offset_of_GF_6() { return static_cast<int32_t>(offsetof(CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51, ___GF_6)); }
	inline GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A * get_GF_6() const { return ___GF_6; }
	inline GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A ** get_address_of_GF_6() { return &___GF_6; }
	inline void set_GF_6(GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A * value)
	{
		___GF_6 = value;
		Il2CppCodeGenWriteBarrier((&___GF_6), value);
	}

	inline static int32_t get_offset_of_AG_7() { return static_cast<int32_t>(offsetof(CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51, ___AG_7)); }
	inline AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D * get_AG_7() const { return ___AG_7; }
	inline AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D ** get_address_of_AG_7() { return &___AG_7; }
	inline void set_AG_7(AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D * value)
	{
		___AG_7 = value;
		Il2CppCodeGenWriteBarrier((&___AG_7), value);
	}

	inline static int32_t get_offset_of_playedTimes_8() { return static_cast<int32_t>(offsetof(CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51, ___playedTimes_8)); }
	inline int32_t get_playedTimes_8() const { return ___playedTimes_8; }
	inline int32_t* get_address_of_playedTimes_8() { return &___playedTimes_8; }
	inline void set_playedTimes_8(int32_t value)
	{
		___playedTimes_8 = value;
	}

	inline static int32_t get_offset_of_n1_9() { return static_cast<int32_t>(offsetof(CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51, ___n1_9)); }
	inline float get_n1_9() const { return ___n1_9; }
	inline float* get_address_of_n1_9() { return &___n1_9; }
	inline void set_n1_9(float value)
	{
		___n1_9 = value;
	}

	inline static int32_t get_offset_of_n2_10() { return static_cast<int32_t>(offsetof(CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51, ___n2_10)); }
	inline float get_n2_10() const { return ___n2_10; }
	inline float* get_address_of_n2_10() { return &___n2_10; }
	inline void set_n2_10(float value)
	{
		___n2_10 = value;
	}

	inline static int32_t get_offset_of_n3_11() { return static_cast<int32_t>(offsetof(CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51, ___n3_11)); }
	inline float get_n3_11() const { return ___n3_11; }
	inline float* get_address_of_n3_11() { return &___n3_11; }
	inline void set_n3_11(float value)
	{
		___n3_11 = value;
	}

	inline static int32_t get_offset_of_n4_12() { return static_cast<int32_t>(offsetof(CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51, ___n4_12)); }
	inline float get_n4_12() const { return ___n4_12; }
	inline float* get_address_of_n4_12() { return &___n4_12; }
	inline void set_n4_12(float value)
	{
		___n4_12 = value;
	}

	inline static int32_t get_offset_of_n5_13() { return static_cast<int32_t>(offsetof(CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51, ___n5_13)); }
	inline float get_n5_13() const { return ___n5_13; }
	inline float* get_address_of_n5_13() { return &___n5_13; }
	inline void set_n5_13(float value)
	{
		___n5_13 = value;
	}

	inline static int32_t get_offset_of_n6_14() { return static_cast<int32_t>(offsetof(CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51, ___n6_14)); }
	inline float get_n6_14() const { return ___n6_14; }
	inline float* get_address_of_n6_14() { return &___n6_14; }
	inline void set_n6_14(float value)
	{
		___n6_14 = value;
	}

	inline static int32_t get_offset_of_n7_15() { return static_cast<int32_t>(offsetof(CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51, ___n7_15)); }
	inline float get_n7_15() const { return ___n7_15; }
	inline float* get_address_of_n7_15() { return &___n7_15; }
	inline void set_n7_15(float value)
	{
		___n7_15 = value;
	}

	inline static int32_t get_offset_of_nUntilEffects_16() { return static_cast<int32_t>(offsetof(CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51, ___nUntilEffects_16)); }
	inline int32_t get_nUntilEffects_16() const { return ___nUntilEffects_16; }
	inline int32_t* get_address_of_nUntilEffects_16() { return &___nUntilEffects_16; }
	inline void set_nUntilEffects_16(int32_t value)
	{
		___nUntilEffects_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAGLITCH_T766226B76251A4C6BC42452E60B8A4FFE8A4DB51_H
#ifndef CAMERAMOVEMENT_T20ED5C3DA8B48B3D044213B5BFB0A101154F96E4_H
#define CAMERAMOVEMENT_T20ED5C3DA8B48B3D044213B5BFB0A101154F96E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraMovement
struct  CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Rigidbody2D CameraMovement::rb
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___rb_4;
	// System.Single CameraMovement::fallSpeed
	float ___fallSpeed_5;

public:
	inline static int32_t get_offset_of_rb_4() { return static_cast<int32_t>(offsetof(CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4, ___rb_4)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_rb_4() const { return ___rb_4; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_rb_4() { return &___rb_4; }
	inline void set_rb_4(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___rb_4 = value;
		Il2CppCodeGenWriteBarrier((&___rb_4), value);
	}

	inline static int32_t get_offset_of_fallSpeed_5() { return static_cast<int32_t>(offsetof(CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4, ___fallSpeed_5)); }
	inline float get_fallSpeed_5() const { return ___fallSpeed_5; }
	inline float* get_address_of_fallSpeed_5() { return &___fallSpeed_5; }
	inline void set_fallSpeed_5(float value)
	{
		___fallSpeed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMOVEMENT_T20ED5C3DA8B48B3D044213B5BFB0A101154F96E4_H
#ifndef COINMAGNET_T15DFE535293F6D06BF459D88CCC778DB5E954A26_H
#define COINMAGNET_T15DFE535293F6D06BF459D88CCC778DB5E954A26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoinMagnet
struct  CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single CoinMagnet::duration
	float ___duration_4;
	// UnityEngine.GameObject CoinMagnet::explodeParticleEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___explodeParticleEffect_5;
	// UnityEngine.GameObject CoinMagnet::explodepickupEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___explodepickupEffect_6;
	// UnityEngine.GameObject CoinMagnet::Player
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Player_7;
	// PlayerController CoinMagnet::playerScript
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * ___playerScript_8;
	// SFXManager CoinMagnet::SFXM
	SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * ___SFXM_9;
	// GameManager CoinMagnet::GM
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___GM_10;
	// UnityEngine.SpriteRenderer CoinMagnet::playerSR
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___playerSR_12;
	// System.Single CoinMagnet::cduration
	float ___cduration_13;
	// System.Single CoinMagnet::t
	float ___t_14;
	// UnityEngine.Color CoinMagnet::playerBlankColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___playerBlankColor_15;
	// System.Boolean CoinMagnet::playerIsFlashing
	bool ___playerIsFlashing_16;
	// CrazyColorChange CoinMagnet::CCC
	CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445 * ___CCC_17;
	// UnityEngine.Animator CoinMagnet::anim
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___anim_18;

public:
	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}

	inline static int32_t get_offset_of_explodeParticleEffect_5() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26, ___explodeParticleEffect_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_explodeParticleEffect_5() const { return ___explodeParticleEffect_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_explodeParticleEffect_5() { return &___explodeParticleEffect_5; }
	inline void set_explodeParticleEffect_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___explodeParticleEffect_5 = value;
		Il2CppCodeGenWriteBarrier((&___explodeParticleEffect_5), value);
	}

	inline static int32_t get_offset_of_explodepickupEffect_6() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26, ___explodepickupEffect_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_explodepickupEffect_6() const { return ___explodepickupEffect_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_explodepickupEffect_6() { return &___explodepickupEffect_6; }
	inline void set_explodepickupEffect_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___explodepickupEffect_6 = value;
		Il2CppCodeGenWriteBarrier((&___explodepickupEffect_6), value);
	}

	inline static int32_t get_offset_of_Player_7() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26, ___Player_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Player_7() const { return ___Player_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Player_7() { return &___Player_7; }
	inline void set_Player_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Player_7 = value;
		Il2CppCodeGenWriteBarrier((&___Player_7), value);
	}

	inline static int32_t get_offset_of_playerScript_8() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26, ___playerScript_8)); }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * get_playerScript_8() const { return ___playerScript_8; }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 ** get_address_of_playerScript_8() { return &___playerScript_8; }
	inline void set_playerScript_8(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * value)
	{
		___playerScript_8 = value;
		Il2CppCodeGenWriteBarrier((&___playerScript_8), value);
	}

	inline static int32_t get_offset_of_SFXM_9() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26, ___SFXM_9)); }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * get_SFXM_9() const { return ___SFXM_9; }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD ** get_address_of_SFXM_9() { return &___SFXM_9; }
	inline void set_SFXM_9(SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * value)
	{
		___SFXM_9 = value;
		Il2CppCodeGenWriteBarrier((&___SFXM_9), value);
	}

	inline static int32_t get_offset_of_GM_10() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26, ___GM_10)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_GM_10() const { return ___GM_10; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_GM_10() { return &___GM_10; }
	inline void set_GM_10(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___GM_10 = value;
		Il2CppCodeGenWriteBarrier((&___GM_10), value);
	}

	inline static int32_t get_offset_of_playerSR_12() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26, ___playerSR_12)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_playerSR_12() const { return ___playerSR_12; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_playerSR_12() { return &___playerSR_12; }
	inline void set_playerSR_12(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___playerSR_12 = value;
		Il2CppCodeGenWriteBarrier((&___playerSR_12), value);
	}

	inline static int32_t get_offset_of_cduration_13() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26, ___cduration_13)); }
	inline float get_cduration_13() const { return ___cduration_13; }
	inline float* get_address_of_cduration_13() { return &___cduration_13; }
	inline void set_cduration_13(float value)
	{
		___cduration_13 = value;
	}

	inline static int32_t get_offset_of_t_14() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26, ___t_14)); }
	inline float get_t_14() const { return ___t_14; }
	inline float* get_address_of_t_14() { return &___t_14; }
	inline void set_t_14(float value)
	{
		___t_14 = value;
	}

	inline static int32_t get_offset_of_playerBlankColor_15() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26, ___playerBlankColor_15)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_playerBlankColor_15() const { return ___playerBlankColor_15; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_playerBlankColor_15() { return &___playerBlankColor_15; }
	inline void set_playerBlankColor_15(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___playerBlankColor_15 = value;
	}

	inline static int32_t get_offset_of_playerIsFlashing_16() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26, ___playerIsFlashing_16)); }
	inline bool get_playerIsFlashing_16() const { return ___playerIsFlashing_16; }
	inline bool* get_address_of_playerIsFlashing_16() { return &___playerIsFlashing_16; }
	inline void set_playerIsFlashing_16(bool value)
	{
		___playerIsFlashing_16 = value;
	}

	inline static int32_t get_offset_of_CCC_17() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26, ___CCC_17)); }
	inline CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445 * get_CCC_17() const { return ___CCC_17; }
	inline CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445 ** get_address_of_CCC_17() { return &___CCC_17; }
	inline void set_CCC_17(CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445 * value)
	{
		___CCC_17 = value;
		Il2CppCodeGenWriteBarrier((&___CCC_17), value);
	}

	inline static int32_t get_offset_of_anim_18() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26, ___anim_18)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_anim_18() const { return ___anim_18; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_anim_18() { return &___anim_18; }
	inline void set_anim_18(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___anim_18 = value;
		Il2CppCodeGenWriteBarrier((&___anim_18), value);
	}
};

struct CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26_StaticFields
{
public:
	// System.Single CoinMagnet::OldMoveSpeed
	float ___OldMoveSpeed_11;

public:
	inline static int32_t get_offset_of_OldMoveSpeed_11() { return static_cast<int32_t>(offsetof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26_StaticFields, ___OldMoveSpeed_11)); }
	inline float get_OldMoveSpeed_11() const { return ___OldMoveSpeed_11; }
	inline float* get_address_of_OldMoveSpeed_11() { return &___OldMoveSpeed_11; }
	inline void set_OldMoveSpeed_11(float value)
	{
		___OldMoveSpeed_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COINMAGNET_T15DFE535293F6D06BF459D88CCC778DB5E954A26_H
#ifndef COLORLERP_T0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2_H
#define COLORLERP_T0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorLerp
struct  ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single ColorLerp::smooth
	float ___smooth_4;
	// UnityEngine.Color ColorLerp::newColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___newColor_5;
	// UnityEngine.SpriteRenderer ColorLerp::SR
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___SR_6;
	// System.Single ColorLerp::fadeToBlueAmount
	float ___fadeToBlueAmount_7;
	// System.Single ColorLerp::fadeToGreenAmount
	float ___fadeToGreenAmount_8;
	// System.Single ColorLerp::fadeToRedAmount
	float ___fadeToRedAmount_9;
	// System.Single ColorLerp::fadingSpeed
	float ___fadingSpeed_10;

public:
	inline static int32_t get_offset_of_smooth_4() { return static_cast<int32_t>(offsetof(ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2, ___smooth_4)); }
	inline float get_smooth_4() const { return ___smooth_4; }
	inline float* get_address_of_smooth_4() { return &___smooth_4; }
	inline void set_smooth_4(float value)
	{
		___smooth_4 = value;
	}

	inline static int32_t get_offset_of_newColor_5() { return static_cast<int32_t>(offsetof(ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2, ___newColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_newColor_5() const { return ___newColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_newColor_5() { return &___newColor_5; }
	inline void set_newColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___newColor_5 = value;
	}

	inline static int32_t get_offset_of_SR_6() { return static_cast<int32_t>(offsetof(ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2, ___SR_6)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_SR_6() const { return ___SR_6; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_SR_6() { return &___SR_6; }
	inline void set_SR_6(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___SR_6 = value;
		Il2CppCodeGenWriteBarrier((&___SR_6), value);
	}

	inline static int32_t get_offset_of_fadeToBlueAmount_7() { return static_cast<int32_t>(offsetof(ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2, ___fadeToBlueAmount_7)); }
	inline float get_fadeToBlueAmount_7() const { return ___fadeToBlueAmount_7; }
	inline float* get_address_of_fadeToBlueAmount_7() { return &___fadeToBlueAmount_7; }
	inline void set_fadeToBlueAmount_7(float value)
	{
		___fadeToBlueAmount_7 = value;
	}

	inline static int32_t get_offset_of_fadeToGreenAmount_8() { return static_cast<int32_t>(offsetof(ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2, ___fadeToGreenAmount_8)); }
	inline float get_fadeToGreenAmount_8() const { return ___fadeToGreenAmount_8; }
	inline float* get_address_of_fadeToGreenAmount_8() { return &___fadeToGreenAmount_8; }
	inline void set_fadeToGreenAmount_8(float value)
	{
		___fadeToGreenAmount_8 = value;
	}

	inline static int32_t get_offset_of_fadeToRedAmount_9() { return static_cast<int32_t>(offsetof(ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2, ___fadeToRedAmount_9)); }
	inline float get_fadeToRedAmount_9() const { return ___fadeToRedAmount_9; }
	inline float* get_address_of_fadeToRedAmount_9() { return &___fadeToRedAmount_9; }
	inline void set_fadeToRedAmount_9(float value)
	{
		___fadeToRedAmount_9 = value;
	}

	inline static int32_t get_offset_of_fadingSpeed_10() { return static_cast<int32_t>(offsetof(ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2, ___fadingSpeed_10)); }
	inline float get_fadingSpeed_10() const { return ___fadingSpeed_10; }
	inline float* get_address_of_fadingSpeed_10() { return &___fadingSpeed_10; }
	inline void set_fadingSpeed_10(float value)
	{
		___fadingSpeed_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORLERP_T0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2_H
#ifndef CRAZYCOLORCHANGE_T686A4AF5D1A6ED35DF0A461E7A7838D8094D4445_H
#define CRAZYCOLORCHANGE_T686A4AF5D1A6ED35DF0A461E7A7838D8094D4445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CrazyColorChange
struct  CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.SpriteRenderer CrazyColorChange::playerSR
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___playerSR_4;
	// System.Boolean CrazyColorChange::isCrazyColorChanging
	bool ___isCrazyColorChanging_5;

public:
	inline static int32_t get_offset_of_playerSR_4() { return static_cast<int32_t>(offsetof(CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445, ___playerSR_4)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_playerSR_4() const { return ___playerSR_4; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_playerSR_4() { return &___playerSR_4; }
	inline void set_playerSR_4(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___playerSR_4 = value;
		Il2CppCodeGenWriteBarrier((&___playerSR_4), value);
	}

	inline static int32_t get_offset_of_isCrazyColorChanging_5() { return static_cast<int32_t>(offsetof(CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445, ___isCrazyColorChanging_5)); }
	inline bool get_isCrazyColorChanging_5() const { return ___isCrazyColorChanging_5; }
	inline bool* get_address_of_isCrazyColorChanging_5() { return &___isCrazyColorChanging_5; }
	inline void set_isCrazyColorChanging_5(bool value)
	{
		___isCrazyColorChanging_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRAZYCOLORCHANGE_T686A4AF5D1A6ED35DF0A461E7A7838D8094D4445_H
#ifndef DESTROYFINISHEDPARTICLE_T0886D79AF36A4C48739BDBF9F111AE953884C9DD_H
#define DESTROYFINISHEDPARTICLE_T0886D79AF36A4C48739BDBF9F111AE953884C9DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyFinishedParticle
struct  DestroyFinishedParticle_t0886D79AF36A4C48739BDBF9F111AE953884C9DD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.ParticleSystem DestroyFinishedParticle::thisParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___thisParticleSystem_4;

public:
	inline static int32_t get_offset_of_thisParticleSystem_4() { return static_cast<int32_t>(offsetof(DestroyFinishedParticle_t0886D79AF36A4C48739BDBF9F111AE953884C9DD, ___thisParticleSystem_4)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_thisParticleSystem_4() const { return ___thisParticleSystem_4; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_thisParticleSystem_4() { return &___thisParticleSystem_4; }
	inline void set_thisParticleSystem_4(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___thisParticleSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___thisParticleSystem_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYFINISHEDPARTICLE_T0886D79AF36A4C48739BDBF9F111AE953884C9DD_H
#ifndef DESTROYGAMEOBJECTOVERTIME_TE562613A730D168CB3500DED69F5192AA8F6D724_H
#define DESTROYGAMEOBJECTOVERTIME_TE562613A730D168CB3500DED69F5192AA8F6D724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyGameObjectOverTime
struct  DestroyGameObjectOverTime_tE562613A730D168CB3500DED69F5192AA8F6D724  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single DestroyGameObjectOverTime::lifetime
	float ___lifetime_4;
	// System.Single DestroyGameObjectOverTime::DfromPlayer
	float ___DfromPlayer_5;
	// PlayerController DestroyGameObjectOverTime::player
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * ___player_6;
	// System.Single DestroyGameObjectOverTime::DY
	float ___DY_7;

public:
	inline static int32_t get_offset_of_lifetime_4() { return static_cast<int32_t>(offsetof(DestroyGameObjectOverTime_tE562613A730D168CB3500DED69F5192AA8F6D724, ___lifetime_4)); }
	inline float get_lifetime_4() const { return ___lifetime_4; }
	inline float* get_address_of_lifetime_4() { return &___lifetime_4; }
	inline void set_lifetime_4(float value)
	{
		___lifetime_4 = value;
	}

	inline static int32_t get_offset_of_DfromPlayer_5() { return static_cast<int32_t>(offsetof(DestroyGameObjectOverTime_tE562613A730D168CB3500DED69F5192AA8F6D724, ___DfromPlayer_5)); }
	inline float get_DfromPlayer_5() const { return ___DfromPlayer_5; }
	inline float* get_address_of_DfromPlayer_5() { return &___DfromPlayer_5; }
	inline void set_DfromPlayer_5(float value)
	{
		___DfromPlayer_5 = value;
	}

	inline static int32_t get_offset_of_player_6() { return static_cast<int32_t>(offsetof(DestroyGameObjectOverTime_tE562613A730D168CB3500DED69F5192AA8F6D724, ___player_6)); }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * get_player_6() const { return ___player_6; }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 ** get_address_of_player_6() { return &___player_6; }
	inline void set_player_6(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * value)
	{
		___player_6 = value;
		Il2CppCodeGenWriteBarrier((&___player_6), value);
	}

	inline static int32_t get_offset_of_DY_7() { return static_cast<int32_t>(offsetof(DestroyGameObjectOverTime_tE562613A730D168CB3500DED69F5192AA8F6D724, ___DY_7)); }
	inline float get_DY_7() const { return ___DY_7; }
	inline float* get_address_of_DY_7() { return &___DY_7; }
	inline void set_DY_7(float value)
	{
		___DY_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYGAMEOBJECTOVERTIME_TE562613A730D168CB3500DED69F5192AA8F6D724_H
#ifndef FADEMANAGER_TAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63_H
#define FADEMANAGER_TAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeManager
struct  FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.CanvasGroup FadeManager::fadeGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___fadeGroup_5;
	// UnityEngine.UI.Image FadeManager::fadeImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___fadeImage_6;
	// System.Boolean FadeManager::startWithFade
	bool ___startWithFade_7;
	// System.Single FadeManager::startFadeTime
	float ___startFadeTime_8;
	// UnityEngine.Color FadeManager::startFadeColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___startFadeColor_9;

public:
	inline static int32_t get_offset_of_fadeGroup_5() { return static_cast<int32_t>(offsetof(FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63, ___fadeGroup_5)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_fadeGroup_5() const { return ___fadeGroup_5; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_fadeGroup_5() { return &___fadeGroup_5; }
	inline void set_fadeGroup_5(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___fadeGroup_5 = value;
		Il2CppCodeGenWriteBarrier((&___fadeGroup_5), value);
	}

	inline static int32_t get_offset_of_fadeImage_6() { return static_cast<int32_t>(offsetof(FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63, ___fadeImage_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_fadeImage_6() const { return ___fadeImage_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_fadeImage_6() { return &___fadeImage_6; }
	inline void set_fadeImage_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___fadeImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___fadeImage_6), value);
	}

	inline static int32_t get_offset_of_startWithFade_7() { return static_cast<int32_t>(offsetof(FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63, ___startWithFade_7)); }
	inline bool get_startWithFade_7() const { return ___startWithFade_7; }
	inline bool* get_address_of_startWithFade_7() { return &___startWithFade_7; }
	inline void set_startWithFade_7(bool value)
	{
		___startWithFade_7 = value;
	}

	inline static int32_t get_offset_of_startFadeTime_8() { return static_cast<int32_t>(offsetof(FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63, ___startFadeTime_8)); }
	inline float get_startFadeTime_8() const { return ___startFadeTime_8; }
	inline float* get_address_of_startFadeTime_8() { return &___startFadeTime_8; }
	inline void set_startFadeTime_8(float value)
	{
		___startFadeTime_8 = value;
	}

	inline static int32_t get_offset_of_startFadeColor_9() { return static_cast<int32_t>(offsetof(FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63, ___startFadeColor_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_startFadeColor_9() const { return ___startFadeColor_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_startFadeColor_9() { return &___startFadeColor_9; }
	inline void set_startFadeColor_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___startFadeColor_9 = value;
	}
};

struct FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63_StaticFields
{
public:
	// FadeManager FadeManager::Instance
	FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63_StaticFields, ___Instance_4)); }
	inline FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63 * get_Instance_4() const { return ___Instance_4; }
	inline FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEMANAGER_TAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63_H
#ifndef FADEMANAGERTEST_T4C9317895745C15FE6D914A97FD0230BA442FB11_H
#define FADEMANAGERTEST_T4C9317895745C15FE6D914A97FD0230BA442FB11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeManagerTest
struct  FadeManagerTest_t4C9317895745C15FE6D914A97FD0230BA442FB11  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEMANAGERTEST_T4C9317895745C15FE6D914A97FD0230BA442FB11_H
#ifndef FLASHINGTMPRO_TC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941_H
#define FLASHINGTMPRO_TC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlashingTMPro
struct  FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TextMeshProUGUI FlashingTMPro::textMesh
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___textMesh_4;
	// System.Single FlashingTMPro::animSpeedInSec
	float ___animSpeedInSec_5;
	// System.Boolean FlashingTMPro::keepAnimating
	bool ___keepAnimating_6;
	// System.Single FlashingTMPro::transitionTime
	float ___transitionTime_7;
	// System.Boolean FlashingTMPro::playerIsFlashing
	bool ___playerIsFlashing_8;
	// GameManager FlashingTMPro::GM
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___GM_9;
	// System.Single FlashingTMPro::T
	float ___T_10;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941, ___textMesh_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}

	inline static int32_t get_offset_of_animSpeedInSec_5() { return static_cast<int32_t>(offsetof(FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941, ___animSpeedInSec_5)); }
	inline float get_animSpeedInSec_5() const { return ___animSpeedInSec_5; }
	inline float* get_address_of_animSpeedInSec_5() { return &___animSpeedInSec_5; }
	inline void set_animSpeedInSec_5(float value)
	{
		___animSpeedInSec_5 = value;
	}

	inline static int32_t get_offset_of_keepAnimating_6() { return static_cast<int32_t>(offsetof(FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941, ___keepAnimating_6)); }
	inline bool get_keepAnimating_6() const { return ___keepAnimating_6; }
	inline bool* get_address_of_keepAnimating_6() { return &___keepAnimating_6; }
	inline void set_keepAnimating_6(bool value)
	{
		___keepAnimating_6 = value;
	}

	inline static int32_t get_offset_of_transitionTime_7() { return static_cast<int32_t>(offsetof(FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941, ___transitionTime_7)); }
	inline float get_transitionTime_7() const { return ___transitionTime_7; }
	inline float* get_address_of_transitionTime_7() { return &___transitionTime_7; }
	inline void set_transitionTime_7(float value)
	{
		___transitionTime_7 = value;
	}

	inline static int32_t get_offset_of_playerIsFlashing_8() { return static_cast<int32_t>(offsetof(FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941, ___playerIsFlashing_8)); }
	inline bool get_playerIsFlashing_8() const { return ___playerIsFlashing_8; }
	inline bool* get_address_of_playerIsFlashing_8() { return &___playerIsFlashing_8; }
	inline void set_playerIsFlashing_8(bool value)
	{
		___playerIsFlashing_8 = value;
	}

	inline static int32_t get_offset_of_GM_9() { return static_cast<int32_t>(offsetof(FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941, ___GM_9)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_GM_9() const { return ___GM_9; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_GM_9() { return &___GM_9; }
	inline void set_GM_9(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___GM_9 = value;
		Il2CppCodeGenWriteBarrier((&___GM_9), value);
	}

	inline static int32_t get_offset_of_T_10() { return static_cast<int32_t>(offsetof(FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941, ___T_10)); }
	inline float get_T_10() const { return ___T_10; }
	inline float* get_address_of_T_10() { return &___T_10; }
	inline void set_T_10(float value)
	{
		___T_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLASHINGTMPRO_TC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941_H
#ifndef GAMECOUNTER_T14CFAEB55D766C8430665F675F336AB4C0D1D89F_H
#define GAMECOUNTER_T14CFAEB55D766C8430665F675F336AB4C0D1D89F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCounter
struct  GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 GameCounter::CyclicplayedGameTimes
	int32_t ___CyclicplayedGameTimes_4;
	// UnityAdsTest GameCounter::UAT
	UnityAdsTest_tB995FE7BE7D9E21056A575559495BF17F06BDA96 * ___UAT_5;
	// System.Int32 GameCounter::playedGameTimes
	int32_t ___playedGameTimes_6;
	// GameManager GameCounter::GM
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___GM_7;
	// PauseMenu GameCounter::PM
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791 * ___PM_8;

public:
	inline static int32_t get_offset_of_CyclicplayedGameTimes_4() { return static_cast<int32_t>(offsetof(GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F, ___CyclicplayedGameTimes_4)); }
	inline int32_t get_CyclicplayedGameTimes_4() const { return ___CyclicplayedGameTimes_4; }
	inline int32_t* get_address_of_CyclicplayedGameTimes_4() { return &___CyclicplayedGameTimes_4; }
	inline void set_CyclicplayedGameTimes_4(int32_t value)
	{
		___CyclicplayedGameTimes_4 = value;
	}

	inline static int32_t get_offset_of_UAT_5() { return static_cast<int32_t>(offsetof(GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F, ___UAT_5)); }
	inline UnityAdsTest_tB995FE7BE7D9E21056A575559495BF17F06BDA96 * get_UAT_5() const { return ___UAT_5; }
	inline UnityAdsTest_tB995FE7BE7D9E21056A575559495BF17F06BDA96 ** get_address_of_UAT_5() { return &___UAT_5; }
	inline void set_UAT_5(UnityAdsTest_tB995FE7BE7D9E21056A575559495BF17F06BDA96 * value)
	{
		___UAT_5 = value;
		Il2CppCodeGenWriteBarrier((&___UAT_5), value);
	}

	inline static int32_t get_offset_of_playedGameTimes_6() { return static_cast<int32_t>(offsetof(GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F, ___playedGameTimes_6)); }
	inline int32_t get_playedGameTimes_6() const { return ___playedGameTimes_6; }
	inline int32_t* get_address_of_playedGameTimes_6() { return &___playedGameTimes_6; }
	inline void set_playedGameTimes_6(int32_t value)
	{
		___playedGameTimes_6 = value;
	}

	inline static int32_t get_offset_of_GM_7() { return static_cast<int32_t>(offsetof(GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F, ___GM_7)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_GM_7() const { return ___GM_7; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_GM_7() { return &___GM_7; }
	inline void set_GM_7(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___GM_7 = value;
		Il2CppCodeGenWriteBarrier((&___GM_7), value);
	}

	inline static int32_t get_offset_of_PM_8() { return static_cast<int32_t>(offsetof(GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F, ___PM_8)); }
	inline PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791 * get_PM_8() const { return ___PM_8; }
	inline PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791 ** get_address_of_PM_8() { return &___PM_8; }
	inline void set_PM_8(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791 * value)
	{
		___PM_8 = value;
		Il2CppCodeGenWriteBarrier((&___PM_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECOUNTER_T14CFAEB55D766C8430665F675F336AB4C0D1D89F_H
#ifndef GAMEMANAGER_TAC830B937D5E37F47803FE8AB44CAB0762B77B89_H
#define GAMEMANAGER_TAC830B937D5E37F47803FE8AB44CAB0762B77B89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean GameManager::gameHasEnded
	bool ___gameHasEnded_4;
	// System.Single GameManager::restartDelay
	float ___restartDelay_5;
	// UnityEngine.GameObject GameManager::DeathMenu
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DeathMenu_6;
	// SFXManager GameManager::SFXM
	SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * ___SFXM_7;
	// PauseMenu GameManager::PM
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791 * ___PM_8;
	// UnityEngine.GameObject GameManager::playerObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___playerObject_9;
	// PlayerController GameManager::player
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * ___player_10;
	// UnityEngine.GameObject GameManager::Camera
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Camera_11;
	// CameraMovement GameManager::camF
	CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4 * ___camF_12;
	// UnityEngine.GameObject GameManager::BG3
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___BG3_13;
	// CameraMovement GameManager::BG3F
	CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4 * ___BG3F_14;
	// System.Single GameManager::moveSpeed
	float ___moveSpeed_15;
	// System.Single GameManager::accel
	float ___accel_16;
	// System.Single GameManager::n
	float ___n_17;
	// System.Single GameManager::DistanceY
	float ___DistanceY_18;
	// System.Boolean GameManager::paused
	bool ___paused_19;
	// System.Single GameManager::T
	float ___T_20;
	// System.Single GameManager::CyclicT
	float ___CyclicT_21;
	// UnityEngine.GameObject GameManager::start
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___start_22;
	// System.Boolean GameManager::Turbo
	bool ___Turbo_24;
	// System.Single GameManager::TurboBoostMult
	float ___TurboBoostMult_25;
	// AudioManager GameManager::AM
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * ___AM_26;
	// GameCounter GameManager::GC
	GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F * ___GC_27;

public:
	inline static int32_t get_offset_of_gameHasEnded_4() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___gameHasEnded_4)); }
	inline bool get_gameHasEnded_4() const { return ___gameHasEnded_4; }
	inline bool* get_address_of_gameHasEnded_4() { return &___gameHasEnded_4; }
	inline void set_gameHasEnded_4(bool value)
	{
		___gameHasEnded_4 = value;
	}

	inline static int32_t get_offset_of_restartDelay_5() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___restartDelay_5)); }
	inline float get_restartDelay_5() const { return ___restartDelay_5; }
	inline float* get_address_of_restartDelay_5() { return &___restartDelay_5; }
	inline void set_restartDelay_5(float value)
	{
		___restartDelay_5 = value;
	}

	inline static int32_t get_offset_of_DeathMenu_6() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___DeathMenu_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DeathMenu_6() const { return ___DeathMenu_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DeathMenu_6() { return &___DeathMenu_6; }
	inline void set_DeathMenu_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DeathMenu_6 = value;
		Il2CppCodeGenWriteBarrier((&___DeathMenu_6), value);
	}

	inline static int32_t get_offset_of_SFXM_7() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___SFXM_7)); }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * get_SFXM_7() const { return ___SFXM_7; }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD ** get_address_of_SFXM_7() { return &___SFXM_7; }
	inline void set_SFXM_7(SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * value)
	{
		___SFXM_7 = value;
		Il2CppCodeGenWriteBarrier((&___SFXM_7), value);
	}

	inline static int32_t get_offset_of_PM_8() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___PM_8)); }
	inline PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791 * get_PM_8() const { return ___PM_8; }
	inline PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791 ** get_address_of_PM_8() { return &___PM_8; }
	inline void set_PM_8(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791 * value)
	{
		___PM_8 = value;
		Il2CppCodeGenWriteBarrier((&___PM_8), value);
	}

	inline static int32_t get_offset_of_playerObject_9() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___playerObject_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_playerObject_9() const { return ___playerObject_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_playerObject_9() { return &___playerObject_9; }
	inline void set_playerObject_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___playerObject_9 = value;
		Il2CppCodeGenWriteBarrier((&___playerObject_9), value);
	}

	inline static int32_t get_offset_of_player_10() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___player_10)); }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * get_player_10() const { return ___player_10; }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 ** get_address_of_player_10() { return &___player_10; }
	inline void set_player_10(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * value)
	{
		___player_10 = value;
		Il2CppCodeGenWriteBarrier((&___player_10), value);
	}

	inline static int32_t get_offset_of_Camera_11() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___Camera_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Camera_11() const { return ___Camera_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Camera_11() { return &___Camera_11; }
	inline void set_Camera_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Camera_11 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_11), value);
	}

	inline static int32_t get_offset_of_camF_12() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___camF_12)); }
	inline CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4 * get_camF_12() const { return ___camF_12; }
	inline CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4 ** get_address_of_camF_12() { return &___camF_12; }
	inline void set_camF_12(CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4 * value)
	{
		___camF_12 = value;
		Il2CppCodeGenWriteBarrier((&___camF_12), value);
	}

	inline static int32_t get_offset_of_BG3_13() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___BG3_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_BG3_13() const { return ___BG3_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_BG3_13() { return &___BG3_13; }
	inline void set_BG3_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___BG3_13 = value;
		Il2CppCodeGenWriteBarrier((&___BG3_13), value);
	}

	inline static int32_t get_offset_of_BG3F_14() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___BG3F_14)); }
	inline CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4 * get_BG3F_14() const { return ___BG3F_14; }
	inline CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4 ** get_address_of_BG3F_14() { return &___BG3F_14; }
	inline void set_BG3F_14(CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4 * value)
	{
		___BG3F_14 = value;
		Il2CppCodeGenWriteBarrier((&___BG3F_14), value);
	}

	inline static int32_t get_offset_of_moveSpeed_15() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___moveSpeed_15)); }
	inline float get_moveSpeed_15() const { return ___moveSpeed_15; }
	inline float* get_address_of_moveSpeed_15() { return &___moveSpeed_15; }
	inline void set_moveSpeed_15(float value)
	{
		___moveSpeed_15 = value;
	}

	inline static int32_t get_offset_of_accel_16() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___accel_16)); }
	inline float get_accel_16() const { return ___accel_16; }
	inline float* get_address_of_accel_16() { return &___accel_16; }
	inline void set_accel_16(float value)
	{
		___accel_16 = value;
	}

	inline static int32_t get_offset_of_n_17() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___n_17)); }
	inline float get_n_17() const { return ___n_17; }
	inline float* get_address_of_n_17() { return &___n_17; }
	inline void set_n_17(float value)
	{
		___n_17 = value;
	}

	inline static int32_t get_offset_of_DistanceY_18() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___DistanceY_18)); }
	inline float get_DistanceY_18() const { return ___DistanceY_18; }
	inline float* get_address_of_DistanceY_18() { return &___DistanceY_18; }
	inline void set_DistanceY_18(float value)
	{
		___DistanceY_18 = value;
	}

	inline static int32_t get_offset_of_paused_19() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___paused_19)); }
	inline bool get_paused_19() const { return ___paused_19; }
	inline bool* get_address_of_paused_19() { return &___paused_19; }
	inline void set_paused_19(bool value)
	{
		___paused_19 = value;
	}

	inline static int32_t get_offset_of_T_20() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___T_20)); }
	inline float get_T_20() const { return ___T_20; }
	inline float* get_address_of_T_20() { return &___T_20; }
	inline void set_T_20(float value)
	{
		___T_20 = value;
	}

	inline static int32_t get_offset_of_CyclicT_21() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___CyclicT_21)); }
	inline float get_CyclicT_21() const { return ___CyclicT_21; }
	inline float* get_address_of_CyclicT_21() { return &___CyclicT_21; }
	inline void set_CyclicT_21(float value)
	{
		___CyclicT_21 = value;
	}

	inline static int32_t get_offset_of_start_22() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___start_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_start_22() const { return ___start_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_start_22() { return &___start_22; }
	inline void set_start_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___start_22 = value;
		Il2CppCodeGenWriteBarrier((&___start_22), value);
	}

	inline static int32_t get_offset_of_Turbo_24() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___Turbo_24)); }
	inline bool get_Turbo_24() const { return ___Turbo_24; }
	inline bool* get_address_of_Turbo_24() { return &___Turbo_24; }
	inline void set_Turbo_24(bool value)
	{
		___Turbo_24 = value;
	}

	inline static int32_t get_offset_of_TurboBoostMult_25() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___TurboBoostMult_25)); }
	inline float get_TurboBoostMult_25() const { return ___TurboBoostMult_25; }
	inline float* get_address_of_TurboBoostMult_25() { return &___TurboBoostMult_25; }
	inline void set_TurboBoostMult_25(float value)
	{
		___TurboBoostMult_25 = value;
	}

	inline static int32_t get_offset_of_AM_26() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___AM_26)); }
	inline AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * get_AM_26() const { return ___AM_26; }
	inline AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 ** get_address_of_AM_26() { return &___AM_26; }
	inline void set_AM_26(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * value)
	{
		___AM_26 = value;
		Il2CppCodeGenWriteBarrier((&___AM_26), value);
	}

	inline static int32_t get_offset_of_GC_27() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89, ___GC_27)); }
	inline GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F * get_GC_27() const { return ___GC_27; }
	inline GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F ** get_address_of_GC_27() { return &___GC_27; }
	inline void set_GC_27(GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F * value)
	{
		___GC_27 = value;
		Il2CppCodeGenWriteBarrier((&___GC_27), value);
	}
};

struct GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89_StaticFields
{
public:
	// System.Single GameManager::moveSpeedinitial
	float ___moveSpeedinitial_23;

public:
	inline static int32_t get_offset_of_moveSpeedinitial_23() { return static_cast<int32_t>(offsetof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89_StaticFields, ___moveSpeedinitial_23)); }
	inline float get_moveSpeedinitial_23() const { return ___moveSpeedinitial_23; }
	inline float* get_address_of_moveSpeedinitial_23() { return &___moveSpeedinitial_23; }
	inline void set_moveSpeedinitial_23(float value)
	{
		___moveSpeedinitial_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_TAC830B937D5E37F47803FE8AB44CAB0762B77B89_H
#ifndef GLITCHFX_T6A31DE633BFF8805214F625E768930C49D0EDC8A_H
#define GLITCHFX_T6A31DE633BFF8805214F625E768930C49D0EDC8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlitchFx
struct  GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single GlitchFx::_intensity
	float ____intensity_4;
	// UnityEngine.Shader GlitchFx::shader
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___shader_5;
	// UnityEngine.Material GlitchFx::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_6;
	// UnityEngine.Texture2D GlitchFx::noiseTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___noiseTexture_7;
	// UnityEngine.RenderTexture GlitchFx::oldFrame1
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___oldFrame1_8;
	// UnityEngine.RenderTexture GlitchFx::oldFrame2
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___oldFrame2_9;
	// System.Int32 GlitchFx::frameCount
	int32_t ___frameCount_10;

public:
	inline static int32_t get_offset_of__intensity_4() { return static_cast<int32_t>(offsetof(GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A, ____intensity_4)); }
	inline float get__intensity_4() const { return ____intensity_4; }
	inline float* get_address_of__intensity_4() { return &____intensity_4; }
	inline void set__intensity_4(float value)
	{
		____intensity_4 = value;
	}

	inline static int32_t get_offset_of_shader_5() { return static_cast<int32_t>(offsetof(GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A, ___shader_5)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_shader_5() const { return ___shader_5; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_shader_5() { return &___shader_5; }
	inline void set_shader_5(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___shader_5 = value;
		Il2CppCodeGenWriteBarrier((&___shader_5), value);
	}

	inline static int32_t get_offset_of_material_6() { return static_cast<int32_t>(offsetof(GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A, ___material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_6() const { return ___material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_6() { return &___material_6; }
	inline void set_material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_6 = value;
		Il2CppCodeGenWriteBarrier((&___material_6), value);
	}

	inline static int32_t get_offset_of_noiseTexture_7() { return static_cast<int32_t>(offsetof(GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A, ___noiseTexture_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_noiseTexture_7() const { return ___noiseTexture_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_noiseTexture_7() { return &___noiseTexture_7; }
	inline void set_noiseTexture_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___noiseTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___noiseTexture_7), value);
	}

	inline static int32_t get_offset_of_oldFrame1_8() { return static_cast<int32_t>(offsetof(GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A, ___oldFrame1_8)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_oldFrame1_8() const { return ___oldFrame1_8; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_oldFrame1_8() { return &___oldFrame1_8; }
	inline void set_oldFrame1_8(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___oldFrame1_8 = value;
		Il2CppCodeGenWriteBarrier((&___oldFrame1_8), value);
	}

	inline static int32_t get_offset_of_oldFrame2_9() { return static_cast<int32_t>(offsetof(GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A, ___oldFrame2_9)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_oldFrame2_9() const { return ___oldFrame2_9; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_oldFrame2_9() { return &___oldFrame2_9; }
	inline void set_oldFrame2_9(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___oldFrame2_9 = value;
		Il2CppCodeGenWriteBarrier((&___oldFrame2_9), value);
	}

	inline static int32_t get_offset_of_frameCount_10() { return static_cast<int32_t>(offsetof(GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A, ___frameCount_10)); }
	inline int32_t get_frameCount_10() const { return ___frameCount_10; }
	inline int32_t* get_address_of_frameCount_10() { return &___frameCount_10; }
	inline void set_frameCount_10(int32_t value)
	{
		___frameCount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLITCHFX_T6A31DE633BFF8805214F625E768930C49D0EDC8A_H
#ifndef GROUNDSPAWNER_T1C5CDDD4D769EDD3A69E8C7145A2D18C92D2B934_H
#define GROUNDSPAWNER_T1C5CDDD4D769EDD3A69E8C7145A2D18C92D2B934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GroundSpawner
struct  GroundSpawner_t1C5CDDD4D769EDD3A69E8C7145A2D18C92D2B934  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject GroundSpawner::track
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___track_4;
	// System.Boolean GroundSpawner::spawned
	bool ___spawned_5;
	// System.Single GroundSpawner::displacementY
	float ___displacementY_6;

public:
	inline static int32_t get_offset_of_track_4() { return static_cast<int32_t>(offsetof(GroundSpawner_t1C5CDDD4D769EDD3A69E8C7145A2D18C92D2B934, ___track_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_track_4() const { return ___track_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_track_4() { return &___track_4; }
	inline void set_track_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___track_4 = value;
		Il2CppCodeGenWriteBarrier((&___track_4), value);
	}

	inline static int32_t get_offset_of_spawned_5() { return static_cast<int32_t>(offsetof(GroundSpawner_t1C5CDDD4D769EDD3A69E8C7145A2D18C92D2B934, ___spawned_5)); }
	inline bool get_spawned_5() const { return ___spawned_5; }
	inline bool* get_address_of_spawned_5() { return &___spawned_5; }
	inline void set_spawned_5(bool value)
	{
		___spawned_5 = value;
	}

	inline static int32_t get_offset_of_displacementY_6() { return static_cast<int32_t>(offsetof(GroundSpawner_t1C5CDDD4D769EDD3A69E8C7145A2D18C92D2B934, ___displacementY_6)); }
	inline float get_displacementY_6() const { return ___displacementY_6; }
	inline float* get_address_of_displacementY_6() { return &___displacementY_6; }
	inline void set_displacementY_6(float value)
	{
		___displacementY_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUNDSPAWNER_T1C5CDDD4D769EDD3A69E8C7145A2D18C92D2B934_H
#ifndef GYROMANAGER_T42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B_H
#define GYROMANAGER_T42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GyroManager
struct  GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Gyroscope GyroManager::gyro
	Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC * ___gyro_5;
	// UnityEngine.Quaternion GyroManager::rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_6;
	// System.Boolean GyroManager::gyroActive
	bool ___gyroActive_7;

public:
	inline static int32_t get_offset_of_gyro_5() { return static_cast<int32_t>(offsetof(GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B, ___gyro_5)); }
	inline Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC * get_gyro_5() const { return ___gyro_5; }
	inline Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC ** get_address_of_gyro_5() { return &___gyro_5; }
	inline void set_gyro_5(Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC * value)
	{
		___gyro_5 = value;
		Il2CppCodeGenWriteBarrier((&___gyro_5), value);
	}

	inline static int32_t get_offset_of_rotation_6() { return static_cast<int32_t>(offsetof(GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B, ___rotation_6)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotation_6() const { return ___rotation_6; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotation_6() { return &___rotation_6; }
	inline void set_rotation_6(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotation_6 = value;
	}

	inline static int32_t get_offset_of_gyroActive_7() { return static_cast<int32_t>(offsetof(GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B, ___gyroActive_7)); }
	inline bool get_gyroActive_7() const { return ___gyroActive_7; }
	inline bool* get_address_of_gyroActive_7() { return &___gyroActive_7; }
	inline void set_gyroActive_7(bool value)
	{
		___gyroActive_7 = value;
	}
};

struct GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B_StaticFields
{
public:
	// GyroManager GyroManager::instance
	GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B_StaticFields, ___instance_4)); }
	inline GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B * get_instance_4() const { return ___instance_4; }
	inline GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROMANAGER_T42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B_H
#ifndef HIGHSCORE_T5A992FDEECF661ADB50FD15D68A56AD0041DE2B0_H
#define HIGHSCORE_T5A992FDEECF661ADB50FD15D68A56AD0041DE2B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Highscore
struct  Highscore_t5A992FDEECF661ADB50FD15D68A56AD0041DE2B0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Score1 Highscore::S1
	Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119 * ___S1_4;
	// TMPro.TextMeshProUGUI Highscore::scoreCounter
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___scoreCounter_5;
	// TMPro.TextMeshProUGUI Highscore::highScore
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___highScore_6;
	// PlayerController Highscore::player
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * ___player_7;
	// TMPro.TextMeshProUGUI Highscore::ShardScore
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___ShardScore_8;

public:
	inline static int32_t get_offset_of_S1_4() { return static_cast<int32_t>(offsetof(Highscore_t5A992FDEECF661ADB50FD15D68A56AD0041DE2B0, ___S1_4)); }
	inline Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119 * get_S1_4() const { return ___S1_4; }
	inline Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119 ** get_address_of_S1_4() { return &___S1_4; }
	inline void set_S1_4(Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119 * value)
	{
		___S1_4 = value;
		Il2CppCodeGenWriteBarrier((&___S1_4), value);
	}

	inline static int32_t get_offset_of_scoreCounter_5() { return static_cast<int32_t>(offsetof(Highscore_t5A992FDEECF661ADB50FD15D68A56AD0041DE2B0, ___scoreCounter_5)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_scoreCounter_5() const { return ___scoreCounter_5; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_scoreCounter_5() { return &___scoreCounter_5; }
	inline void set_scoreCounter_5(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___scoreCounter_5 = value;
		Il2CppCodeGenWriteBarrier((&___scoreCounter_5), value);
	}

	inline static int32_t get_offset_of_highScore_6() { return static_cast<int32_t>(offsetof(Highscore_t5A992FDEECF661ADB50FD15D68A56AD0041DE2B0, ___highScore_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_highScore_6() const { return ___highScore_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_highScore_6() { return &___highScore_6; }
	inline void set_highScore_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___highScore_6 = value;
		Il2CppCodeGenWriteBarrier((&___highScore_6), value);
	}

	inline static int32_t get_offset_of_player_7() { return static_cast<int32_t>(offsetof(Highscore_t5A992FDEECF661ADB50FD15D68A56AD0041DE2B0, ___player_7)); }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * get_player_7() const { return ___player_7; }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 ** get_address_of_player_7() { return &___player_7; }
	inline void set_player_7(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * value)
	{
		___player_7 = value;
		Il2CppCodeGenWriteBarrier((&___player_7), value);
	}

	inline static int32_t get_offset_of_ShardScore_8() { return static_cast<int32_t>(offsetof(Highscore_t5A992FDEECF661ADB50FD15D68A56AD0041DE2B0, ___ShardScore_8)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_ShardScore_8() const { return ___ShardScore_8; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_ShardScore_8() { return &___ShardScore_8; }
	inline void set_ShardScore_8(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___ShardScore_8 = value;
		Il2CppCodeGenWriteBarrier((&___ShardScore_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIGHSCORE_T5A992FDEECF661ADB50FD15D68A56AD0041DE2B0_H
#ifndef JOYSTICK_T3DF5D60C31824A6BFD16338F9377102BE73A0153_H
#define JOYSTICK_T3DF5D60C31824A6BFD16338F9377102BE73A0153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Joystick
struct  Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Joystick::handleRange
	float ___handleRange_4;
	// System.Single Joystick::deadZone
	float ___deadZone_5;
	// AxisOptions Joystick::axisOptions
	int32_t ___axisOptions_6;
	// System.Boolean Joystick::snapX
	bool ___snapX_7;
	// System.Boolean Joystick::snapY
	bool ___snapY_8;
	// UnityEngine.RectTransform Joystick::background
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___background_9;
	// UnityEngine.RectTransform Joystick::handle
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___handle_10;
	// UnityEngine.RectTransform Joystick::baseRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___baseRect_11;
	// UnityEngine.Canvas Joystick::canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___canvas_12;
	// UnityEngine.Camera Joystick::cam
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cam_13;
	// UnityEngine.Vector2 Joystick::input
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___input_14;

public:
	inline static int32_t get_offset_of_handleRange_4() { return static_cast<int32_t>(offsetof(Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153, ___handleRange_4)); }
	inline float get_handleRange_4() const { return ___handleRange_4; }
	inline float* get_address_of_handleRange_4() { return &___handleRange_4; }
	inline void set_handleRange_4(float value)
	{
		___handleRange_4 = value;
	}

	inline static int32_t get_offset_of_deadZone_5() { return static_cast<int32_t>(offsetof(Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153, ___deadZone_5)); }
	inline float get_deadZone_5() const { return ___deadZone_5; }
	inline float* get_address_of_deadZone_5() { return &___deadZone_5; }
	inline void set_deadZone_5(float value)
	{
		___deadZone_5 = value;
	}

	inline static int32_t get_offset_of_axisOptions_6() { return static_cast<int32_t>(offsetof(Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153, ___axisOptions_6)); }
	inline int32_t get_axisOptions_6() const { return ___axisOptions_6; }
	inline int32_t* get_address_of_axisOptions_6() { return &___axisOptions_6; }
	inline void set_axisOptions_6(int32_t value)
	{
		___axisOptions_6 = value;
	}

	inline static int32_t get_offset_of_snapX_7() { return static_cast<int32_t>(offsetof(Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153, ___snapX_7)); }
	inline bool get_snapX_7() const { return ___snapX_7; }
	inline bool* get_address_of_snapX_7() { return &___snapX_7; }
	inline void set_snapX_7(bool value)
	{
		___snapX_7 = value;
	}

	inline static int32_t get_offset_of_snapY_8() { return static_cast<int32_t>(offsetof(Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153, ___snapY_8)); }
	inline bool get_snapY_8() const { return ___snapY_8; }
	inline bool* get_address_of_snapY_8() { return &___snapY_8; }
	inline void set_snapY_8(bool value)
	{
		___snapY_8 = value;
	}

	inline static int32_t get_offset_of_background_9() { return static_cast<int32_t>(offsetof(Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153, ___background_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_background_9() const { return ___background_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_background_9() { return &___background_9; }
	inline void set_background_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___background_9 = value;
		Il2CppCodeGenWriteBarrier((&___background_9), value);
	}

	inline static int32_t get_offset_of_handle_10() { return static_cast<int32_t>(offsetof(Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153, ___handle_10)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_handle_10() const { return ___handle_10; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_handle_10() { return &___handle_10; }
	inline void set_handle_10(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___handle_10 = value;
		Il2CppCodeGenWriteBarrier((&___handle_10), value);
	}

	inline static int32_t get_offset_of_baseRect_11() { return static_cast<int32_t>(offsetof(Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153, ___baseRect_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_baseRect_11() const { return ___baseRect_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_baseRect_11() { return &___baseRect_11; }
	inline void set_baseRect_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___baseRect_11 = value;
		Il2CppCodeGenWriteBarrier((&___baseRect_11), value);
	}

	inline static int32_t get_offset_of_canvas_12() { return static_cast<int32_t>(offsetof(Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153, ___canvas_12)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_canvas_12() const { return ___canvas_12; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_canvas_12() { return &___canvas_12; }
	inline void set_canvas_12(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___canvas_12 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_12), value);
	}

	inline static int32_t get_offset_of_cam_13() { return static_cast<int32_t>(offsetof(Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153, ___cam_13)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cam_13() const { return ___cam_13; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cam_13() { return &___cam_13; }
	inline void set_cam_13(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cam_13 = value;
		Il2CppCodeGenWriteBarrier((&___cam_13), value);
	}

	inline static int32_t get_offset_of_input_14() { return static_cast<int32_t>(offsetof(Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153, ___input_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_input_14() const { return ___input_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_input_14() { return &___input_14; }
	inline void set_input_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___input_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T3DF5D60C31824A6BFD16338F9377102BE73A0153_H
#ifndef JOYSTICKPLAYEREXAMPLE_TC21BF4F29E220888E49AD1C28ED50EA82E0C583B_H
#define JOYSTICKPLAYEREXAMPLE_TC21BF4F29E220888E49AD1C28ED50EA82E0C583B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoystickPlayerExample
struct  JoystickPlayerExample_tC21BF4F29E220888E49AD1C28ED50EA82E0C583B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single JoystickPlayerExample::speed
	float ___speed_4;
	// VariableJoystick JoystickPlayerExample::variableJoystick
	VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1 * ___variableJoystick_5;
	// UnityEngine.Rigidbody JoystickPlayerExample::rb
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___rb_6;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(JoystickPlayerExample_tC21BF4F29E220888E49AD1C28ED50EA82E0C583B, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_variableJoystick_5() { return static_cast<int32_t>(offsetof(JoystickPlayerExample_tC21BF4F29E220888E49AD1C28ED50EA82E0C583B, ___variableJoystick_5)); }
	inline VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1 * get_variableJoystick_5() const { return ___variableJoystick_5; }
	inline VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1 ** get_address_of_variableJoystick_5() { return &___variableJoystick_5; }
	inline void set_variableJoystick_5(VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1 * value)
	{
		___variableJoystick_5 = value;
		Il2CppCodeGenWriteBarrier((&___variableJoystick_5), value);
	}

	inline static int32_t get_offset_of_rb_6() { return static_cast<int32_t>(offsetof(JoystickPlayerExample_tC21BF4F29E220888E49AD1C28ED50EA82E0C583B, ___rb_6)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_rb_6() const { return ___rb_6; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_rb_6() { return &___rb_6; }
	inline void set_rb_6(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___rb_6 = value;
		Il2CppCodeGenWriteBarrier((&___rb_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKPLAYEREXAMPLE_TC21BF4F29E220888E49AD1C28ED50EA82E0C583B_H
#ifndef JOYSTICKSETTEREXAMPLE_T0C714E5922483B0E1D4C129300C7C8FD79E4F435_H
#define JOYSTICKSETTEREXAMPLE_T0C714E5922483B0E1D4C129300C7C8FD79E4F435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoystickSetterExample
struct  JoystickSetterExample_t0C714E5922483B0E1D4C129300C7C8FD79E4F435  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// VariableJoystick JoystickSetterExample::variableJoystick
	VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1 * ___variableJoystick_4;
	// UnityEngine.UI.Text JoystickSetterExample::valueText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___valueText_5;
	// UnityEngine.UI.Image JoystickSetterExample::background
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___background_6;
	// UnityEngine.Sprite[] JoystickSetterExample::axisSprites
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___axisSprites_7;

public:
	inline static int32_t get_offset_of_variableJoystick_4() { return static_cast<int32_t>(offsetof(JoystickSetterExample_t0C714E5922483B0E1D4C129300C7C8FD79E4F435, ___variableJoystick_4)); }
	inline VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1 * get_variableJoystick_4() const { return ___variableJoystick_4; }
	inline VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1 ** get_address_of_variableJoystick_4() { return &___variableJoystick_4; }
	inline void set_variableJoystick_4(VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1 * value)
	{
		___variableJoystick_4 = value;
		Il2CppCodeGenWriteBarrier((&___variableJoystick_4), value);
	}

	inline static int32_t get_offset_of_valueText_5() { return static_cast<int32_t>(offsetof(JoystickSetterExample_t0C714E5922483B0E1D4C129300C7C8FD79E4F435, ___valueText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_valueText_5() const { return ___valueText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_valueText_5() { return &___valueText_5; }
	inline void set_valueText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___valueText_5 = value;
		Il2CppCodeGenWriteBarrier((&___valueText_5), value);
	}

	inline static int32_t get_offset_of_background_6() { return static_cast<int32_t>(offsetof(JoystickSetterExample_t0C714E5922483B0E1D4C129300C7C8FD79E4F435, ___background_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_background_6() const { return ___background_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_background_6() { return &___background_6; }
	inline void set_background_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___background_6 = value;
		Il2CppCodeGenWriteBarrier((&___background_6), value);
	}

	inline static int32_t get_offset_of_axisSprites_7() { return static_cast<int32_t>(offsetof(JoystickSetterExample_t0C714E5922483B0E1D4C129300C7C8FD79E4F435, ___axisSprites_7)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_axisSprites_7() const { return ___axisSprites_7; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_axisSprites_7() { return &___axisSprites_7; }
	inline void set_axisSprites_7(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___axisSprites_7 = value;
		Il2CppCodeGenWriteBarrier((&___axisSprites_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKSETTEREXAMPLE_T0C714E5922483B0E1D4C129300C7C8FD79E4F435_H
#ifndef LAUNCHPREVIEW_T6D5C9A5543C80FBD86CAB533D40A443035DE132D_H
#define LAUNCHPREVIEW_T6D5C9A5543C80FBD86CAB533D40A443035DE132D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LaunchPreview
struct  LaunchPreview_t6D5C9A5543C80FBD86CAB533D40A443035DE132D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.LineRenderer LaunchPreview::lineRenderer
	LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * ___lineRenderer_4;
	// UnityEngine.Vector3 LaunchPreview::dragStartPoint
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dragStartPoint_5;

public:
	inline static int32_t get_offset_of_lineRenderer_4() { return static_cast<int32_t>(offsetof(LaunchPreview_t6D5C9A5543C80FBD86CAB533D40A443035DE132D, ___lineRenderer_4)); }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * get_lineRenderer_4() const { return ___lineRenderer_4; }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 ** get_address_of_lineRenderer_4() { return &___lineRenderer_4; }
	inline void set_lineRenderer_4(LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * value)
	{
		___lineRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineRenderer_4), value);
	}

	inline static int32_t get_offset_of_dragStartPoint_5() { return static_cast<int32_t>(offsetof(LaunchPreview_t6D5C9A5543C80FBD86CAB533D40A443035DE132D, ___dragStartPoint_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_dragStartPoint_5() const { return ___dragStartPoint_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_dragStartPoint_5() { return &___dragStartPoint_5; }
	inline void set_dragStartPoint_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___dragStartPoint_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAUNCHPREVIEW_T6D5C9A5543C80FBD86CAB533D40A443035DE132D_H
#ifndef LEVELLOADER_T528DC9F40D2C5040043E386207C89B1400AE0343_H
#define LEVELLOADER_T528DC9F40D2C5040043E386207C89B1400AE0343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelLoader
struct  LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject LevelLoader::loadingScreen
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___loadingScreen_4;
	// UnityEngine.UI.Slider LevelLoader::slider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___slider_5;
	// TMPro.TextMeshProUGUI LevelLoader::progressText
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___progressText_6;

public:
	inline static int32_t get_offset_of_loadingScreen_4() { return static_cast<int32_t>(offsetof(LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343, ___loadingScreen_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_loadingScreen_4() const { return ___loadingScreen_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_loadingScreen_4() { return &___loadingScreen_4; }
	inline void set_loadingScreen_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___loadingScreen_4 = value;
		Il2CppCodeGenWriteBarrier((&___loadingScreen_4), value);
	}

	inline static int32_t get_offset_of_slider_5() { return static_cast<int32_t>(offsetof(LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343, ___slider_5)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_slider_5() const { return ___slider_5; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_slider_5() { return &___slider_5; }
	inline void set_slider_5(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___slider_5 = value;
		Il2CppCodeGenWriteBarrier((&___slider_5), value);
	}

	inline static int32_t get_offset_of_progressText_6() { return static_cast<int32_t>(offsetof(LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343, ___progressText_6)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_progressText_6() const { return ___progressText_6; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_progressText_6() { return &___progressText_6; }
	inline void set_progressText_6(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___progressText_6 = value;
		Il2CppCodeGenWriteBarrier((&___progressText_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELLOADER_T528DC9F40D2C5040043E386207C89B1400AE0343_H
#ifndef LEVELMANAGER_T4694E049F1814D6105F42EA77B02E163824A2B2D_H
#define LEVELMANAGER_T4694E049F1814D6105F42EA77B02E163824A2B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelManager
struct  LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject LevelManager::currentCheckpoint
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___currentCheckpoint_4;
	// PlayerController LevelManager::player
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * ___player_5;
	// UnityEngine.GameObject LevelManager::deathParticle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___deathParticle_6;
	// UnityEngine.GameObject LevelManager::respawnParticle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___respawnParticle_7;
	// System.Int32 LevelManager::pointPenaltyOnDeath
	int32_t ___pointPenaltyOnDeath_8;
	// System.Single LevelManager::respawnDelay
	float ___respawnDelay_9;
	// System.Single LevelManager::gravityStore
	float ___gravityStore_10;
	// CameraFollow LevelManager::cam
	CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142 * ___cam_11;

public:
	inline static int32_t get_offset_of_currentCheckpoint_4() { return static_cast<int32_t>(offsetof(LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D, ___currentCheckpoint_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_currentCheckpoint_4() const { return ___currentCheckpoint_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_currentCheckpoint_4() { return &___currentCheckpoint_4; }
	inline void set_currentCheckpoint_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___currentCheckpoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentCheckpoint_4), value);
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D, ___player_5)); }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * get_player_5() const { return ___player_5; }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((&___player_5), value);
	}

	inline static int32_t get_offset_of_deathParticle_6() { return static_cast<int32_t>(offsetof(LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D, ___deathParticle_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_deathParticle_6() const { return ___deathParticle_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_deathParticle_6() { return &___deathParticle_6; }
	inline void set_deathParticle_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___deathParticle_6 = value;
		Il2CppCodeGenWriteBarrier((&___deathParticle_6), value);
	}

	inline static int32_t get_offset_of_respawnParticle_7() { return static_cast<int32_t>(offsetof(LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D, ___respawnParticle_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_respawnParticle_7() const { return ___respawnParticle_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_respawnParticle_7() { return &___respawnParticle_7; }
	inline void set_respawnParticle_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___respawnParticle_7 = value;
		Il2CppCodeGenWriteBarrier((&___respawnParticle_7), value);
	}

	inline static int32_t get_offset_of_pointPenaltyOnDeath_8() { return static_cast<int32_t>(offsetof(LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D, ___pointPenaltyOnDeath_8)); }
	inline int32_t get_pointPenaltyOnDeath_8() const { return ___pointPenaltyOnDeath_8; }
	inline int32_t* get_address_of_pointPenaltyOnDeath_8() { return &___pointPenaltyOnDeath_8; }
	inline void set_pointPenaltyOnDeath_8(int32_t value)
	{
		___pointPenaltyOnDeath_8 = value;
	}

	inline static int32_t get_offset_of_respawnDelay_9() { return static_cast<int32_t>(offsetof(LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D, ___respawnDelay_9)); }
	inline float get_respawnDelay_9() const { return ___respawnDelay_9; }
	inline float* get_address_of_respawnDelay_9() { return &___respawnDelay_9; }
	inline void set_respawnDelay_9(float value)
	{
		___respawnDelay_9 = value;
	}

	inline static int32_t get_offset_of_gravityStore_10() { return static_cast<int32_t>(offsetof(LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D, ___gravityStore_10)); }
	inline float get_gravityStore_10() const { return ___gravityStore_10; }
	inline float* get_address_of_gravityStore_10() { return &___gravityStore_10; }
	inline void set_gravityStore_10(float value)
	{
		___gravityStore_10 = value;
	}

	inline static int32_t get_offset_of_cam_11() { return static_cast<int32_t>(offsetof(LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D, ___cam_11)); }
	inline CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142 * get_cam_11() const { return ___cam_11; }
	inline CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142 ** get_address_of_cam_11() { return &___cam_11; }
	inline void set_cam_11(CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142 * value)
	{
		___cam_11 = value;
		Il2CppCodeGenWriteBarrier((&___cam_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELMANAGER_T4694E049F1814D6105F42EA77B02E163824A2B2D_H
#ifndef MAINMENU_T7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105_H
#define MAINMENU_T7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenu
struct  MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENU_T7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105_H
#ifndef MOVE_T48FAC5147B90F0345C8CB913CDEF6D8B3FC63B35_H
#define MOVE_T48FAC5147B90F0345C8CB913CDEF6D8B3FC63B35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Move
struct  Move_t48FAC5147B90F0345C8CB913CDEF6D8B3FC63B35  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVE_T48FAC5147B90F0345C8CB913CDEF6D8B3FC63B35_H
#ifndef OBSTACLE_T45A47093FD0D640DE4C85A2CB52665B5192A232E_H
#define OBSTACLE_T45A47093FD0D640DE4C85A2CB52665B5192A232E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Obstacle
struct  Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject Obstacle::Player
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Player_4;
	// PlayerController Obstacle::PC
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * ___PC_5;
	// SFXManager Obstacle::SFXM
	SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * ___SFXM_6;
	// GameManager Obstacle::GM
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___GM_7;
	// UnityEngine.Color Obstacle::mycol
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___mycol_8;
	// UnityEngine.Color Obstacle::currentcolor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___currentcolor_9;
	// UnityEngine.Color Obstacle::nextcolor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___nextcolor_10;

public:
	inline static int32_t get_offset_of_Player_4() { return static_cast<int32_t>(offsetof(Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E, ___Player_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Player_4() const { return ___Player_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Player_4() { return &___Player_4; }
	inline void set_Player_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Player_4 = value;
		Il2CppCodeGenWriteBarrier((&___Player_4), value);
	}

	inline static int32_t get_offset_of_PC_5() { return static_cast<int32_t>(offsetof(Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E, ___PC_5)); }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * get_PC_5() const { return ___PC_5; }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 ** get_address_of_PC_5() { return &___PC_5; }
	inline void set_PC_5(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * value)
	{
		___PC_5 = value;
		Il2CppCodeGenWriteBarrier((&___PC_5), value);
	}

	inline static int32_t get_offset_of_SFXM_6() { return static_cast<int32_t>(offsetof(Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E, ___SFXM_6)); }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * get_SFXM_6() const { return ___SFXM_6; }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD ** get_address_of_SFXM_6() { return &___SFXM_6; }
	inline void set_SFXM_6(SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * value)
	{
		___SFXM_6 = value;
		Il2CppCodeGenWriteBarrier((&___SFXM_6), value);
	}

	inline static int32_t get_offset_of_GM_7() { return static_cast<int32_t>(offsetof(Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E, ___GM_7)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_GM_7() const { return ___GM_7; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_GM_7() { return &___GM_7; }
	inline void set_GM_7(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___GM_7 = value;
		Il2CppCodeGenWriteBarrier((&___GM_7), value);
	}

	inline static int32_t get_offset_of_mycol_8() { return static_cast<int32_t>(offsetof(Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E, ___mycol_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_mycol_8() const { return ___mycol_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_mycol_8() { return &___mycol_8; }
	inline void set_mycol_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___mycol_8 = value;
	}

	inline static int32_t get_offset_of_currentcolor_9() { return static_cast<int32_t>(offsetof(Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E, ___currentcolor_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_currentcolor_9() const { return ___currentcolor_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_currentcolor_9() { return &___currentcolor_9; }
	inline void set_currentcolor_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___currentcolor_9 = value;
	}

	inline static int32_t get_offset_of_nextcolor_10() { return static_cast<int32_t>(offsetof(Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E, ___nextcolor_10)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_nextcolor_10() const { return ___nextcolor_10; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_nextcolor_10() { return &___nextcolor_10; }
	inline void set_nextcolor_10(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___nextcolor_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSTACLE_T45A47093FD0D640DE4C85A2CB52665B5192A232E_H
#ifndef OBSTACLESPAWNING_T660785F865CF293ACE3FC388FE62969E5F4AFA08_H
#define OBSTACLESPAWNING_T660785F865CF293ACE3FC388FE62969E5F4AFA08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObstacleSpawning
struct  ObstacleSpawning_t660785F865CF293ACE3FC388FE62969E5F4AFA08  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject ObstacleSpawning::player
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___player_4;
	// UnityEngine.GameObject ObstacleSpawning::obstacle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obstacle_5;
	// System.Single ObstacleSpawning::amountOfObstacles
	float ___amountOfObstacles_6;
	// System.Single ObstacleSpawning::minX
	float ___minX_7;
	// System.Single ObstacleSpawning::maxX
	float ___maxX_8;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(ObstacleSpawning_t660785F865CF293ACE3FC388FE62969E5F4AFA08, ___player_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_player_4() const { return ___player_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_obstacle_5() { return static_cast<int32_t>(offsetof(ObstacleSpawning_t660785F865CF293ACE3FC388FE62969E5F4AFA08, ___obstacle_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obstacle_5() const { return ___obstacle_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obstacle_5() { return &___obstacle_5; }
	inline void set_obstacle_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obstacle_5 = value;
		Il2CppCodeGenWriteBarrier((&___obstacle_5), value);
	}

	inline static int32_t get_offset_of_amountOfObstacles_6() { return static_cast<int32_t>(offsetof(ObstacleSpawning_t660785F865CF293ACE3FC388FE62969E5F4AFA08, ___amountOfObstacles_6)); }
	inline float get_amountOfObstacles_6() const { return ___amountOfObstacles_6; }
	inline float* get_address_of_amountOfObstacles_6() { return &___amountOfObstacles_6; }
	inline void set_amountOfObstacles_6(float value)
	{
		___amountOfObstacles_6 = value;
	}

	inline static int32_t get_offset_of_minX_7() { return static_cast<int32_t>(offsetof(ObstacleSpawning_t660785F865CF293ACE3FC388FE62969E5F4AFA08, ___minX_7)); }
	inline float get_minX_7() const { return ___minX_7; }
	inline float* get_address_of_minX_7() { return &___minX_7; }
	inline void set_minX_7(float value)
	{
		___minX_7 = value;
	}

	inline static int32_t get_offset_of_maxX_8() { return static_cast<int32_t>(offsetof(ObstacleSpawning_t660785F865CF293ACE3FC388FE62969E5F4AFA08, ___maxX_8)); }
	inline float get_maxX_8() const { return ___maxX_8; }
	inline float* get_address_of_maxX_8() { return &___maxX_8; }
	inline void set_maxX_8(float value)
	{
		___maxX_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSTACLESPAWNING_T660785F865CF293ACE3FC388FE62969E5F4AFA08_H
#ifndef OBSTACLESCUBE_TC10D5FE1BFB6542BCC7486CB6F71064F09E480E5_H
#define OBSTACLESCUBE_TC10D5FE1BFB6542BCC7486CB6F71064F09E480E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Obstaclescube
struct  Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Obstaclescube::xDistance
	float ___xDistance_4;
	// System.Single Obstaclescube::minSpread
	float ___minSpread_5;
	// System.Single Obstaclescube::maxSpread
	float ___maxSpread_6;
	// UnityEngine.Transform Obstaclescube::playerTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___playerTransform_7;
	// UnityEngine.Transform Obstaclescube::obstaclePrefab
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___obstaclePrefab_8;
	// System.Single Obstaclescube::xSpread
	float ___xSpread_9;
	// System.Single Obstaclescube::lastXPos
	float ___lastXPos_10;

public:
	inline static int32_t get_offset_of_xDistance_4() { return static_cast<int32_t>(offsetof(Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5, ___xDistance_4)); }
	inline float get_xDistance_4() const { return ___xDistance_4; }
	inline float* get_address_of_xDistance_4() { return &___xDistance_4; }
	inline void set_xDistance_4(float value)
	{
		___xDistance_4 = value;
	}

	inline static int32_t get_offset_of_minSpread_5() { return static_cast<int32_t>(offsetof(Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5, ___minSpread_5)); }
	inline float get_minSpread_5() const { return ___minSpread_5; }
	inline float* get_address_of_minSpread_5() { return &___minSpread_5; }
	inline void set_minSpread_5(float value)
	{
		___minSpread_5 = value;
	}

	inline static int32_t get_offset_of_maxSpread_6() { return static_cast<int32_t>(offsetof(Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5, ___maxSpread_6)); }
	inline float get_maxSpread_6() const { return ___maxSpread_6; }
	inline float* get_address_of_maxSpread_6() { return &___maxSpread_6; }
	inline void set_maxSpread_6(float value)
	{
		___maxSpread_6 = value;
	}

	inline static int32_t get_offset_of_playerTransform_7() { return static_cast<int32_t>(offsetof(Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5, ___playerTransform_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_playerTransform_7() const { return ___playerTransform_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_playerTransform_7() { return &___playerTransform_7; }
	inline void set_playerTransform_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___playerTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___playerTransform_7), value);
	}

	inline static int32_t get_offset_of_obstaclePrefab_8() { return static_cast<int32_t>(offsetof(Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5, ___obstaclePrefab_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_obstaclePrefab_8() const { return ___obstaclePrefab_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_obstaclePrefab_8() { return &___obstaclePrefab_8; }
	inline void set_obstaclePrefab_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___obstaclePrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___obstaclePrefab_8), value);
	}

	inline static int32_t get_offset_of_xSpread_9() { return static_cast<int32_t>(offsetof(Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5, ___xSpread_9)); }
	inline float get_xSpread_9() const { return ___xSpread_9; }
	inline float* get_address_of_xSpread_9() { return &___xSpread_9; }
	inline void set_xSpread_9(float value)
	{
		___xSpread_9 = value;
	}

	inline static int32_t get_offset_of_lastXPos_10() { return static_cast<int32_t>(offsetof(Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5, ___lastXPos_10)); }
	inline float get_lastXPos_10() const { return ___lastXPos_10; }
	inline float* get_address_of_lastXPos_10() { return &___lastXPos_10; }
	inline void set_lastXPos_10(float value)
	{
		___lastXPos_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSTACLESCUBE_TC10D5FE1BFB6542BCC7486CB6F71064F09E480E5_H
#ifndef OPTIONSMENU_T6FC54B44E11AEAB0BE9088478B24E686823BB4CD_H
#define OPTIONSMENU_T6FC54B44E11AEAB0BE9088478B24E686823BB4CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OptionsMenu
struct  OptionsMenu_t6FC54B44E11AEAB0BE9088478B24E686823BB4CD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Audio.AudioMixer OptionsMenu::audioMixer
	AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * ___audioMixer_4;
	// UnityEngine.Resolution[] OptionsMenu::resolutions
	ResolutionU5BU5D_t7B0EB2421A00B22819A02FE474A7F747845BED9A* ___resolutions_5;
	// TMPro.TMP_Dropdown OptionsMenu::resolutionDropdown
	TMP_Dropdown_t9FB6FD74CE2463D3A4C71A5A2A6FC29162B90353 * ___resolutionDropdown_6;

public:
	inline static int32_t get_offset_of_audioMixer_4() { return static_cast<int32_t>(offsetof(OptionsMenu_t6FC54B44E11AEAB0BE9088478B24E686823BB4CD, ___audioMixer_4)); }
	inline AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * get_audioMixer_4() const { return ___audioMixer_4; }
	inline AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F ** get_address_of_audioMixer_4() { return &___audioMixer_4; }
	inline void set_audioMixer_4(AudioMixer_t4B13E2A32B5CCB989E7F0DFBF5370CFEF561541F * value)
	{
		___audioMixer_4 = value;
		Il2CppCodeGenWriteBarrier((&___audioMixer_4), value);
	}

	inline static int32_t get_offset_of_resolutions_5() { return static_cast<int32_t>(offsetof(OptionsMenu_t6FC54B44E11AEAB0BE9088478B24E686823BB4CD, ___resolutions_5)); }
	inline ResolutionU5BU5D_t7B0EB2421A00B22819A02FE474A7F747845BED9A* get_resolutions_5() const { return ___resolutions_5; }
	inline ResolutionU5BU5D_t7B0EB2421A00B22819A02FE474A7F747845BED9A** get_address_of_resolutions_5() { return &___resolutions_5; }
	inline void set_resolutions_5(ResolutionU5BU5D_t7B0EB2421A00B22819A02FE474A7F747845BED9A* value)
	{
		___resolutions_5 = value;
		Il2CppCodeGenWriteBarrier((&___resolutions_5), value);
	}

	inline static int32_t get_offset_of_resolutionDropdown_6() { return static_cast<int32_t>(offsetof(OptionsMenu_t6FC54B44E11AEAB0BE9088478B24E686823BB4CD, ___resolutionDropdown_6)); }
	inline TMP_Dropdown_t9FB6FD74CE2463D3A4C71A5A2A6FC29162B90353 * get_resolutionDropdown_6() const { return ___resolutionDropdown_6; }
	inline TMP_Dropdown_t9FB6FD74CE2463D3A4C71A5A2A6FC29162B90353 ** get_address_of_resolutionDropdown_6() { return &___resolutionDropdown_6; }
	inline void set_resolutionDropdown_6(TMP_Dropdown_t9FB6FD74CE2463D3A4C71A5A2A6FC29162B90353 * value)
	{
		___resolutionDropdown_6 = value;
		Il2CppCodeGenWriteBarrier((&___resolutionDropdown_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONSMENU_T6FC54B44E11AEAB0BE9088478B24E686823BB4CD_H
#ifndef PARTICLECCC_TF0DEE3A8537B9C95EAAC9B93F64A4AD46B7B73FA_H
#define PARTICLECCC_TF0DEE3A8537B9C95EAAC9B93F64A4AD46B7B73FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleCCC
struct  ParticleCCC_tF0DEE3A8537B9C95EAAC9B93F64A4AD46B7B73FA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.ParticleSystem/MinMaxGradient ParticleCCC::startColor
	MinMaxGradient_tD94D591FCD1E394D6502774CDFC068CFA893FE6B  ___startColor_4;
	// System.Boolean ParticleCCC::isCrazyColorChanging
	bool ___isCrazyColorChanging_5;

public:
	inline static int32_t get_offset_of_startColor_4() { return static_cast<int32_t>(offsetof(ParticleCCC_tF0DEE3A8537B9C95EAAC9B93F64A4AD46B7B73FA, ___startColor_4)); }
	inline MinMaxGradient_tD94D591FCD1E394D6502774CDFC068CFA893FE6B  get_startColor_4() const { return ___startColor_4; }
	inline MinMaxGradient_tD94D591FCD1E394D6502774CDFC068CFA893FE6B * get_address_of_startColor_4() { return &___startColor_4; }
	inline void set_startColor_4(MinMaxGradient_tD94D591FCD1E394D6502774CDFC068CFA893FE6B  value)
	{
		___startColor_4 = value;
	}

	inline static int32_t get_offset_of_isCrazyColorChanging_5() { return static_cast<int32_t>(offsetof(ParticleCCC_tF0DEE3A8537B9C95EAAC9B93F64A4AD46B7B73FA, ___isCrazyColorChanging_5)); }
	inline bool get_isCrazyColorChanging_5() const { return ___isCrazyColorChanging_5; }
	inline bool* get_address_of_isCrazyColorChanging_5() { return &___isCrazyColorChanging_5; }
	inline void set_isCrazyColorChanging_5(bool value)
	{
		___isCrazyColorChanging_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLECCC_TF0DEE3A8537B9C95EAAC9B93F64A4AD46B7B73FA_H
#ifndef PAUSEMENU_TE6D6728ABA83B6BB731E1D06DC22A5A54656D791_H
#define PAUSEMENU_TE6D6728ABA83B6BB731E1D06DC22A5A54656D791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseMenu
struct  PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PauseMenu::pauseMenuUI
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pauseMenuUI_5;
	// System.String PauseMenu::menuSceneName
	String_t* ___menuSceneName_6;
	// UnityEngine.Audio.AudioMixerSnapshot PauseMenu::paused
	AudioMixerSnapshot_tD274CDCB45EE39E6CAE4A36F09FA5DDB65196174 * ___paused_7;
	// UnityEngine.Audio.AudioMixerSnapshot PauseMenu::playing
	AudioMixerSnapshot_tD274CDCB45EE39E6CAE4A36F09FA5DDB65196174 * ___playing_8;
	// UnityEngine.Canvas PauseMenu::canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___canvas_9;
	// AudioManager PauseMenu::AM
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * ___AM_10;
	// UnityEngine.GameObject PauseMenu::player
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___player_11;
	// PlayerController PauseMenu::playerScript
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * ___playerScript_12;
	// GameManager PauseMenu::GM
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___GM_13;

public:
	inline static int32_t get_offset_of_pauseMenuUI_5() { return static_cast<int32_t>(offsetof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791, ___pauseMenuUI_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pauseMenuUI_5() const { return ___pauseMenuUI_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pauseMenuUI_5() { return &___pauseMenuUI_5; }
	inline void set_pauseMenuUI_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pauseMenuUI_5 = value;
		Il2CppCodeGenWriteBarrier((&___pauseMenuUI_5), value);
	}

	inline static int32_t get_offset_of_menuSceneName_6() { return static_cast<int32_t>(offsetof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791, ___menuSceneName_6)); }
	inline String_t* get_menuSceneName_6() const { return ___menuSceneName_6; }
	inline String_t** get_address_of_menuSceneName_6() { return &___menuSceneName_6; }
	inline void set_menuSceneName_6(String_t* value)
	{
		___menuSceneName_6 = value;
		Il2CppCodeGenWriteBarrier((&___menuSceneName_6), value);
	}

	inline static int32_t get_offset_of_paused_7() { return static_cast<int32_t>(offsetof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791, ___paused_7)); }
	inline AudioMixerSnapshot_tD274CDCB45EE39E6CAE4A36F09FA5DDB65196174 * get_paused_7() const { return ___paused_7; }
	inline AudioMixerSnapshot_tD274CDCB45EE39E6CAE4A36F09FA5DDB65196174 ** get_address_of_paused_7() { return &___paused_7; }
	inline void set_paused_7(AudioMixerSnapshot_tD274CDCB45EE39E6CAE4A36F09FA5DDB65196174 * value)
	{
		___paused_7 = value;
		Il2CppCodeGenWriteBarrier((&___paused_7), value);
	}

	inline static int32_t get_offset_of_playing_8() { return static_cast<int32_t>(offsetof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791, ___playing_8)); }
	inline AudioMixerSnapshot_tD274CDCB45EE39E6CAE4A36F09FA5DDB65196174 * get_playing_8() const { return ___playing_8; }
	inline AudioMixerSnapshot_tD274CDCB45EE39E6CAE4A36F09FA5DDB65196174 ** get_address_of_playing_8() { return &___playing_8; }
	inline void set_playing_8(AudioMixerSnapshot_tD274CDCB45EE39E6CAE4A36F09FA5DDB65196174 * value)
	{
		___playing_8 = value;
		Il2CppCodeGenWriteBarrier((&___playing_8), value);
	}

	inline static int32_t get_offset_of_canvas_9() { return static_cast<int32_t>(offsetof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791, ___canvas_9)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_canvas_9() const { return ___canvas_9; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_canvas_9() { return &___canvas_9; }
	inline void set_canvas_9(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_9), value);
	}

	inline static int32_t get_offset_of_AM_10() { return static_cast<int32_t>(offsetof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791, ___AM_10)); }
	inline AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * get_AM_10() const { return ___AM_10; }
	inline AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 ** get_address_of_AM_10() { return &___AM_10; }
	inline void set_AM_10(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * value)
	{
		___AM_10 = value;
		Il2CppCodeGenWriteBarrier((&___AM_10), value);
	}

	inline static int32_t get_offset_of_player_11() { return static_cast<int32_t>(offsetof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791, ___player_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_player_11() const { return ___player_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_player_11() { return &___player_11; }
	inline void set_player_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___player_11 = value;
		Il2CppCodeGenWriteBarrier((&___player_11), value);
	}

	inline static int32_t get_offset_of_playerScript_12() { return static_cast<int32_t>(offsetof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791, ___playerScript_12)); }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * get_playerScript_12() const { return ___playerScript_12; }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 ** get_address_of_playerScript_12() { return &___playerScript_12; }
	inline void set_playerScript_12(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * value)
	{
		___playerScript_12 = value;
		Il2CppCodeGenWriteBarrier((&___playerScript_12), value);
	}

	inline static int32_t get_offset_of_GM_13() { return static_cast<int32_t>(offsetof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791, ___GM_13)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_GM_13() const { return ___GM_13; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_GM_13() { return &___GM_13; }
	inline void set_GM_13(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___GM_13 = value;
		Il2CppCodeGenWriteBarrier((&___GM_13), value);
	}
};

struct PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791_StaticFields
{
public:
	// System.Boolean PauseMenu::GameIsPaused
	bool ___GameIsPaused_4;

public:
	inline static int32_t get_offset_of_GameIsPaused_4() { return static_cast<int32_t>(offsetof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791_StaticFields, ___GameIsPaused_4)); }
	inline bool get_GameIsPaused_4() const { return ___GameIsPaused_4; }
	inline bool* get_address_of_GameIsPaused_4() { return &___GameIsPaused_4; }
	inline void set_GameIsPaused_4(bool value)
	{
		___GameIsPaused_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSEMENU_TE6D6728ABA83B6BB731E1D06DC22A5A54656D791_H
#ifndef PLAYERCONTROLLER_T4CE339054014370D89B89922EDC0EA2766589C85_H
#define PLAYERCONTROLLER_T4CE339054014370D89B89922EDC0EA2766589C85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerController
struct  PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Rigidbody2D PlayerController::rb
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___rb_4;
	// System.Single PlayerController::movespeed
	float ___movespeed_5;
	// System.Boolean PlayerController::moveright
	bool ___moveright_6;
	// System.Boolean PlayerController::moveleft
	bool ___moveleft_7;
	// System.Boolean PlayerController::moveup
	bool ___moveup_8;
	// System.Boolean PlayerController::movedown
	bool ___movedown_9;
	// System.Single PlayerController::movevelocity
	float ___movevelocity_10;
	// System.Single PlayerController::movevelocity2
	float ___movevelocity2_11;
	// System.Boolean PlayerController::jump
	bool ___jump_12;
	// System.Single PlayerController::jumpheight
	float ___jumpheight_13;
	// UnityEngine.Transform PlayerController::RightWallCheck
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___RightWallCheck_14;
	// UnityEngine.Transform PlayerController::LeftWallCheck
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___LeftWallCheck_15;
	// System.Single PlayerController::WallCheckRadius
	float ___WallCheckRadius_16;
	// UnityEngine.LayerMask PlayerController::whatIsWall
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___whatIsWall_17;
	// System.Boolean PlayerController::TouchingRightWall
	bool ___TouchingRightWall_18;
	// System.Boolean PlayerController::TouchingLeftWall
	bool ___TouchingLeftWall_19;
	// UnityEngine.Transform PlayerController::startPosition
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___startPosition_20;
	// System.Int32 PlayerController::Shard
	int32_t ___Shard_21;
	// UnityEngine.Animator PlayerController::anim
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___anim_22;
	// System.Single PlayerController::fallSpeed
	float ___fallSpeed_23;
	// System.Int32 PlayerController::levelIndex
	int32_t ___levelIndex_24;
	// GameManager PlayerController::gameManager
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___gameManager_25;
	// UnityEngine.Vector2 PlayerController::position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___position_26;
	// SFXManager PlayerController::SFXM
	SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * ___SFXM_27;
	// System.Single PlayerController::playerX
	float ___playerX_28;
	// System.Single PlayerController::playerY
	float ___playerY_29;
	// System.Boolean PlayerController::isPaused
	bool ___isPaused_31;
	// UnityEngine.Vector3 PlayerController::startDragPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startDragPosition_32;
	// UnityEngine.Vector3 PlayerController::endDragPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___endDragPosition_33;
	// UnityEngine.Vector3 PlayerController::currentPositon
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___currentPositon_34;
	// UnityEngine.Vector3 PlayerController::worldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_35;
	// UnityEngine.Vector3 PlayerController::lastPositon
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___lastPositon_36;
	// System.Single PlayerController::fallAcc
	float ___fallAcc_37;
	// UnityEngine.GameObject PlayerController::DeathParticle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DeathParticle_38;
	// UnityEngine.GameObject PlayerController::DeathEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DeathEffect_39;
	// System.Boolean PlayerController::moveType1
	bool ___moveType1_40;
	// System.Boolean PlayerController::moveType2
	bool ___moveType2_41;
	// System.Boolean PlayerController::moveType3
	bool ___moveType3_42;
	// Joystick PlayerController::joystick
	Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153 * ___joystick_43;
	// System.Single PlayerController::horizontalMove
	float ___horizontalMove_44;
	// System.Single PlayerController::verticalMove
	float ___verticalMove_45;
	// System.Boolean PlayerController::cointMagnetOn
	bool ___cointMagnetOn_46;

public:
	inline static int32_t get_offset_of_rb_4() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___rb_4)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_rb_4() const { return ___rb_4; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_rb_4() { return &___rb_4; }
	inline void set_rb_4(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___rb_4 = value;
		Il2CppCodeGenWriteBarrier((&___rb_4), value);
	}

	inline static int32_t get_offset_of_movespeed_5() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___movespeed_5)); }
	inline float get_movespeed_5() const { return ___movespeed_5; }
	inline float* get_address_of_movespeed_5() { return &___movespeed_5; }
	inline void set_movespeed_5(float value)
	{
		___movespeed_5 = value;
	}

	inline static int32_t get_offset_of_moveright_6() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___moveright_6)); }
	inline bool get_moveright_6() const { return ___moveright_6; }
	inline bool* get_address_of_moveright_6() { return &___moveright_6; }
	inline void set_moveright_6(bool value)
	{
		___moveright_6 = value;
	}

	inline static int32_t get_offset_of_moveleft_7() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___moveleft_7)); }
	inline bool get_moveleft_7() const { return ___moveleft_7; }
	inline bool* get_address_of_moveleft_7() { return &___moveleft_7; }
	inline void set_moveleft_7(bool value)
	{
		___moveleft_7 = value;
	}

	inline static int32_t get_offset_of_moveup_8() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___moveup_8)); }
	inline bool get_moveup_8() const { return ___moveup_8; }
	inline bool* get_address_of_moveup_8() { return &___moveup_8; }
	inline void set_moveup_8(bool value)
	{
		___moveup_8 = value;
	}

	inline static int32_t get_offset_of_movedown_9() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___movedown_9)); }
	inline bool get_movedown_9() const { return ___movedown_9; }
	inline bool* get_address_of_movedown_9() { return &___movedown_9; }
	inline void set_movedown_9(bool value)
	{
		___movedown_9 = value;
	}

	inline static int32_t get_offset_of_movevelocity_10() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___movevelocity_10)); }
	inline float get_movevelocity_10() const { return ___movevelocity_10; }
	inline float* get_address_of_movevelocity_10() { return &___movevelocity_10; }
	inline void set_movevelocity_10(float value)
	{
		___movevelocity_10 = value;
	}

	inline static int32_t get_offset_of_movevelocity2_11() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___movevelocity2_11)); }
	inline float get_movevelocity2_11() const { return ___movevelocity2_11; }
	inline float* get_address_of_movevelocity2_11() { return &___movevelocity2_11; }
	inline void set_movevelocity2_11(float value)
	{
		___movevelocity2_11 = value;
	}

	inline static int32_t get_offset_of_jump_12() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___jump_12)); }
	inline bool get_jump_12() const { return ___jump_12; }
	inline bool* get_address_of_jump_12() { return &___jump_12; }
	inline void set_jump_12(bool value)
	{
		___jump_12 = value;
	}

	inline static int32_t get_offset_of_jumpheight_13() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___jumpheight_13)); }
	inline float get_jumpheight_13() const { return ___jumpheight_13; }
	inline float* get_address_of_jumpheight_13() { return &___jumpheight_13; }
	inline void set_jumpheight_13(float value)
	{
		___jumpheight_13 = value;
	}

	inline static int32_t get_offset_of_RightWallCheck_14() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___RightWallCheck_14)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_RightWallCheck_14() const { return ___RightWallCheck_14; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_RightWallCheck_14() { return &___RightWallCheck_14; }
	inline void set_RightWallCheck_14(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___RightWallCheck_14 = value;
		Il2CppCodeGenWriteBarrier((&___RightWallCheck_14), value);
	}

	inline static int32_t get_offset_of_LeftWallCheck_15() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___LeftWallCheck_15)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_LeftWallCheck_15() const { return ___LeftWallCheck_15; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_LeftWallCheck_15() { return &___LeftWallCheck_15; }
	inline void set_LeftWallCheck_15(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___LeftWallCheck_15 = value;
		Il2CppCodeGenWriteBarrier((&___LeftWallCheck_15), value);
	}

	inline static int32_t get_offset_of_WallCheckRadius_16() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___WallCheckRadius_16)); }
	inline float get_WallCheckRadius_16() const { return ___WallCheckRadius_16; }
	inline float* get_address_of_WallCheckRadius_16() { return &___WallCheckRadius_16; }
	inline void set_WallCheckRadius_16(float value)
	{
		___WallCheckRadius_16 = value;
	}

	inline static int32_t get_offset_of_whatIsWall_17() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___whatIsWall_17)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_whatIsWall_17() const { return ___whatIsWall_17; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_whatIsWall_17() { return &___whatIsWall_17; }
	inline void set_whatIsWall_17(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___whatIsWall_17 = value;
	}

	inline static int32_t get_offset_of_TouchingRightWall_18() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___TouchingRightWall_18)); }
	inline bool get_TouchingRightWall_18() const { return ___TouchingRightWall_18; }
	inline bool* get_address_of_TouchingRightWall_18() { return &___TouchingRightWall_18; }
	inline void set_TouchingRightWall_18(bool value)
	{
		___TouchingRightWall_18 = value;
	}

	inline static int32_t get_offset_of_TouchingLeftWall_19() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___TouchingLeftWall_19)); }
	inline bool get_TouchingLeftWall_19() const { return ___TouchingLeftWall_19; }
	inline bool* get_address_of_TouchingLeftWall_19() { return &___TouchingLeftWall_19; }
	inline void set_TouchingLeftWall_19(bool value)
	{
		___TouchingLeftWall_19 = value;
	}

	inline static int32_t get_offset_of_startPosition_20() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___startPosition_20)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_startPosition_20() const { return ___startPosition_20; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_startPosition_20() { return &___startPosition_20; }
	inline void set_startPosition_20(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___startPosition_20 = value;
		Il2CppCodeGenWriteBarrier((&___startPosition_20), value);
	}

	inline static int32_t get_offset_of_Shard_21() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___Shard_21)); }
	inline int32_t get_Shard_21() const { return ___Shard_21; }
	inline int32_t* get_address_of_Shard_21() { return &___Shard_21; }
	inline void set_Shard_21(int32_t value)
	{
		___Shard_21 = value;
	}

	inline static int32_t get_offset_of_anim_22() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___anim_22)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_anim_22() const { return ___anim_22; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_anim_22() { return &___anim_22; }
	inline void set_anim_22(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___anim_22 = value;
		Il2CppCodeGenWriteBarrier((&___anim_22), value);
	}

	inline static int32_t get_offset_of_fallSpeed_23() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___fallSpeed_23)); }
	inline float get_fallSpeed_23() const { return ___fallSpeed_23; }
	inline float* get_address_of_fallSpeed_23() { return &___fallSpeed_23; }
	inline void set_fallSpeed_23(float value)
	{
		___fallSpeed_23 = value;
	}

	inline static int32_t get_offset_of_levelIndex_24() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___levelIndex_24)); }
	inline int32_t get_levelIndex_24() const { return ___levelIndex_24; }
	inline int32_t* get_address_of_levelIndex_24() { return &___levelIndex_24; }
	inline void set_levelIndex_24(int32_t value)
	{
		___levelIndex_24 = value;
	}

	inline static int32_t get_offset_of_gameManager_25() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___gameManager_25)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_gameManager_25() const { return ___gameManager_25; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_gameManager_25() { return &___gameManager_25; }
	inline void set_gameManager_25(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___gameManager_25 = value;
		Il2CppCodeGenWriteBarrier((&___gameManager_25), value);
	}

	inline static int32_t get_offset_of_position_26() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___position_26)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_position_26() const { return ___position_26; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_position_26() { return &___position_26; }
	inline void set_position_26(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___position_26 = value;
	}

	inline static int32_t get_offset_of_SFXM_27() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___SFXM_27)); }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * get_SFXM_27() const { return ___SFXM_27; }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD ** get_address_of_SFXM_27() { return &___SFXM_27; }
	inline void set_SFXM_27(SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * value)
	{
		___SFXM_27 = value;
		Il2CppCodeGenWriteBarrier((&___SFXM_27), value);
	}

	inline static int32_t get_offset_of_playerX_28() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___playerX_28)); }
	inline float get_playerX_28() const { return ___playerX_28; }
	inline float* get_address_of_playerX_28() { return &___playerX_28; }
	inline void set_playerX_28(float value)
	{
		___playerX_28 = value;
	}

	inline static int32_t get_offset_of_playerY_29() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___playerY_29)); }
	inline float get_playerY_29() const { return ___playerY_29; }
	inline float* get_address_of_playerY_29() { return &___playerY_29; }
	inline void set_playerY_29(float value)
	{
		___playerY_29 = value;
	}

	inline static int32_t get_offset_of_isPaused_31() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___isPaused_31)); }
	inline bool get_isPaused_31() const { return ___isPaused_31; }
	inline bool* get_address_of_isPaused_31() { return &___isPaused_31; }
	inline void set_isPaused_31(bool value)
	{
		___isPaused_31 = value;
	}

	inline static int32_t get_offset_of_startDragPosition_32() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___startDragPosition_32)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startDragPosition_32() const { return ___startDragPosition_32; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startDragPosition_32() { return &___startDragPosition_32; }
	inline void set_startDragPosition_32(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startDragPosition_32 = value;
	}

	inline static int32_t get_offset_of_endDragPosition_33() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___endDragPosition_33)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_endDragPosition_33() const { return ___endDragPosition_33; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_endDragPosition_33() { return &___endDragPosition_33; }
	inline void set_endDragPosition_33(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___endDragPosition_33 = value;
	}

	inline static int32_t get_offset_of_currentPositon_34() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___currentPositon_34)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_currentPositon_34() const { return ___currentPositon_34; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_currentPositon_34() { return &___currentPositon_34; }
	inline void set_currentPositon_34(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___currentPositon_34 = value;
	}

	inline static int32_t get_offset_of_worldPosition_35() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___worldPosition_35)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldPosition_35() const { return ___worldPosition_35; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldPosition_35() { return &___worldPosition_35; }
	inline void set_worldPosition_35(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldPosition_35 = value;
	}

	inline static int32_t get_offset_of_lastPositon_36() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___lastPositon_36)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_lastPositon_36() const { return ___lastPositon_36; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_lastPositon_36() { return &___lastPositon_36; }
	inline void set_lastPositon_36(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___lastPositon_36 = value;
	}

	inline static int32_t get_offset_of_fallAcc_37() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___fallAcc_37)); }
	inline float get_fallAcc_37() const { return ___fallAcc_37; }
	inline float* get_address_of_fallAcc_37() { return &___fallAcc_37; }
	inline void set_fallAcc_37(float value)
	{
		___fallAcc_37 = value;
	}

	inline static int32_t get_offset_of_DeathParticle_38() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___DeathParticle_38)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DeathParticle_38() const { return ___DeathParticle_38; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DeathParticle_38() { return &___DeathParticle_38; }
	inline void set_DeathParticle_38(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DeathParticle_38 = value;
		Il2CppCodeGenWriteBarrier((&___DeathParticle_38), value);
	}

	inline static int32_t get_offset_of_DeathEffect_39() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___DeathEffect_39)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DeathEffect_39() const { return ___DeathEffect_39; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DeathEffect_39() { return &___DeathEffect_39; }
	inline void set_DeathEffect_39(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DeathEffect_39 = value;
		Il2CppCodeGenWriteBarrier((&___DeathEffect_39), value);
	}

	inline static int32_t get_offset_of_moveType1_40() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___moveType1_40)); }
	inline bool get_moveType1_40() const { return ___moveType1_40; }
	inline bool* get_address_of_moveType1_40() { return &___moveType1_40; }
	inline void set_moveType1_40(bool value)
	{
		___moveType1_40 = value;
	}

	inline static int32_t get_offset_of_moveType2_41() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___moveType2_41)); }
	inline bool get_moveType2_41() const { return ___moveType2_41; }
	inline bool* get_address_of_moveType2_41() { return &___moveType2_41; }
	inline void set_moveType2_41(bool value)
	{
		___moveType2_41 = value;
	}

	inline static int32_t get_offset_of_moveType3_42() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___moveType3_42)); }
	inline bool get_moveType3_42() const { return ___moveType3_42; }
	inline bool* get_address_of_moveType3_42() { return &___moveType3_42; }
	inline void set_moveType3_42(bool value)
	{
		___moveType3_42 = value;
	}

	inline static int32_t get_offset_of_joystick_43() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___joystick_43)); }
	inline Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153 * get_joystick_43() const { return ___joystick_43; }
	inline Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153 ** get_address_of_joystick_43() { return &___joystick_43; }
	inline void set_joystick_43(Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153 * value)
	{
		___joystick_43 = value;
		Il2CppCodeGenWriteBarrier((&___joystick_43), value);
	}

	inline static int32_t get_offset_of_horizontalMove_44() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___horizontalMove_44)); }
	inline float get_horizontalMove_44() const { return ___horizontalMove_44; }
	inline float* get_address_of_horizontalMove_44() { return &___horizontalMove_44; }
	inline void set_horizontalMove_44(float value)
	{
		___horizontalMove_44 = value;
	}

	inline static int32_t get_offset_of_verticalMove_45() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___verticalMove_45)); }
	inline float get_verticalMove_45() const { return ___verticalMove_45; }
	inline float* get_address_of_verticalMove_45() { return &___verticalMove_45; }
	inline void set_verticalMove_45(float value)
	{
		___verticalMove_45 = value;
	}

	inline static int32_t get_offset_of_cointMagnetOn_46() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85, ___cointMagnetOn_46)); }
	inline bool get_cointMagnetOn_46() const { return ___cointMagnetOn_46; }
	inline bool* get_address_of_cointMagnetOn_46() { return &___cointMagnetOn_46; }
	inline void set_cointMagnetOn_46(bool value)
	{
		___cointMagnetOn_46 = value;
	}
};

struct PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85_StaticFields
{
public:
	// System.Boolean PlayerController::pauseCheck
	bool ___pauseCheck_30;

public:
	inline static int32_t get_offset_of_pauseCheck_30() { return static_cast<int32_t>(offsetof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85_StaticFields, ___pauseCheck_30)); }
	inline bool get_pauseCheck_30() const { return ___pauseCheck_30; }
	inline bool* get_address_of_pauseCheck_30() { return &___pauseCheck_30; }
	inline void set_pauseCheck_30(bool value)
	{
		___pauseCheck_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_T4CE339054014370D89B89922EDC0EA2766589C85_H
#ifndef POWERUP_T19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C_H
#define POWERUP_T19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PowerUP
struct  PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single PowerUP::duration
	float ___duration_4;
	// UnityEngine.GameObject PowerUP::turboParticleEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___turboParticleEffect_5;
	// UnityEngine.GameObject PowerUP::pickupEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pickupEffect_6;
	// UnityEngine.GameObject PowerUP::explodeParticleEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___explodeParticleEffect_7;
	// UnityEngine.GameObject PowerUP::explodepickupEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___explodepickupEffect_8;
	// UnityEngine.GameObject PowerUP::Player
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Player_9;
	// PlayerController PowerUP::playerScript
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * ___playerScript_10;
	// SFXManager PowerUP::SFXM
	SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * ___SFXM_11;
	// GameManager PowerUP::GM
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___GM_12;
	// UnityEngine.SpriteRenderer PowerUP::playerSR
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___playerSR_14;
	// System.Single PowerUP::cduration
	float ___cduration_15;
	// System.Single PowerUP::t
	float ___t_16;
	// UnityEngine.Color PowerUP::playerBlankColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___playerBlankColor_17;
	// System.Boolean PowerUP::playerIsFlashing
	bool ___playerIsFlashing_18;
	// CrazyColorChange PowerUP::CCC
	CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445 * ___CCC_19;
	// UnityEngine.Animator PowerUP::anim
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___anim_20;

public:
	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}

	inline static int32_t get_offset_of_turboParticleEffect_5() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___turboParticleEffect_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_turboParticleEffect_5() const { return ___turboParticleEffect_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_turboParticleEffect_5() { return &___turboParticleEffect_5; }
	inline void set_turboParticleEffect_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___turboParticleEffect_5 = value;
		Il2CppCodeGenWriteBarrier((&___turboParticleEffect_5), value);
	}

	inline static int32_t get_offset_of_pickupEffect_6() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___pickupEffect_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pickupEffect_6() const { return ___pickupEffect_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pickupEffect_6() { return &___pickupEffect_6; }
	inline void set_pickupEffect_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pickupEffect_6 = value;
		Il2CppCodeGenWriteBarrier((&___pickupEffect_6), value);
	}

	inline static int32_t get_offset_of_explodeParticleEffect_7() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___explodeParticleEffect_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_explodeParticleEffect_7() const { return ___explodeParticleEffect_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_explodeParticleEffect_7() { return &___explodeParticleEffect_7; }
	inline void set_explodeParticleEffect_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___explodeParticleEffect_7 = value;
		Il2CppCodeGenWriteBarrier((&___explodeParticleEffect_7), value);
	}

	inline static int32_t get_offset_of_explodepickupEffect_8() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___explodepickupEffect_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_explodepickupEffect_8() const { return ___explodepickupEffect_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_explodepickupEffect_8() { return &___explodepickupEffect_8; }
	inline void set_explodepickupEffect_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___explodepickupEffect_8 = value;
		Il2CppCodeGenWriteBarrier((&___explodepickupEffect_8), value);
	}

	inline static int32_t get_offset_of_Player_9() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___Player_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Player_9() const { return ___Player_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Player_9() { return &___Player_9; }
	inline void set_Player_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Player_9 = value;
		Il2CppCodeGenWriteBarrier((&___Player_9), value);
	}

	inline static int32_t get_offset_of_playerScript_10() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___playerScript_10)); }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * get_playerScript_10() const { return ___playerScript_10; }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 ** get_address_of_playerScript_10() { return &___playerScript_10; }
	inline void set_playerScript_10(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * value)
	{
		___playerScript_10 = value;
		Il2CppCodeGenWriteBarrier((&___playerScript_10), value);
	}

	inline static int32_t get_offset_of_SFXM_11() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___SFXM_11)); }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * get_SFXM_11() const { return ___SFXM_11; }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD ** get_address_of_SFXM_11() { return &___SFXM_11; }
	inline void set_SFXM_11(SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * value)
	{
		___SFXM_11 = value;
		Il2CppCodeGenWriteBarrier((&___SFXM_11), value);
	}

	inline static int32_t get_offset_of_GM_12() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___GM_12)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_GM_12() const { return ___GM_12; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_GM_12() { return &___GM_12; }
	inline void set_GM_12(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___GM_12 = value;
		Il2CppCodeGenWriteBarrier((&___GM_12), value);
	}

	inline static int32_t get_offset_of_playerSR_14() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___playerSR_14)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_playerSR_14() const { return ___playerSR_14; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_playerSR_14() { return &___playerSR_14; }
	inline void set_playerSR_14(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___playerSR_14 = value;
		Il2CppCodeGenWriteBarrier((&___playerSR_14), value);
	}

	inline static int32_t get_offset_of_cduration_15() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___cduration_15)); }
	inline float get_cduration_15() const { return ___cduration_15; }
	inline float* get_address_of_cduration_15() { return &___cduration_15; }
	inline void set_cduration_15(float value)
	{
		___cduration_15 = value;
	}

	inline static int32_t get_offset_of_t_16() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___t_16)); }
	inline float get_t_16() const { return ___t_16; }
	inline float* get_address_of_t_16() { return &___t_16; }
	inline void set_t_16(float value)
	{
		___t_16 = value;
	}

	inline static int32_t get_offset_of_playerBlankColor_17() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___playerBlankColor_17)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_playerBlankColor_17() const { return ___playerBlankColor_17; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_playerBlankColor_17() { return &___playerBlankColor_17; }
	inline void set_playerBlankColor_17(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___playerBlankColor_17 = value;
	}

	inline static int32_t get_offset_of_playerIsFlashing_18() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___playerIsFlashing_18)); }
	inline bool get_playerIsFlashing_18() const { return ___playerIsFlashing_18; }
	inline bool* get_address_of_playerIsFlashing_18() { return &___playerIsFlashing_18; }
	inline void set_playerIsFlashing_18(bool value)
	{
		___playerIsFlashing_18 = value;
	}

	inline static int32_t get_offset_of_CCC_19() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___CCC_19)); }
	inline CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445 * get_CCC_19() const { return ___CCC_19; }
	inline CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445 ** get_address_of_CCC_19() { return &___CCC_19; }
	inline void set_CCC_19(CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445 * value)
	{
		___CCC_19 = value;
		Il2CppCodeGenWriteBarrier((&___CCC_19), value);
	}

	inline static int32_t get_offset_of_anim_20() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C, ___anim_20)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_anim_20() const { return ___anim_20; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_anim_20() { return &___anim_20; }
	inline void set_anim_20(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___anim_20 = value;
		Il2CppCodeGenWriteBarrier((&___anim_20), value);
	}
};

struct PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C_StaticFields
{
public:
	// System.Single PowerUP::OldMoveSpeed
	float ___OldMoveSpeed_13;

public:
	inline static int32_t get_offset_of_OldMoveSpeed_13() { return static_cast<int32_t>(offsetof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C_StaticFields, ___OldMoveSpeed_13)); }
	inline float get_OldMoveSpeed_13() const { return ___OldMoveSpeed_13; }
	inline float* get_address_of_OldMoveSpeed_13() { return &___OldMoveSpeed_13; }
	inline void set_OldMoveSpeed_13(float value)
	{
		___OldMoveSpeed_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POWERUP_T19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C_H
#ifndef POWERUPINVICIBLE_T696BAC1110ED86CA29287FF9FD25AC5D63B26112_H
#define POWERUPINVICIBLE_T696BAC1110ED86CA29287FF9FD25AC5D63B26112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PowerUpInvicible
struct  PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single PowerUpInvicible::duration
	float ___duration_4;
	// UnityEngine.GameObject PowerUpInvicible::InvicibleParticleEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___InvicibleParticleEffect_5;
	// UnityEngine.GameObject PowerUpInvicible::pickupEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pickupEffect_6;
	// UnityEngine.GameObject PowerUpInvicible::Player
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Player_7;
	// PlayerController PowerUpInvicible::playerScript
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * ___playerScript_8;
	// SFXManager PowerUpInvicible::SFXM
	SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * ___SFXM_9;
	// GameManager PowerUpInvicible::GM
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___GM_10;
	// UnityEngine.SpriteRenderer PowerUpInvicible::playerSR
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___playerSR_11;
	// System.Single PowerUpInvicible::cduration
	float ___cduration_12;
	// System.Single PowerUpInvicible::t
	float ___t_13;
	// UnityEngine.Color PowerUpInvicible::playerBlankColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___playerBlankColor_14;
	// System.Boolean PowerUpInvicible::playerIsFlashing
	bool ___playerIsFlashing_15;
	// CrazyColorChange PowerUpInvicible::CCC
	CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445 * ___CCC_16;
	// UnityEngine.Animator PowerUpInvicible::anim
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___anim_17;

public:
	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}

	inline static int32_t get_offset_of_InvicibleParticleEffect_5() { return static_cast<int32_t>(offsetof(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112, ___InvicibleParticleEffect_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_InvicibleParticleEffect_5() const { return ___InvicibleParticleEffect_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_InvicibleParticleEffect_5() { return &___InvicibleParticleEffect_5; }
	inline void set_InvicibleParticleEffect_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___InvicibleParticleEffect_5 = value;
		Il2CppCodeGenWriteBarrier((&___InvicibleParticleEffect_5), value);
	}

	inline static int32_t get_offset_of_pickupEffect_6() { return static_cast<int32_t>(offsetof(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112, ___pickupEffect_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pickupEffect_6() const { return ___pickupEffect_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pickupEffect_6() { return &___pickupEffect_6; }
	inline void set_pickupEffect_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pickupEffect_6 = value;
		Il2CppCodeGenWriteBarrier((&___pickupEffect_6), value);
	}

	inline static int32_t get_offset_of_Player_7() { return static_cast<int32_t>(offsetof(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112, ___Player_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Player_7() const { return ___Player_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Player_7() { return &___Player_7; }
	inline void set_Player_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Player_7 = value;
		Il2CppCodeGenWriteBarrier((&___Player_7), value);
	}

	inline static int32_t get_offset_of_playerScript_8() { return static_cast<int32_t>(offsetof(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112, ___playerScript_8)); }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * get_playerScript_8() const { return ___playerScript_8; }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 ** get_address_of_playerScript_8() { return &___playerScript_8; }
	inline void set_playerScript_8(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * value)
	{
		___playerScript_8 = value;
		Il2CppCodeGenWriteBarrier((&___playerScript_8), value);
	}

	inline static int32_t get_offset_of_SFXM_9() { return static_cast<int32_t>(offsetof(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112, ___SFXM_9)); }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * get_SFXM_9() const { return ___SFXM_9; }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD ** get_address_of_SFXM_9() { return &___SFXM_9; }
	inline void set_SFXM_9(SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * value)
	{
		___SFXM_9 = value;
		Il2CppCodeGenWriteBarrier((&___SFXM_9), value);
	}

	inline static int32_t get_offset_of_GM_10() { return static_cast<int32_t>(offsetof(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112, ___GM_10)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_GM_10() const { return ___GM_10; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_GM_10() { return &___GM_10; }
	inline void set_GM_10(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___GM_10 = value;
		Il2CppCodeGenWriteBarrier((&___GM_10), value);
	}

	inline static int32_t get_offset_of_playerSR_11() { return static_cast<int32_t>(offsetof(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112, ___playerSR_11)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_playerSR_11() const { return ___playerSR_11; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_playerSR_11() { return &___playerSR_11; }
	inline void set_playerSR_11(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___playerSR_11 = value;
		Il2CppCodeGenWriteBarrier((&___playerSR_11), value);
	}

	inline static int32_t get_offset_of_cduration_12() { return static_cast<int32_t>(offsetof(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112, ___cduration_12)); }
	inline float get_cduration_12() const { return ___cduration_12; }
	inline float* get_address_of_cduration_12() { return &___cduration_12; }
	inline void set_cduration_12(float value)
	{
		___cduration_12 = value;
	}

	inline static int32_t get_offset_of_t_13() { return static_cast<int32_t>(offsetof(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112, ___t_13)); }
	inline float get_t_13() const { return ___t_13; }
	inline float* get_address_of_t_13() { return &___t_13; }
	inline void set_t_13(float value)
	{
		___t_13 = value;
	}

	inline static int32_t get_offset_of_playerBlankColor_14() { return static_cast<int32_t>(offsetof(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112, ___playerBlankColor_14)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_playerBlankColor_14() const { return ___playerBlankColor_14; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_playerBlankColor_14() { return &___playerBlankColor_14; }
	inline void set_playerBlankColor_14(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___playerBlankColor_14 = value;
	}

	inline static int32_t get_offset_of_playerIsFlashing_15() { return static_cast<int32_t>(offsetof(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112, ___playerIsFlashing_15)); }
	inline bool get_playerIsFlashing_15() const { return ___playerIsFlashing_15; }
	inline bool* get_address_of_playerIsFlashing_15() { return &___playerIsFlashing_15; }
	inline void set_playerIsFlashing_15(bool value)
	{
		___playerIsFlashing_15 = value;
	}

	inline static int32_t get_offset_of_CCC_16() { return static_cast<int32_t>(offsetof(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112, ___CCC_16)); }
	inline CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445 * get_CCC_16() const { return ___CCC_16; }
	inline CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445 ** get_address_of_CCC_16() { return &___CCC_16; }
	inline void set_CCC_16(CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445 * value)
	{
		___CCC_16 = value;
		Il2CppCodeGenWriteBarrier((&___CCC_16), value);
	}

	inline static int32_t get_offset_of_anim_17() { return static_cast<int32_t>(offsetof(PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112, ___anim_17)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_anim_17() const { return ___anim_17; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_anim_17() { return &___anim_17; }
	inline void set_anim_17(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___anim_17 = value;
		Il2CppCodeGenWriteBarrier((&___anim_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POWERUPINVICIBLE_T696BAC1110ED86CA29287FF9FD25AC5D63B26112_H
#ifndef POWERUPMINI_T5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26_H
#define POWERUPMINI_T5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PowerUpMini
struct  PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single PowerUpMini::multiplier
	float ___multiplier_4;
	// System.Single PowerUpMini::duration
	float ___duration_5;
	// UnityEngine.GameObject PowerUpMini::MiniParticleEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___MiniParticleEffect_6;
	// UnityEngine.GameObject PowerUpMini::pickupEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pickupEffect_7;
	// UnityEngine.GameObject PowerUpMini::Player
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Player_8;
	// PlayerController PowerUpMini::playerScript
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * ___playerScript_9;
	// SFXManager PowerUpMini::SFXM
	SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * ___SFXM_10;
	// GameManager PowerUpMini::GM
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___GM_11;
	// UnityEngine.SpriteRenderer PowerUpMini::playerSR
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___playerSR_12;
	// System.Single PowerUpMini::cduration
	float ___cduration_13;
	// System.Single PowerUpMini::t
	float ___t_14;
	// UnityEngine.Color PowerUpMini::playerBlankColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___playerBlankColor_15;
	// System.Boolean PowerUpMini::playerIsFlashing
	bool ___playerIsFlashing_16;

public:
	inline static int32_t get_offset_of_multiplier_4() { return static_cast<int32_t>(offsetof(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26, ___multiplier_4)); }
	inline float get_multiplier_4() const { return ___multiplier_4; }
	inline float* get_address_of_multiplier_4() { return &___multiplier_4; }
	inline void set_multiplier_4(float value)
	{
		___multiplier_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_MiniParticleEffect_6() { return static_cast<int32_t>(offsetof(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26, ___MiniParticleEffect_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_MiniParticleEffect_6() const { return ___MiniParticleEffect_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_MiniParticleEffect_6() { return &___MiniParticleEffect_6; }
	inline void set_MiniParticleEffect_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___MiniParticleEffect_6 = value;
		Il2CppCodeGenWriteBarrier((&___MiniParticleEffect_6), value);
	}

	inline static int32_t get_offset_of_pickupEffect_7() { return static_cast<int32_t>(offsetof(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26, ___pickupEffect_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pickupEffect_7() const { return ___pickupEffect_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pickupEffect_7() { return &___pickupEffect_7; }
	inline void set_pickupEffect_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pickupEffect_7 = value;
		Il2CppCodeGenWriteBarrier((&___pickupEffect_7), value);
	}

	inline static int32_t get_offset_of_Player_8() { return static_cast<int32_t>(offsetof(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26, ___Player_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Player_8() const { return ___Player_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Player_8() { return &___Player_8; }
	inline void set_Player_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Player_8 = value;
		Il2CppCodeGenWriteBarrier((&___Player_8), value);
	}

	inline static int32_t get_offset_of_playerScript_9() { return static_cast<int32_t>(offsetof(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26, ___playerScript_9)); }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * get_playerScript_9() const { return ___playerScript_9; }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 ** get_address_of_playerScript_9() { return &___playerScript_9; }
	inline void set_playerScript_9(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * value)
	{
		___playerScript_9 = value;
		Il2CppCodeGenWriteBarrier((&___playerScript_9), value);
	}

	inline static int32_t get_offset_of_SFXM_10() { return static_cast<int32_t>(offsetof(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26, ___SFXM_10)); }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * get_SFXM_10() const { return ___SFXM_10; }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD ** get_address_of_SFXM_10() { return &___SFXM_10; }
	inline void set_SFXM_10(SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * value)
	{
		___SFXM_10 = value;
		Il2CppCodeGenWriteBarrier((&___SFXM_10), value);
	}

	inline static int32_t get_offset_of_GM_11() { return static_cast<int32_t>(offsetof(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26, ___GM_11)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_GM_11() const { return ___GM_11; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_GM_11() { return &___GM_11; }
	inline void set_GM_11(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___GM_11 = value;
		Il2CppCodeGenWriteBarrier((&___GM_11), value);
	}

	inline static int32_t get_offset_of_playerSR_12() { return static_cast<int32_t>(offsetof(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26, ___playerSR_12)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_playerSR_12() const { return ___playerSR_12; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_playerSR_12() { return &___playerSR_12; }
	inline void set_playerSR_12(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___playerSR_12 = value;
		Il2CppCodeGenWriteBarrier((&___playerSR_12), value);
	}

	inline static int32_t get_offset_of_cduration_13() { return static_cast<int32_t>(offsetof(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26, ___cduration_13)); }
	inline float get_cduration_13() const { return ___cduration_13; }
	inline float* get_address_of_cduration_13() { return &___cduration_13; }
	inline void set_cduration_13(float value)
	{
		___cduration_13 = value;
	}

	inline static int32_t get_offset_of_t_14() { return static_cast<int32_t>(offsetof(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26, ___t_14)); }
	inline float get_t_14() const { return ___t_14; }
	inline float* get_address_of_t_14() { return &___t_14; }
	inline void set_t_14(float value)
	{
		___t_14 = value;
	}

	inline static int32_t get_offset_of_playerBlankColor_15() { return static_cast<int32_t>(offsetof(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26, ___playerBlankColor_15)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_playerBlankColor_15() const { return ___playerBlankColor_15; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_playerBlankColor_15() { return &___playerBlankColor_15; }
	inline void set_playerBlankColor_15(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___playerBlankColor_15 = value;
	}

	inline static int32_t get_offset_of_playerIsFlashing_16() { return static_cast<int32_t>(offsetof(PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26, ___playerIsFlashing_16)); }
	inline bool get_playerIsFlashing_16() const { return ___playerIsFlashing_16; }
	inline bool* get_address_of_playerIsFlashing_16() { return &___playerIsFlashing_16; }
	inline void set_playerIsFlashing_16(bool value)
	{
		___playerIsFlashing_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POWERUPMINI_T5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26_H
#ifndef POWERUPSPAWNING_TA1354036C94B3B391B27D8AA65890EFB811CC9BB_H
#define POWERUPSPAWNING_TA1354036C94B3B391B27D8AA65890EFB811CC9BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PowerUpSpawning
struct  PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PowerUpSpawning::player
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___player_4;
	// UnityEngine.Rigidbody2D PowerUpSpawning::playerRb
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___playerRb_5;
	// UnityEngine.GameObject PowerUpSpawning::Star
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Star_6;
	// UnityEngine.GameObject PowerUpSpawning::Star2
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Star2_7;
	// UnityEngine.GameObject PowerUpSpawning::Star3
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Star3_8;
	// UnityEngine.GameObject PowerUpSpawning::CoinMagnetObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___CoinMagnetObject_9;
	// System.Single PowerUpSpawning::amountOfPowerUps
	float ___amountOfPowerUps_10;
	// System.Single PowerUpSpawning::minX
	float ___minX_11;
	// System.Single PowerUpSpawning::maxX
	float ___maxX_12;
	// UnityEngine.GameObject PowerUpSpawning::start
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___start_13;
	// GameManager PowerUpSpawning::GM
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___GM_14;
	// System.Single PowerUpSpawning::sPositionY
	float ___sPositionY_15;
	// System.Single PowerUpSpawning::cPositionY
	float ___cPositionY_16;
	// System.Single PowerUpSpawning::spawnInterval
	float ___spawnInterval_17;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB, ___player_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_player_4() const { return ___player_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_playerRb_5() { return static_cast<int32_t>(offsetof(PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB, ___playerRb_5)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_playerRb_5() const { return ___playerRb_5; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_playerRb_5() { return &___playerRb_5; }
	inline void set_playerRb_5(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___playerRb_5 = value;
		Il2CppCodeGenWriteBarrier((&___playerRb_5), value);
	}

	inline static int32_t get_offset_of_Star_6() { return static_cast<int32_t>(offsetof(PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB, ___Star_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Star_6() const { return ___Star_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Star_6() { return &___Star_6; }
	inline void set_Star_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Star_6 = value;
		Il2CppCodeGenWriteBarrier((&___Star_6), value);
	}

	inline static int32_t get_offset_of_Star2_7() { return static_cast<int32_t>(offsetof(PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB, ___Star2_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Star2_7() const { return ___Star2_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Star2_7() { return &___Star2_7; }
	inline void set_Star2_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Star2_7 = value;
		Il2CppCodeGenWriteBarrier((&___Star2_7), value);
	}

	inline static int32_t get_offset_of_Star3_8() { return static_cast<int32_t>(offsetof(PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB, ___Star3_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Star3_8() const { return ___Star3_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Star3_8() { return &___Star3_8; }
	inline void set_Star3_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Star3_8 = value;
		Il2CppCodeGenWriteBarrier((&___Star3_8), value);
	}

	inline static int32_t get_offset_of_CoinMagnetObject_9() { return static_cast<int32_t>(offsetof(PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB, ___CoinMagnetObject_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_CoinMagnetObject_9() const { return ___CoinMagnetObject_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_CoinMagnetObject_9() { return &___CoinMagnetObject_9; }
	inline void set_CoinMagnetObject_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___CoinMagnetObject_9 = value;
		Il2CppCodeGenWriteBarrier((&___CoinMagnetObject_9), value);
	}

	inline static int32_t get_offset_of_amountOfPowerUps_10() { return static_cast<int32_t>(offsetof(PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB, ___amountOfPowerUps_10)); }
	inline float get_amountOfPowerUps_10() const { return ___amountOfPowerUps_10; }
	inline float* get_address_of_amountOfPowerUps_10() { return &___amountOfPowerUps_10; }
	inline void set_amountOfPowerUps_10(float value)
	{
		___amountOfPowerUps_10 = value;
	}

	inline static int32_t get_offset_of_minX_11() { return static_cast<int32_t>(offsetof(PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB, ___minX_11)); }
	inline float get_minX_11() const { return ___minX_11; }
	inline float* get_address_of_minX_11() { return &___minX_11; }
	inline void set_minX_11(float value)
	{
		___minX_11 = value;
	}

	inline static int32_t get_offset_of_maxX_12() { return static_cast<int32_t>(offsetof(PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB, ___maxX_12)); }
	inline float get_maxX_12() const { return ___maxX_12; }
	inline float* get_address_of_maxX_12() { return &___maxX_12; }
	inline void set_maxX_12(float value)
	{
		___maxX_12 = value;
	}

	inline static int32_t get_offset_of_start_13() { return static_cast<int32_t>(offsetof(PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB, ___start_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_start_13() const { return ___start_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_start_13() { return &___start_13; }
	inline void set_start_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___start_13 = value;
		Il2CppCodeGenWriteBarrier((&___start_13), value);
	}

	inline static int32_t get_offset_of_GM_14() { return static_cast<int32_t>(offsetof(PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB, ___GM_14)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_GM_14() const { return ___GM_14; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_GM_14() { return &___GM_14; }
	inline void set_GM_14(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___GM_14 = value;
		Il2CppCodeGenWriteBarrier((&___GM_14), value);
	}

	inline static int32_t get_offset_of_sPositionY_15() { return static_cast<int32_t>(offsetof(PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB, ___sPositionY_15)); }
	inline float get_sPositionY_15() const { return ___sPositionY_15; }
	inline float* get_address_of_sPositionY_15() { return &___sPositionY_15; }
	inline void set_sPositionY_15(float value)
	{
		___sPositionY_15 = value;
	}

	inline static int32_t get_offset_of_cPositionY_16() { return static_cast<int32_t>(offsetof(PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB, ___cPositionY_16)); }
	inline float get_cPositionY_16() const { return ___cPositionY_16; }
	inline float* get_address_of_cPositionY_16() { return &___cPositionY_16; }
	inline void set_cPositionY_16(float value)
	{
		___cPositionY_16 = value;
	}

	inline static int32_t get_offset_of_spawnInterval_17() { return static_cast<int32_t>(offsetof(PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB, ___spawnInterval_17)); }
	inline float get_spawnInterval_17() const { return ___spawnInterval_17; }
	inline float* get_address_of_spawnInterval_17() { return &___spawnInterval_17; }
	inline void set_spawnInterval_17(float value)
	{
		___spawnInterval_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POWERUPSPAWNING_TA1354036C94B3B391B27D8AA65890EFB811CC9BB_H
#ifndef RANDOMCOLORLERP_TC7B190341E7444C43211DC08216A296A13653666_H
#define RANDOMCOLORLERP_TC7B190341E7444C43211DC08216A296A13653666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RandomColorLerp
struct  RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.CanvasGroup RandomColorLerp::ColorGroup
	CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * ___ColorGroup_4;
	// UnityEngine.UI.Image RandomColorLerp::ColorImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___ColorImage_5;
	// System.Boolean RandomColorLerp::startlerpFade
	bool ___startlerpFade_6;
	// System.Single RandomColorLerp::startlerpTime
	float ___startlerpTime_7;
	// UnityEngine.Color RandomColorLerp::startColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___startColor_8;
	// UnityEngine.Color RandomColorLerp::finishColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___finishColor_9;
	// UnityEngine.SpriteRenderer RandomColorLerp::SR
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___SR_10;
	// UnityEngine.Color RandomColorLerp::sColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___sColor_11;
	// UnityEngine.Color RandomColorLerp::eColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___eColor_12;
	// System.Single RandomColorLerp::duration
	float ___duration_13;
	// System.Single RandomColorLerp::t
	float ___t_14;
	// System.Single RandomColorLerp::every
	float ___every_15;
	// System.Single RandomColorLerp::colorstep
	float ___colorstep_16;
	// UnityEngine.Color[] RandomColorLerp::colors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___colors_17;
	// System.Int32 RandomColorLerp::i
	int32_t ___i_18;
	// UnityEngine.Color RandomColorLerp::lerpedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___lerpedColor_19;
	// GameManager RandomColorLerp::GM
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___GM_20;

public:
	inline static int32_t get_offset_of_ColorGroup_4() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___ColorGroup_4)); }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * get_ColorGroup_4() const { return ___ColorGroup_4; }
	inline CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 ** get_address_of_ColorGroup_4() { return &___ColorGroup_4; }
	inline void set_ColorGroup_4(CanvasGroup_tE2C664C60990D1DCCEE0CC6545CC3E80369C7F90 * value)
	{
		___ColorGroup_4 = value;
		Il2CppCodeGenWriteBarrier((&___ColorGroup_4), value);
	}

	inline static int32_t get_offset_of_ColorImage_5() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___ColorImage_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_ColorImage_5() const { return ___ColorImage_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_ColorImage_5() { return &___ColorImage_5; }
	inline void set_ColorImage_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___ColorImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___ColorImage_5), value);
	}

	inline static int32_t get_offset_of_startlerpFade_6() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___startlerpFade_6)); }
	inline bool get_startlerpFade_6() const { return ___startlerpFade_6; }
	inline bool* get_address_of_startlerpFade_6() { return &___startlerpFade_6; }
	inline void set_startlerpFade_6(bool value)
	{
		___startlerpFade_6 = value;
	}

	inline static int32_t get_offset_of_startlerpTime_7() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___startlerpTime_7)); }
	inline float get_startlerpTime_7() const { return ___startlerpTime_7; }
	inline float* get_address_of_startlerpTime_7() { return &___startlerpTime_7; }
	inline void set_startlerpTime_7(float value)
	{
		___startlerpTime_7 = value;
	}

	inline static int32_t get_offset_of_startColor_8() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___startColor_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_startColor_8() const { return ___startColor_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_startColor_8() { return &___startColor_8; }
	inline void set_startColor_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___startColor_8 = value;
	}

	inline static int32_t get_offset_of_finishColor_9() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___finishColor_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_finishColor_9() const { return ___finishColor_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_finishColor_9() { return &___finishColor_9; }
	inline void set_finishColor_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___finishColor_9 = value;
	}

	inline static int32_t get_offset_of_SR_10() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___SR_10)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_SR_10() const { return ___SR_10; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_SR_10() { return &___SR_10; }
	inline void set_SR_10(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___SR_10 = value;
		Il2CppCodeGenWriteBarrier((&___SR_10), value);
	}

	inline static int32_t get_offset_of_sColor_11() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___sColor_11)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_sColor_11() const { return ___sColor_11; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_sColor_11() { return &___sColor_11; }
	inline void set_sColor_11(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___sColor_11 = value;
	}

	inline static int32_t get_offset_of_eColor_12() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___eColor_12)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_eColor_12() const { return ___eColor_12; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_eColor_12() { return &___eColor_12; }
	inline void set_eColor_12(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___eColor_12 = value;
	}

	inline static int32_t get_offset_of_duration_13() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___duration_13)); }
	inline float get_duration_13() const { return ___duration_13; }
	inline float* get_address_of_duration_13() { return &___duration_13; }
	inline void set_duration_13(float value)
	{
		___duration_13 = value;
	}

	inline static int32_t get_offset_of_t_14() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___t_14)); }
	inline float get_t_14() const { return ___t_14; }
	inline float* get_address_of_t_14() { return &___t_14; }
	inline void set_t_14(float value)
	{
		___t_14 = value;
	}

	inline static int32_t get_offset_of_every_15() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___every_15)); }
	inline float get_every_15() const { return ___every_15; }
	inline float* get_address_of_every_15() { return &___every_15; }
	inline void set_every_15(float value)
	{
		___every_15 = value;
	}

	inline static int32_t get_offset_of_colorstep_16() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___colorstep_16)); }
	inline float get_colorstep_16() const { return ___colorstep_16; }
	inline float* get_address_of_colorstep_16() { return &___colorstep_16; }
	inline void set_colorstep_16(float value)
	{
		___colorstep_16 = value;
	}

	inline static int32_t get_offset_of_colors_17() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___colors_17)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_colors_17() const { return ___colors_17; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_colors_17() { return &___colors_17; }
	inline void set_colors_17(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___colors_17 = value;
		Il2CppCodeGenWriteBarrier((&___colors_17), value);
	}

	inline static int32_t get_offset_of_i_18() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___i_18)); }
	inline int32_t get_i_18() const { return ___i_18; }
	inline int32_t* get_address_of_i_18() { return &___i_18; }
	inline void set_i_18(int32_t value)
	{
		___i_18 = value;
	}

	inline static int32_t get_offset_of_lerpedColor_19() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___lerpedColor_19)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_lerpedColor_19() const { return ___lerpedColor_19; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_lerpedColor_19() { return &___lerpedColor_19; }
	inline void set_lerpedColor_19(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___lerpedColor_19 = value;
	}

	inline static int32_t get_offset_of_GM_20() { return static_cast<int32_t>(offsetof(RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666, ___GM_20)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_GM_20() const { return ___GM_20; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_GM_20() { return &___GM_20; }
	inline void set_GM_20(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___GM_20 = value;
		Il2CppCodeGenWriteBarrier((&___GM_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMCOLORLERP_TC7B190341E7444C43211DC08216A296A13653666_H
#ifndef SFXMANAGER_T1C53274E4B3E9A99429EE65654E7914DF17739BD_H
#define SFXMANAGER_T1C53274E4B3E9A99429EE65654E7914DF17739BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SFXManager
struct  SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Sound[] SFXManager::sounds
	SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B* ___sounds_4;

public:
	inline static int32_t get_offset_of_sounds_4() { return static_cast<int32_t>(offsetof(SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD, ___sounds_4)); }
	inline SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B* get_sounds_4() const { return ___sounds_4; }
	inline SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B** get_address_of_sounds_4() { return &___sounds_4; }
	inline void set_sounds_4(SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B* value)
	{
		___sounds_4 = value;
		Il2CppCodeGenWriteBarrier((&___sounds_4), value);
	}
};

struct SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD_StaticFields
{
public:
	// SFXManager SFXManager::instance
	SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD_StaticFields, ___instance_5)); }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * get_instance_5() const { return ___instance_5; }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SFXMANAGER_T1C53274E4B3E9A99429EE65654E7914DF17739BD_H
#ifndef SCORE1_T32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119_H
#define SCORE1_T32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Score1
struct  Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TextMeshProUGUI Score1::text
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___text_4;
	// PlayerController Score1::player
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * ___player_5;
	// System.Single Score1::fscore
	float ___fscore_6;
	// System.Int32 Score1::score1
	int32_t ___score1_7;
	// GameManager Score1::GM
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___GM_8;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119, ___text_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_text_4() const { return ___text_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119, ___player_5)); }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * get_player_5() const { return ___player_5; }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((&___player_5), value);
	}

	inline static int32_t get_offset_of_fscore_6() { return static_cast<int32_t>(offsetof(Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119, ___fscore_6)); }
	inline float get_fscore_6() const { return ___fscore_6; }
	inline float* get_address_of_fscore_6() { return &___fscore_6; }
	inline void set_fscore_6(float value)
	{
		___fscore_6 = value;
	}

	inline static int32_t get_offset_of_score1_7() { return static_cast<int32_t>(offsetof(Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119, ___score1_7)); }
	inline int32_t get_score1_7() const { return ___score1_7; }
	inline int32_t* get_address_of_score1_7() { return &___score1_7; }
	inline void set_score1_7(int32_t value)
	{
		___score1_7 = value;
	}

	inline static int32_t get_offset_of_GM_8() { return static_cast<int32_t>(offsetof(Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119, ___GM_8)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_GM_8() const { return ___GM_8; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_GM_8() { return &___GM_8; }
	inline void set_GM_8(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___GM_8 = value;
		Il2CppCodeGenWriteBarrier((&___GM_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCORE1_T32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119_H
#ifndef SHARD_T91C7467F054A32443C2F199338B29BF534ECE0F5_H
#define SHARD_T91C7467F054A32443C2F199338B29BF534ECE0F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shard
struct  Shard_t91C7467F054A32443C2F199338B29BF534ECE0F5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// SFXManager Shard::SFXM
	SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * ___SFXM_4;
	// PlayerController Shard::player
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * ___player_5;
	// System.Boolean Shard::isCoinMagnetAffected
	bool ___isCoinMagnetAffected_6;
	// UnityEngine.Rigidbody2D Shard::rb
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___rb_7;

public:
	inline static int32_t get_offset_of_SFXM_4() { return static_cast<int32_t>(offsetof(Shard_t91C7467F054A32443C2F199338B29BF534ECE0F5, ___SFXM_4)); }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * get_SFXM_4() const { return ___SFXM_4; }
	inline SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD ** get_address_of_SFXM_4() { return &___SFXM_4; }
	inline void set_SFXM_4(SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD * value)
	{
		___SFXM_4 = value;
		Il2CppCodeGenWriteBarrier((&___SFXM_4), value);
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(Shard_t91C7467F054A32443C2F199338B29BF534ECE0F5, ___player_5)); }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * get_player_5() const { return ___player_5; }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((&___player_5), value);
	}

	inline static int32_t get_offset_of_isCoinMagnetAffected_6() { return static_cast<int32_t>(offsetof(Shard_t91C7467F054A32443C2F199338B29BF534ECE0F5, ___isCoinMagnetAffected_6)); }
	inline bool get_isCoinMagnetAffected_6() const { return ___isCoinMagnetAffected_6; }
	inline bool* get_address_of_isCoinMagnetAffected_6() { return &___isCoinMagnetAffected_6; }
	inline void set_isCoinMagnetAffected_6(bool value)
	{
		___isCoinMagnetAffected_6 = value;
	}

	inline static int32_t get_offset_of_rb_7() { return static_cast<int32_t>(offsetof(Shard_t91C7467F054A32443C2F199338B29BF534ECE0F5, ___rb_7)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_rb_7() const { return ___rb_7; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_rb_7() { return &___rb_7; }
	inline void set_rb_7(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___rb_7 = value;
		Il2CppCodeGenWriteBarrier((&___rb_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARD_T91C7467F054A32443C2F199338B29BF534ECE0F5_H
#ifndef SHARDCOUNTER_T3AF47A70BD8A226972075B08CF5F364173DCADE6_H
#define SHARDCOUNTER_T3AF47A70BD8A226972075B08CF5F364173DCADE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShardCounter
struct  ShardCounter_t3AF47A70BD8A226972075B08CF5F364173DCADE6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TextMeshProUGUI ShardCounter::text
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___text_4;
	// PlayerController ShardCounter::player
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * ___player_5;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(ShardCounter_t3AF47A70BD8A226972075B08CF5F364173DCADE6, ___text_4)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_text_4() const { return ___text_4; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(ShardCounter_t3AF47A70BD8A226972075B08CF5F364173DCADE6, ___player_5)); }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * get_player_5() const { return ___player_5; }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((&___player_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARDCOUNTER_T3AF47A70BD8A226972075B08CF5F364173DCADE6_H
#ifndef SHARDSPAWNING_T4B2F40A0798829663C401DEB595C1D2FDE4ABBAA_H
#define SHARDSPAWNING_T4B2F40A0798829663C401DEB595C1D2FDE4ABBAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShardSpawning
struct  ShardSpawning_t4B2F40A0798829663C401DEB595C1D2FDE4ABBAA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject ShardSpawning::player
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___player_4;
	// UnityEngine.GameObject ShardSpawning::Shard
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Shard_5;
	// System.Single ShardSpawning::amountOfShards
	float ___amountOfShards_6;
	// System.Single ShardSpawning::minX
	float ___minX_7;
	// System.Single ShardSpawning::maxX
	float ___maxX_8;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(ShardSpawning_t4B2F40A0798829663C401DEB595C1D2FDE4ABBAA, ___player_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_player_4() const { return ___player_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_Shard_5() { return static_cast<int32_t>(offsetof(ShardSpawning_t4B2F40A0798829663C401DEB595C1D2FDE4ABBAA, ___Shard_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Shard_5() const { return ___Shard_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Shard_5() { return &___Shard_5; }
	inline void set_Shard_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Shard_5 = value;
		Il2CppCodeGenWriteBarrier((&___Shard_5), value);
	}

	inline static int32_t get_offset_of_amountOfShards_6() { return static_cast<int32_t>(offsetof(ShardSpawning_t4B2F40A0798829663C401DEB595C1D2FDE4ABBAA, ___amountOfShards_6)); }
	inline float get_amountOfShards_6() const { return ___amountOfShards_6; }
	inline float* get_address_of_amountOfShards_6() { return &___amountOfShards_6; }
	inline void set_amountOfShards_6(float value)
	{
		___amountOfShards_6 = value;
	}

	inline static int32_t get_offset_of_minX_7() { return static_cast<int32_t>(offsetof(ShardSpawning_t4B2F40A0798829663C401DEB595C1D2FDE4ABBAA, ___minX_7)); }
	inline float get_minX_7() const { return ___minX_7; }
	inline float* get_address_of_minX_7() { return &___minX_7; }
	inline void set_minX_7(float value)
	{
		___minX_7 = value;
	}

	inline static int32_t get_offset_of_maxX_8() { return static_cast<int32_t>(offsetof(ShardSpawning_t4B2F40A0798829663C401DEB595C1D2FDE4ABBAA, ___maxX_8)); }
	inline float get_maxX_8() const { return ___maxX_8; }
	inline float* get_address_of_maxX_8() { return &___maxX_8; }
	inline void set_maxX_8(float value)
	{
		___maxX_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARDSPAWNING_T4B2F40A0798829663C401DEB595C1D2FDE4ABBAA_H
#ifndef SPRITESGLITCH_TBDBCC7C8FEC92CD55EA62E39C4444A86A378E442_H
#define SPRITESGLITCH_TBDBCC7C8FEC92CD55EA62E39C4444A86A378E442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpritesGlitch
struct  SpritesGlitch_tBDBCC7C8FEC92CD55EA62E39C4444A86A378E442  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESGLITCH_TBDBCC7C8FEC92CD55EA62E39C4444A86A378E442_H
#ifndef STARTMENU_T16A2DA0B3CBABA283134C0B7AA0403F13F231016_H
#define STARTMENU_T16A2DA0B3CBABA283134C0B7AA0403F13F231016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartMenu
struct  StartMenu_t16A2DA0B3CBABA283134C0B7AA0403F13F231016  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RectTransform StartMenu::menuContainer
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___menuContainer_4;
	// System.Boolean StartMenu::smooth
	bool ___smooth_5;
	// System.Single StartMenu::smoothSpeed
	float ___smoothSpeed_6;
	// UnityEngine.Vector3[] StartMenu::menuPositions
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___menuPositions_7;
	// UnityEngine.Vector3 StartMenu::desiredPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___desiredPosition_8;

public:
	inline static int32_t get_offset_of_menuContainer_4() { return static_cast<int32_t>(offsetof(StartMenu_t16A2DA0B3CBABA283134C0B7AA0403F13F231016, ___menuContainer_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_menuContainer_4() const { return ___menuContainer_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_menuContainer_4() { return &___menuContainer_4; }
	inline void set_menuContainer_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___menuContainer_4 = value;
		Il2CppCodeGenWriteBarrier((&___menuContainer_4), value);
	}

	inline static int32_t get_offset_of_smooth_5() { return static_cast<int32_t>(offsetof(StartMenu_t16A2DA0B3CBABA283134C0B7AA0403F13F231016, ___smooth_5)); }
	inline bool get_smooth_5() const { return ___smooth_5; }
	inline bool* get_address_of_smooth_5() { return &___smooth_5; }
	inline void set_smooth_5(bool value)
	{
		___smooth_5 = value;
	}

	inline static int32_t get_offset_of_smoothSpeed_6() { return static_cast<int32_t>(offsetof(StartMenu_t16A2DA0B3CBABA283134C0B7AA0403F13F231016, ___smoothSpeed_6)); }
	inline float get_smoothSpeed_6() const { return ___smoothSpeed_6; }
	inline float* get_address_of_smoothSpeed_6() { return &___smoothSpeed_6; }
	inline void set_smoothSpeed_6(float value)
	{
		___smoothSpeed_6 = value;
	}

	inline static int32_t get_offset_of_menuPositions_7() { return static_cast<int32_t>(offsetof(StartMenu_t16A2DA0B3CBABA283134C0B7AA0403F13F231016, ___menuPositions_7)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_menuPositions_7() const { return ___menuPositions_7; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_menuPositions_7() { return &___menuPositions_7; }
	inline void set_menuPositions_7(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___menuPositions_7 = value;
		Il2CppCodeGenWriteBarrier((&___menuPositions_7), value);
	}

	inline static int32_t get_offset_of_desiredPosition_8() { return static_cast<int32_t>(offsetof(StartMenu_t16A2DA0B3CBABA283134C0B7AA0403F13F231016, ___desiredPosition_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_desiredPosition_8() const { return ___desiredPosition_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_desiredPosition_8() { return &___desiredPosition_8; }
	inline void set_desiredPosition_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___desiredPosition_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTMENU_T16A2DA0B3CBABA283134C0B7AA0403F13F231016_H
#ifndef TRIANGLECOLOR_T56B96AF6F83658FBED5BFCE6572D3CD6D04525AD_H
#define TRIANGLECOLOR_T56B96AF6F83658FBED5BFCE6572D3CD6D04525AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TriangleColor
struct  TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.SpriteRenderer TriangleColor::SR
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ___SR_4;
	// System.Single TriangleColor::duration
	float ___duration_5;
	// System.Single TriangleColor::colorstep
	float ___colorstep_6;
	// UnityEngine.Color[] TriangleColor::colors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___colors_7;
	// System.Int32 TriangleColor::i
	int32_t ___i_8;
	// UnityEngine.Color TriangleColor::lerpedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___lerpedColor_9;
	// GameManager TriangleColor::GM
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * ___GM_10;
	// System.Single TriangleColor::N
	float ___N_11;
	// System.Single TriangleColor::colorduration
	float ___colorduration_12;
	// System.Int32 TriangleColor::n
	int32_t ___n_13;

public:
	inline static int32_t get_offset_of_SR_4() { return static_cast<int32_t>(offsetof(TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD, ___SR_4)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get_SR_4() const { return ___SR_4; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of_SR_4() { return &___SR_4; }
	inline void set_SR_4(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		___SR_4 = value;
		Il2CppCodeGenWriteBarrier((&___SR_4), value);
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_colorstep_6() { return static_cast<int32_t>(offsetof(TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD, ___colorstep_6)); }
	inline float get_colorstep_6() const { return ___colorstep_6; }
	inline float* get_address_of_colorstep_6() { return &___colorstep_6; }
	inline void set_colorstep_6(float value)
	{
		___colorstep_6 = value;
	}

	inline static int32_t get_offset_of_colors_7() { return static_cast<int32_t>(offsetof(TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD, ___colors_7)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_colors_7() const { return ___colors_7; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_colors_7() { return &___colors_7; }
	inline void set_colors_7(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___colors_7 = value;
		Il2CppCodeGenWriteBarrier((&___colors_7), value);
	}

	inline static int32_t get_offset_of_i_8() { return static_cast<int32_t>(offsetof(TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD, ___i_8)); }
	inline int32_t get_i_8() const { return ___i_8; }
	inline int32_t* get_address_of_i_8() { return &___i_8; }
	inline void set_i_8(int32_t value)
	{
		___i_8 = value;
	}

	inline static int32_t get_offset_of_lerpedColor_9() { return static_cast<int32_t>(offsetof(TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD, ___lerpedColor_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_lerpedColor_9() const { return ___lerpedColor_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_lerpedColor_9() { return &___lerpedColor_9; }
	inline void set_lerpedColor_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___lerpedColor_9 = value;
	}

	inline static int32_t get_offset_of_GM_10() { return static_cast<int32_t>(offsetof(TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD, ___GM_10)); }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * get_GM_10() const { return ___GM_10; }
	inline GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 ** get_address_of_GM_10() { return &___GM_10; }
	inline void set_GM_10(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89 * value)
	{
		___GM_10 = value;
		Il2CppCodeGenWriteBarrier((&___GM_10), value);
	}

	inline static int32_t get_offset_of_N_11() { return static_cast<int32_t>(offsetof(TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD, ___N_11)); }
	inline float get_N_11() const { return ___N_11; }
	inline float* get_address_of_N_11() { return &___N_11; }
	inline void set_N_11(float value)
	{
		___N_11 = value;
	}

	inline static int32_t get_offset_of_colorduration_12() { return static_cast<int32_t>(offsetof(TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD, ___colorduration_12)); }
	inline float get_colorduration_12() const { return ___colorduration_12; }
	inline float* get_address_of_colorduration_12() { return &___colorduration_12; }
	inline void set_colorduration_12(float value)
	{
		___colorduration_12 = value;
	}

	inline static int32_t get_offset_of_n_13() { return static_cast<int32_t>(offsetof(TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD, ___n_13)); }
	inline int32_t get_n_13() const { return ___n_13; }
	inline int32_t* get_address_of_n_13() { return &___n_13; }
	inline void set_n_13(int32_t value)
	{
		___n_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGLECOLOR_T56B96AF6F83658FBED5BFCE6572D3CD6D04525AD_H
#ifndef UIAUDIOMANAGER_TF960318B36B6631CC8000C06CC79BD7EEEF47FFC_H
#define UIAUDIOMANAGER_TF960318B36B6631CC8000C06CC79BD7EEEF47FFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIAudioManager
struct  UIAudioManager_tF960318B36B6631CC8000C06CC79BD7EEEF47FFC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Sound[] UIAudioManager::sounds
	SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B* ___sounds_4;

public:
	inline static int32_t get_offset_of_sounds_4() { return static_cast<int32_t>(offsetof(UIAudioManager_tF960318B36B6631CC8000C06CC79BD7EEEF47FFC, ___sounds_4)); }
	inline SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B* get_sounds_4() const { return ___sounds_4; }
	inline SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B** get_address_of_sounds_4() { return &___sounds_4; }
	inline void set_sounds_4(SoundU5BU5D_t662FA600FF60E2AAB7F66F3A6E9A0753E844389B* value)
	{
		___sounds_4 = value;
		Il2CppCodeGenWriteBarrier((&___sounds_4), value);
	}
};

struct UIAudioManager_tF960318B36B6631CC8000C06CC79BD7EEEF47FFC_StaticFields
{
public:
	// AudioManager UIAudioManager::instance
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(UIAudioManager_tF960318B36B6631CC8000C06CC79BD7EEEF47FFC_StaticFields, ___instance_5)); }
	inline AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * get_instance_5() const { return ___instance_5; }
	inline AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIAUDIOMANAGER_TF960318B36B6631CC8000C06CC79BD7EEEF47FFC_H
#ifndef UNITYADSMANAGER_TD5833E6FBB2801A688FE74119AB4CAC5158BA0DC_H
#define UNITYADSMANAGER_TD5833E6FBB2801A688FE74119AB4CAC5158BA0DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityAdsManager
struct  UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityAdsManager::androidGameID
	String_t* ___androidGameID_5;
	// System.Boolean UnityAdsManager::testMode
	bool ___testMode_6;
	// System.String UnityAdsManager::rewardedVideoPlacementId
	String_t* ___rewardedVideoPlacementId_7;
	// System.String UnityAdsManager::regularPlacementId
	String_t* ___regularPlacementId_8;

public:
	inline static int32_t get_offset_of_androidGameID_5() { return static_cast<int32_t>(offsetof(UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC, ___androidGameID_5)); }
	inline String_t* get_androidGameID_5() const { return ___androidGameID_5; }
	inline String_t** get_address_of_androidGameID_5() { return &___androidGameID_5; }
	inline void set_androidGameID_5(String_t* value)
	{
		___androidGameID_5 = value;
		Il2CppCodeGenWriteBarrier((&___androidGameID_5), value);
	}

	inline static int32_t get_offset_of_testMode_6() { return static_cast<int32_t>(offsetof(UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC, ___testMode_6)); }
	inline bool get_testMode_6() const { return ___testMode_6; }
	inline bool* get_address_of_testMode_6() { return &___testMode_6; }
	inline void set_testMode_6(bool value)
	{
		___testMode_6 = value;
	}

	inline static int32_t get_offset_of_rewardedVideoPlacementId_7() { return static_cast<int32_t>(offsetof(UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC, ___rewardedVideoPlacementId_7)); }
	inline String_t* get_rewardedVideoPlacementId_7() const { return ___rewardedVideoPlacementId_7; }
	inline String_t** get_address_of_rewardedVideoPlacementId_7() { return &___rewardedVideoPlacementId_7; }
	inline void set_rewardedVideoPlacementId_7(String_t* value)
	{
		___rewardedVideoPlacementId_7 = value;
		Il2CppCodeGenWriteBarrier((&___rewardedVideoPlacementId_7), value);
	}

	inline static int32_t get_offset_of_regularPlacementId_8() { return static_cast<int32_t>(offsetof(UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC, ___regularPlacementId_8)); }
	inline String_t* get_regularPlacementId_8() const { return ___regularPlacementId_8; }
	inline String_t** get_address_of_regularPlacementId_8() { return &___regularPlacementId_8; }
	inline void set_regularPlacementId_8(String_t* value)
	{
		___regularPlacementId_8 = value;
		Il2CppCodeGenWriteBarrier((&___regularPlacementId_8), value);
	}
};

struct UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC_StaticFields
{
public:
	// UnityAdsManager UnityAdsManager::instance
	UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC_StaticFields, ___instance_4)); }
	inline UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC * get_instance_4() const { return ___instance_4; }
	inline UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSMANAGER_TD5833E6FBB2801A688FE74119AB4CAC5158BA0DC_H
#ifndef UNITYADSTEST_TB995FE7BE7D9E21056A575559495BF17F06BDA96_H
#define UNITYADSTEST_TB995FE7BE7D9E21056A575559495BF17F06BDA96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityAdsTest
struct  UnityAdsTest_tB995FE7BE7D9E21056A575559495BF17F06BDA96  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYADSTEST_TB995FE7BE7D9E21056A575559495BF17F06BDA96_H
#ifndef VOLUMESLIDER_T886BCE4047D78E7ECAC0D913DBE615C756B55A78_H
#define VOLUMESLIDER_T886BCE4047D78E7ECAC0D913DBE615C756B55A78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VolumeSlider
struct  VolumeSlider_t886BCE4047D78E7ECAC0D913DBE615C756B55A78  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioSource VolumeSlider::audioSrc
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSrc_4;
	// System.Single VolumeSlider::musicVolume
	float ___musicVolume_5;

public:
	inline static int32_t get_offset_of_audioSrc_4() { return static_cast<int32_t>(offsetof(VolumeSlider_t886BCE4047D78E7ECAC0D913DBE615C756B55A78, ___audioSrc_4)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSrc_4() const { return ___audioSrc_4; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSrc_4() { return &___audioSrc_4; }
	inline void set_audioSrc_4(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSrc_4 = value;
		Il2CppCodeGenWriteBarrier((&___audioSrc_4), value);
	}

	inline static int32_t get_offset_of_musicVolume_5() { return static_cast<int32_t>(offsetof(VolumeSlider_t886BCE4047D78E7ECAC0D913DBE615C756B55A78, ___musicVolume_5)); }
	inline float get_musicVolume_5() const { return ___musicVolume_5; }
	inline float* get_address_of_musicVolume_5() { return &___musicVolume_5; }
	inline void set_musicVolume_5(float value)
	{
		___musicVolume_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUMESLIDER_T886BCE4047D78E7ECAC0D913DBE615C756B55A78_H
#ifndef WALLBOUNCE_T84F81F6F5785B6BDB2F7FABA23BF8A72D8126EC4_H
#define WALLBOUNCE_T84F81F6F5785B6BDB2F7FABA23BF8A72D8126EC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WallBounce
struct  WallBounce_t84F81F6F5785B6BDB2F7FABA23BF8A72D8126EC4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animator WallBounce::anim
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___anim_4;
	// PlayerController WallBounce::PC
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * ___PC_5;

public:
	inline static int32_t get_offset_of_anim_4() { return static_cast<int32_t>(offsetof(WallBounce_t84F81F6F5785B6BDB2F7FABA23BF8A72D8126EC4, ___anim_4)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_anim_4() const { return ___anim_4; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_anim_4() { return &___anim_4; }
	inline void set_anim_4(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___anim_4 = value;
		Il2CppCodeGenWriteBarrier((&___anim_4), value);
	}

	inline static int32_t get_offset_of_PC_5() { return static_cast<int32_t>(offsetof(WallBounce_t84F81F6F5785B6BDB2F7FABA23BF8A72D8126EC4, ___PC_5)); }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * get_PC_5() const { return ___PC_5; }
	inline PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 ** get_address_of_PC_5() { return &___PC_5; }
	inline void set_PC_5(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85 * value)
	{
		___PC_5 = value;
		Il2CppCodeGenWriteBarrier((&___PC_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WALLBOUNCE_T84F81F6F5785B6BDB2F7FABA23BF8A72D8126EC4_H
#ifndef DYNAMICJOYSTICK_TCF78F682E54A0DC802556F259FCCD587476C2DC3_H
#define DYNAMICJOYSTICK_TCF78F682E54A0DC802556F259FCCD587476C2DC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DynamicJoystick
struct  DynamicJoystick_tCF78F682E54A0DC802556F259FCCD587476C2DC3  : public Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153
{
public:
	// System.Single DynamicJoystick::moveThreshold
	float ___moveThreshold_15;

public:
	inline static int32_t get_offset_of_moveThreshold_15() { return static_cast<int32_t>(offsetof(DynamicJoystick_tCF78F682E54A0DC802556F259FCCD587476C2DC3, ___moveThreshold_15)); }
	inline float get_moveThreshold_15() const { return ___moveThreshold_15; }
	inline float* get_address_of_moveThreshold_15() { return &___moveThreshold_15; }
	inline void set_moveThreshold_15(float value)
	{
		___moveThreshold_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICJOYSTICK_TCF78F682E54A0DC802556F259FCCD587476C2DC3_H
#ifndef FIXEDJOYSTICK_TA793D5240D38B506203B52A20B5E94895CC41909_H
#define FIXEDJOYSTICK_TA793D5240D38B506203B52A20B5E94895CC41909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FixedJoystick
struct  FixedJoystick_tA793D5240D38B506203B52A20B5E94895CC41909  : public Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDJOYSTICK_TA793D5240D38B506203B52A20B5E94895CC41909_H
#ifndef FLOATINGJOYSTICK_TBA7C79E5FC21EE9F117E0583A6A5FD38EFF5F8FE_H
#define FLOATINGJOYSTICK_TBA7C79E5FC21EE9F117E0583A6A5FD38EFF5F8FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloatingJoystick
struct  FloatingJoystick_tBA7C79E5FC21EE9F117E0583A6A5FD38EFF5F8FE  : public Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATINGJOYSTICK_TBA7C79E5FC21EE9F117E0583A6A5FD38EFF5F8FE_H
#ifndef VARIABLEJOYSTICK_T9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1_H
#define VARIABLEJOYSTICK_T9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VariableJoystick
struct  VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1  : public Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153
{
public:
	// System.Single VariableJoystick::moveThreshold
	float ___moveThreshold_15;
	// JoystickType VariableJoystick::joystickType
	int32_t ___joystickType_16;
	// UnityEngine.Vector2 VariableJoystick::fixedPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___fixedPosition_17;

public:
	inline static int32_t get_offset_of_moveThreshold_15() { return static_cast<int32_t>(offsetof(VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1, ___moveThreshold_15)); }
	inline float get_moveThreshold_15() const { return ___moveThreshold_15; }
	inline float* get_address_of_moveThreshold_15() { return &___moveThreshold_15; }
	inline void set_moveThreshold_15(float value)
	{
		___moveThreshold_15 = value;
	}

	inline static int32_t get_offset_of_joystickType_16() { return static_cast<int32_t>(offsetof(VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1, ___joystickType_16)); }
	inline int32_t get_joystickType_16() const { return ___joystickType_16; }
	inline int32_t* get_address_of_joystickType_16() { return &___joystickType_16; }
	inline void set_joystickType_16(int32_t value)
	{
		___joystickType_16 = value;
	}

	inline static int32_t get_offset_of_fixedPosition_17() { return static_cast<int32_t>(offsetof(VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1, ___fixedPosition_17)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_fixedPosition_17() const { return ___fixedPosition_17; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_fixedPosition_17() { return &___fixedPosition_17; }
	inline void set_fixedPosition_17(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___fixedPosition_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLEJOYSTICK_T9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (unityAdsPurchasingGetPurchasingVersion_t6FAC2706D072DAA39D5634C7CBC01302A42EE43B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (unityAdsPurchasingInitialize_t5D3C1F5E55EA709C504F5B479E39D92D34278768), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (U3CU3Ec__DisplayClass29_0_tCBCCDC66D57704B2D5EB0935B24500D03F2E5411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[1] = 
{
	U3CU3Ec__DisplayClass29_0_tCBCCDC66D57704B2D5EB0935B24500D03F2E5411::get_offset_of_placementId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (U3CU3Ec__DisplayClass30_0_t1AB6F6C5823B12DD8552585BA09B19074A2F13AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[2] = 
{
	U3CU3Ec__DisplayClass30_0_t1AB6F6C5823B12DD8552585BA09B19074A2F13AA::get_offset_of_rawError_0(),
	U3CU3Ec__DisplayClass30_0_t1AB6F6C5823B12DD8552585BA09B19074A2F13AA::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (U3CU3Ec__DisplayClass31_0_t671E94813CC93F03B5DB031F8FFFACC1FD99668C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2704[1] = 
{
	U3CU3Ec__DisplayClass31_0_t671E94813CC93F03B5DB031F8FFFACC1FD99668C::get_offset_of_placementId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (U3CU3Ec__DisplayClass32_0_t2BE5122D5B70F219CA03D04B51DCD66A6EA0F4DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[2] = 
{
	U3CU3Ec__DisplayClass32_0_t2BE5122D5B70F219CA03D04B51DCD66A6EA0F4DD::get_offset_of_placementId_0(),
	U3CU3Ec__DisplayClass32_0_t2BE5122D5B70F219CA03D04B51DCD66A6EA0F4DD::get_offset_of_rawShowResult_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (PurchasingEvent_t0574DBB0BD92BE872A7FD505038250E56DF9E1C7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2706[6] = 
{
	PurchasingEvent_t0574DBB0BD92BE872A7FD505038250E56DF9E1C7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2), -1, sizeof(Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2707[10] = 
{
	Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields::get_offset_of_s_PurchasingManagerType_0(),
	Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields::get_offset_of_s_Initialized_1(),
	Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields::get_offset_of_s_PurchasingInitiatePurchaseMethodInfo_2(),
	Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields::get_offset_of_s_PurchasingGetPromoVersionMethodInfo_3(),
	Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields::get_offset_of_s_PurchasingGetPromoCatalogMethodInfo_4(),
	Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields::get_offset_of_s_PurchasingManagerClassName_5(),
	Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields::get_offset_of_s_PurchasingInitiatePurchaseMethodName_6(),
	Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields::get_offset_of_s_PurchasingGetPromoVersionMethodName_7(),
	Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields::get_offset_of_s_PurchasingGetPromoCatalogMethodName_8(),
	Purchasing_t6E5C314B69988BD97CB80A8480B95D7302B93DB2_StaticFields::get_offset_of_s_Platform_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (ReadyEventArgs_tA72D1D6B702B685AEEABEED8CB6B264A4C9F811D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[1] = 
{
	ReadyEventArgs_tA72D1D6B702B685AEEABEED8CB6B264A4C9F811D::get_offset_of_U3CplacementIdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (ShowOptions_t5DB8E1867258D81478370DB18E9D5E9FA0174FB1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2709[2] = 
{
	ShowOptions_t5DB8E1867258D81478370DB18E9D5E9FA0174FB1::get_offset_of_U3CresultCallbackU3Ek__BackingField_0(),
	ShowOptions_t5DB8E1867258D81478370DB18E9D5E9FA0174FB1::get_offset_of_U3CgamerSidU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (ShowResult_t2D262B4453C0ADBD99223216EB569FDDA32516B6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2710[4] = 
{
	ShowResult_t2D262B4453C0ADBD99223216EB569FDDA32516B6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (StartEventArgs_t84C5E305550244AFFF16E687F63FF2ED19320385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[1] = 
{
	StartEventArgs_t84C5E305550244AFFF16E687F63FF2ED19320385::get_offset_of_U3CplacementIdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (UnityEngineApplication_t05457944E7341DF19A1837B5E14BB6FFA8E0CD21), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (UnsupportedPlatform_t66201CAD897970CA608C1A3658018CFA76C53440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[1] = 
{
	UnsupportedPlatform_t66201CAD897970CA608C1A3658018CFA76C53440::get_offset_of_OnFinish_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (Json_t1233A9134669B91C5F52DAF83019662916AC1453), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (Parser_tE4D03B41834709DFEADA93A7339A46C21D037CE8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2715[2] = 
{
	0,
	Parser_tE4D03B41834709DFEADA93A7339A46C21D037CE8::get_offset_of_json_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (TOKEN_t788CFC64AC09A1B3548F66D4E2ED5DC12E6258CD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2716[13] = 
{
	TOKEN_t788CFC64AC09A1B3548F66D4E2ED5DC12E6258CD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (Serializer_t70CBC4A159065B79BAA7B1A81C13FCDE2A6C4C18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2717[1] = 
{
	Serializer_t70CBC4A159065B79BAA7B1A81C13FCDE2A6C4C18::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[7] = 
{
	GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A::get_offset_of__intensity_4(),
	GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A::get_offset_of_shader_5(),
	GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A::get_offset_of_material_6(),
	GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A::get_offset_of_noiseTexture_7(),
	GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A::get_offset_of_oldFrame1_8(),
	GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A::get_offset_of_oldFrame2_9(),
	GlitchFx_t6A31DE633BFF8805214F625E768930C49D0EDC8A::get_offset_of_frameCount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (JoystickPlayerExample_tC21BF4F29E220888E49AD1C28ED50EA82E0C583B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2720[3] = 
{
	JoystickPlayerExample_tC21BF4F29E220888E49AD1C28ED50EA82E0C583B::get_offset_of_speed_4(),
	JoystickPlayerExample_tC21BF4F29E220888E49AD1C28ED50EA82E0C583B::get_offset_of_variableJoystick_5(),
	JoystickPlayerExample_tC21BF4F29E220888E49AD1C28ED50EA82E0C583B::get_offset_of_rb_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (JoystickSetterExample_t0C714E5922483B0E1D4C129300C7C8FD79E4F435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[4] = 
{
	JoystickSetterExample_t0C714E5922483B0E1D4C129300C7C8FD79E4F435::get_offset_of_variableJoystick_4(),
	JoystickSetterExample_t0C714E5922483B0E1D4C129300C7C8FD79E4F435::get_offset_of_valueText_5(),
	JoystickSetterExample_t0C714E5922483B0E1D4C129300C7C8FD79E4F435::get_offset_of_background_6(),
	JoystickSetterExample_t0C714E5922483B0E1D4C129300C7C8FD79E4F435::get_offset_of_axisSprites_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[11] = 
{
	Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153::get_offset_of_handleRange_4(),
	Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153::get_offset_of_deadZone_5(),
	Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153::get_offset_of_axisOptions_6(),
	Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153::get_offset_of_snapX_7(),
	Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153::get_offset_of_snapY_8(),
	Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153::get_offset_of_background_9(),
	Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153::get_offset_of_handle_10(),
	Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153::get_offset_of_baseRect_11(),
	Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153::get_offset_of_canvas_12(),
	Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153::get_offset_of_cam_13(),
	Joystick_t3DF5D60C31824A6BFD16338F9377102BE73A0153::get_offset_of_input_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (AxisOptions_t5AC84B43FFEB20E770F1E4CF5EC2D3AADA9D18D0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2723[4] = 
{
	AxisOptions_t5AC84B43FFEB20E770F1E4CF5EC2D3AADA9D18D0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (DynamicJoystick_tCF78F682E54A0DC802556F259FCCD587476C2DC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2724[1] = 
{
	DynamicJoystick_tCF78F682E54A0DC802556F259FCCD587476C2DC3::get_offset_of_moveThreshold_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (FixedJoystick_tA793D5240D38B506203B52A20B5E94895CC41909), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (FloatingJoystick_tBA7C79E5FC21EE9F117E0583A6A5FD38EFF5F8FE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[3] = 
{
	VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1::get_offset_of_moveThreshold_15(),
	VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1::get_offset_of_joystickType_16(),
	VariableJoystick_t9C1A77B6CFC93759F7E8F5F84F9EDBFF28A80ED1::get_offset_of_fixedPosition_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (JoystickType_t8F089F639C0CED5CA78941C78E1CC72EB61892B6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2728[4] = 
{
	JoystickType_t8F089F639C0CED5CA78941C78E1CC72EB61892B6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2729[5] = 
{
	GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F::get_offset_of_CyclicplayedGameTimes_4(),
	GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F::get_offset_of_UAT_5(),
	GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F::get_offset_of_playedGameTimes_6(),
	GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F::get_offset_of_GM_7(),
	GameCounter_t14CFAEB55D766C8430665F675F336AB4C0D1D89F::get_offset_of_PM_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC), -1, sizeof(UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2730[5] = 
{
	UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC_StaticFields::get_offset_of_instance_4(),
	UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC::get_offset_of_androidGameID_5(),
	UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC::get_offset_of_testMode_6(),
	UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC::get_offset_of_rewardedVideoPlacementId_7(),
	UnityAdsManager_tD5833E6FBB2801A688FE74119AB4CAC5158BA0DC::get_offset_of_regularPlacementId_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (UnityAdsTest_tB995FE7BE7D9E21056A575559495BF17F06BDA96), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23), -1, sizeof(AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2732[3] = 
{
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23::get_offset_of_sounds_4(),
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23_StaticFields::get_offset_of_instance_5(),
	AudioManager_t67152D1A926351222F6AD0C0F2442EAE024C7D23::get_offset_of_m_Scene_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (U3CU3Ec__DisplayClass5_0_tDBFB93D6BB16759DEFE7EDDB05C6414159913063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2733[1] = 
{
	U3CU3Ec__DisplayClass5_0_tDBFB93D6BB16759DEFE7EDDB05C6414159913063::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD), -1, sizeof(SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2734[2] = 
{
	SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD::get_offset_of_sounds_4(),
	SFXManager_t1C53274E4B3E9A99429EE65654E7914DF17739BD_StaticFields::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (U3CU3Ec__DisplayClass4_0_tEC126D91916AC34E93F1B459EC4445CCDDEC7BB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[1] = 
{
	U3CU3Ec__DisplayClass4_0_tEC126D91916AC34E93F1B459EC4445CCDDEC7BB7::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (Sound_tE1867C28E2BC13723E294746060427A1487DF1F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2736[8] = 
{
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_name_0(),
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_clip_1(),
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_volume_2(),
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_pitch_3(),
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_loop_4(),
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_output_5(),
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_source_6(),
	Sound_tE1867C28E2BC13723E294746060427A1487DF1F1::get_offset_of_outputAudioMixerGroup_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (UIAudioManager_tF960318B36B6631CC8000C06CC79BD7EEEF47FFC), -1, sizeof(UIAudioManager_tF960318B36B6631CC8000C06CC79BD7EEEF47FFC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2737[2] = 
{
	UIAudioManager_tF960318B36B6631CC8000C06CC79BD7EEEF47FFC::get_offset_of_sounds_4(),
	UIAudioManager_tF960318B36B6631CC8000C06CC79BD7EEEF47FFC_StaticFields::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (U3CU3Ec__DisplayClass4_0_t76722F36D9DBF3157E79FEE53AA1617CF45E7C14), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2738[1] = 
{
	U3CU3Ec__DisplayClass4_0_t76722F36D9DBF3157E79FEE53AA1617CF45E7C14::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (VolumeSlider_t886BCE4047D78E7ECAC0D913DBE615C756B55A78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[2] = 
{
	VolumeSlider_t886BCE4047D78E7ECAC0D913DBE615C756B55A78::get_offset_of_audioSrc_4(),
	VolumeSlider_t886BCE4047D78E7ECAC0D913DBE615C756B55A78::get_offset_of_musicVolume_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[4] = 
{
	CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142::get_offset_of_fallSpeed_4(),
	CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142::get_offset_of_isMoving_5(),
	CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142::get_offset_of_xOffset_6(),
	CameraFollow_t427BFD1DE36FAC87DF2D53C9E956FE0F39DEB142::get_offset_of_yOffset_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[13] = 
{
	CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51::get_offset_of_GC_4(),
	CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51::get_offset_of_GE_5(),
	CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51::get_offset_of_GF_6(),
	CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51::get_offset_of_AG_7(),
	CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51::get_offset_of_playedTimes_8(),
	CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51::get_offset_of_n1_9(),
	CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51::get_offset_of_n2_10(),
	CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51::get_offset_of_n3_11(),
	CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51::get_offset_of_n4_12(),
	CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51::get_offset_of_n5_13(),
	CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51::get_offset_of_n6_14(),
	CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51::get_offset_of_n7_15(),
	CameraGlitch_t766226B76251A4C6BC42452E60B8A4FFE8A4DB51::get_offset_of_nUntilEffects_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2742[2] = 
{
	CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4::get_offset_of_rb_4(),
	CameraMovement_t20ED5C3DA8B48B3D044213B5BFB0A101154F96E4::get_offset_of_fallSpeed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[7] = 
{
	ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2::get_offset_of_smooth_4(),
	ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2::get_offset_of_newColor_5(),
	ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2::get_offset_of_SR_6(),
	ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2::get_offset_of_fadeToBlueAmount_7(),
	ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2::get_offset_of_fadeToGreenAmount_8(),
	ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2::get_offset_of_fadeToRedAmount_9(),
	ColorLerp_t0C4DAC64DED0C0F0BB0FEDB933415A91693A85C2::get_offset_of_fadingSpeed_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (U3CFadeToBlueU3Ed__11_t2A35DAEF14F0F1CE3186738A1FA2603E9E1E9827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[4] = 
{
	U3CFadeToBlueU3Ed__11_t2A35DAEF14F0F1CE3186738A1FA2603E9E1E9827::get_offset_of_U3CU3E1__state_0(),
	U3CFadeToBlueU3Ed__11_t2A35DAEF14F0F1CE3186738A1FA2603E9E1E9827::get_offset_of_U3CU3E2__current_1(),
	U3CFadeToBlueU3Ed__11_t2A35DAEF14F0F1CE3186738A1FA2603E9E1E9827::get_offset_of_U3CU3E4__this_2(),
	U3CFadeToBlueU3Ed__11_t2A35DAEF14F0F1CE3186738A1FA2603E9E1E9827::get_offset_of_U3CiU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (U3COnWaitU3Ed__13_t44C65DD445D849BFAC53EB0B13141D1C51DB238C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[3] = 
{
	U3COnWaitU3Ed__13_t44C65DD445D849BFAC53EB0B13141D1C51DB238C::get_offset_of_U3CU3E1__state_0(),
	U3COnWaitU3Ed__13_t44C65DD445D849BFAC53EB0B13141D1C51DB238C::get_offset_of_U3CU3E2__current_1(),
	U3COnWaitU3Ed__13_t44C65DD445D849BFAC53EB0B13141D1C51DB238C::get_offset_of_sec_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[2] = 
{
	CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445::get_offset_of_playerSR_4(),
	CrazyColorChange_t686A4AF5D1A6ED35DF0A461E7A7838D8094D4445::get_offset_of_isCrazyColorChanging_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (ParticleCCC_tF0DEE3A8537B9C95EAAC9B93F64A4AD46B7B73FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[2] = 
{
	ParticleCCC_tF0DEE3A8537B9C95EAAC9B93F64A4AD46B7B73FA::get_offset_of_startColor_4(),
	ParticleCCC_tF0DEE3A8537B9C95EAAC9B93F64A4AD46B7B73FA::get_offset_of_isCrazyColorChanging_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2748[17] = 
{
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_ColorGroup_4(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_ColorImage_5(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_startlerpFade_6(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_startlerpTime_7(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_startColor_8(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_finishColor_9(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_SR_10(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_sColor_11(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_eColor_12(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_duration_13(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_t_14(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_every_15(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_colorstep_16(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_colors_17(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_i_18(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_lerpedColor_19(),
	RandomColorLerp_tC7B190341E7444C43211DC08216A296A13653666::get_offset_of_GM_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (U3CUpdateColorU3Ed__20_t3FCA7F437809572BBD84AAA1530D9B56D2F6F762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[4] = 
{
	U3CUpdateColorU3Ed__20_t3FCA7F437809572BBD84AAA1530D9B56D2F6F762::get_offset_of_U3CU3E1__state_0(),
	U3CUpdateColorU3Ed__20_t3FCA7F437809572BBD84AAA1530D9B56D2F6F762::get_offset_of_U3CU3E2__current_1(),
	U3CUpdateColorU3Ed__20_t3FCA7F437809572BBD84AAA1530D9B56D2F6F762::get_offset_of_U3CU3E4__this_2(),
	U3CUpdateColorU3Ed__20_t3FCA7F437809572BBD84AAA1530D9B56D2F6F762::get_offset_of_func_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2750[7] = 
{
	U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2::get_offset_of_U3CU3E1__state_0(),
	U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2::get_offset_of_U3CU3E2__current_1(),
	U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2::get_offset_of_color_2(),
	U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2::get_offset_of_U3CU3E4__this_3(),
	U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2::get_offset_of_transitionTime_4(),
	U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2::get_offset_of_func_5(),
	U3CUpdateFadeRedU3Ed__24_tB4F13466E870A0C10E65D2B528B2786D59F016C2::get_offset_of_U3CtU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[10] = 
{
	TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD::get_offset_of_SR_4(),
	TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD::get_offset_of_duration_5(),
	TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD::get_offset_of_colorstep_6(),
	TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD::get_offset_of_colors_7(),
	TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD::get_offset_of_i_8(),
	TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD::get_offset_of_lerpedColor_9(),
	TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD::get_offset_of_GM_10(),
	TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD::get_offset_of_N_11(),
	TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD::get_offset_of_colorduration_12(),
	TriangleColor_t56B96AF6F83658FBED5BFCE6572D3CD6D04525AD::get_offset_of_n_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (Highscore_t5A992FDEECF661ADB50FD15D68A56AD0041DE2B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[5] = 
{
	Highscore_t5A992FDEECF661ADB50FD15D68A56AD0041DE2B0::get_offset_of_S1_4(),
	Highscore_t5A992FDEECF661ADB50FD15D68A56AD0041DE2B0::get_offset_of_scoreCounter_5(),
	Highscore_t5A992FDEECF661ADB50FD15D68A56AD0041DE2B0::get_offset_of_highScore_6(),
	Highscore_t5A992FDEECF661ADB50FD15D68A56AD0041DE2B0::get_offset_of_player_7(),
	Highscore_t5A992FDEECF661ADB50FD15D68A56AD0041DE2B0::get_offset_of_ShardScore_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[5] = 
{
	Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119::get_offset_of_text_4(),
	Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119::get_offset_of_player_5(),
	Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119::get_offset_of_fscore_6(),
	Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119::get_offset_of_score1_7(),
	Score1_t32C63EBC59C6554F6F2DA2A5F1D932EBD61D8119::get_offset_of_GM_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (ShardCounter_t3AF47A70BD8A226972075B08CF5F364173DCADE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[2] = 
{
	ShardCounter_t3AF47A70BD8A226972075B08CF5F364173DCADE6::get_offset_of_text_4(),
	ShardCounter_t3AF47A70BD8A226972075B08CF5F364173DCADE6::get_offset_of_player_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63), -1, sizeof(FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2755[6] = 
{
	FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63_StaticFields::get_offset_of_Instance_4(),
	FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63::get_offset_of_fadeGroup_5(),
	FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63::get_offset_of_fadeImage_6(),
	FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63::get_offset_of_startWithFade_7(),
	FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63::get_offset_of_startFadeTime_8(),
	FadeManager_tAA803744A3B56BFE7D7BC1D78D8AD5F127CB6F63::get_offset_of_startFadeColor_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (U3CUpdateFadeInU3Ed__10_t59D91E61872645E158D875325C09DDE83163DC77), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2756[6] = 
{
	U3CUpdateFadeInU3Ed__10_t59D91E61872645E158D875325C09DDE83163DC77::get_offset_of_U3CU3E1__state_0(),
	U3CUpdateFadeInU3Ed__10_t59D91E61872645E158D875325C09DDE83163DC77::get_offset_of_U3CU3E2__current_1(),
	U3CUpdateFadeInU3Ed__10_t59D91E61872645E158D875325C09DDE83163DC77::get_offset_of_U3CU3E4__this_2(),
	U3CUpdateFadeInU3Ed__10_t59D91E61872645E158D875325C09DDE83163DC77::get_offset_of_transitionTime_3(),
	U3CUpdateFadeInU3Ed__10_t59D91E61872645E158D875325C09DDE83163DC77::get_offset_of_func_4(),
	U3CUpdateFadeInU3Ed__10_t59D91E61872645E158D875325C09DDE83163DC77::get_offset_of_U3CtU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (U3CUpdateFadeOutU3Ed__14_t378395BE6BFA0E5893A07C37EAA07F88363A8161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2757[6] = 
{
	U3CUpdateFadeOutU3Ed__14_t378395BE6BFA0E5893A07C37EAA07F88363A8161::get_offset_of_U3CU3E1__state_0(),
	U3CUpdateFadeOutU3Ed__14_t378395BE6BFA0E5893A07C37EAA07F88363A8161::get_offset_of_U3CU3E2__current_1(),
	U3CUpdateFadeOutU3Ed__14_t378395BE6BFA0E5893A07C37EAA07F88363A8161::get_offset_of_U3CU3E4__this_2(),
	U3CUpdateFadeOutU3Ed__14_t378395BE6BFA0E5893A07C37EAA07F88363A8161::get_offset_of_transitionTime_3(),
	U3CUpdateFadeOutU3Ed__14_t378395BE6BFA0E5893A07C37EAA07F88363A8161::get_offset_of_func_4(),
	U3CUpdateFadeOutU3Ed__14_t378395BE6BFA0E5893A07C37EAA07F88363A8161::get_offset_of_U3CtU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (FadeManagerTest_t4C9317895745C15FE6D914A97FD0230BA442FB11), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (DestroyFinishedParticle_t0886D79AF36A4C48739BDBF9F111AE953884C9DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[1] = 
{
	DestroyFinishedParticle_t0886D79AF36A4C48739BDBF9F111AE953884C9DD::get_offset_of_thisParticleSystem_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (DestroyGameObjectOverTime_tE562613A730D168CB3500DED69F5192AA8F6D724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2760[4] = 
{
	DestroyGameObjectOverTime_tE562613A730D168CB3500DED69F5192AA8F6D724::get_offset_of_lifetime_4(),
	DestroyGameObjectOverTime_tE562613A730D168CB3500DED69F5192AA8F6D724::get_offset_of_DfromPlayer_5(),
	DestroyGameObjectOverTime_tE562613A730D168CB3500DED69F5192AA8F6D724::get_offset_of_player_6(),
	DestroyGameObjectOverTime_tE562613A730D168CB3500DED69F5192AA8F6D724::get_offset_of_DY_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (SpritesGlitch_tBDBCC7C8FEC92CD55EA62E39C4444A86A378E442), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2762[7] = 
{
	AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D::get_offset_of__scanLineJitter_4(),
	AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D::get_offset_of__verticalJump_5(),
	AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D::get_offset_of__horizontalShake_6(),
	AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D::get_offset_of__colorDrift_7(),
	AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D::get_offset_of__shader_8(),
	AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D::get_offset_of__material_9(),
	AnalogGlitch_t92C13F7DA52341788C5070C934405D92D486D86D::get_offset_of__verticalJumpTime_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26), -1, sizeof(CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2763[15] = 
{
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26::get_offset_of_duration_4(),
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26::get_offset_of_explodeParticleEffect_5(),
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26::get_offset_of_explodepickupEffect_6(),
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26::get_offset_of_Player_7(),
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26::get_offset_of_playerScript_8(),
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26::get_offset_of_SFXM_9(),
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26::get_offset_of_GM_10(),
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26_StaticFields::get_offset_of_OldMoveSpeed_11(),
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26::get_offset_of_playerSR_12(),
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26::get_offset_of_cduration_13(),
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26::get_offset_of_t_14(),
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26::get_offset_of_playerBlankColor_15(),
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26::get_offset_of_playerIsFlashing_16(),
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26::get_offset_of_CCC_17(),
	CoinMagnet_t15DFE535293F6D06BF459D88CCC778DB5E954A26::get_offset_of_anim_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (U3CCoinMagnetPickupU3Ed__17_tD8524B7B36542781DE80094F3FDE21EBD72AF670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[4] = 
{
	U3CCoinMagnetPickupU3Ed__17_tD8524B7B36542781DE80094F3FDE21EBD72AF670::get_offset_of_U3CU3E1__state_0(),
	U3CCoinMagnetPickupU3Ed__17_tD8524B7B36542781DE80094F3FDE21EBD72AF670::get_offset_of_U3CU3E2__current_1(),
	U3CCoinMagnetPickupU3Ed__17_tD8524B7B36542781DE80094F3FDE21EBD72AF670::get_offset_of_U3CU3E4__this_2(),
	U3CCoinMagnetPickupU3Ed__17_tD8524B7B36542781DE80094F3FDE21EBD72AF670::get_offset_of_player_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (GroundSpawner_t1C5CDDD4D769EDD3A69E8C7145A2D18C92D2B934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[3] = 
{
	GroundSpawner_t1C5CDDD4D769EDD3A69E8C7145A2D18C92D2B934::get_offset_of_track_4(),
	GroundSpawner_t1C5CDDD4D769EDD3A69E8C7145A2D18C92D2B934::get_offset_of_spawned_5(),
	GroundSpawner_t1C5CDDD4D769EDD3A69E8C7145A2D18C92D2B934::get_offset_of_displacementY_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2766[7] = 
{
	Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E::get_offset_of_Player_4(),
	Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E::get_offset_of_PC_5(),
	Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E::get_offset_of_SFXM_6(),
	Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E::get_offset_of_GM_7(),
	Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E::get_offset_of_mycol_8(),
	Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E::get_offset_of_currentcolor_9(),
	Obstacle_t45A47093FD0D640DE4C85A2CB52665B5192A232E::get_offset_of_nextcolor_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (U3ConWaitU3Ed__10_t40593F43BE2EB83D23E24877C98CAE6EAC008FA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2767[4] = 
{
	U3ConWaitU3Ed__10_t40593F43BE2EB83D23E24877C98CAE6EAC008FA1::get_offset_of_U3CU3E1__state_0(),
	U3ConWaitU3Ed__10_t40593F43BE2EB83D23E24877C98CAE6EAC008FA1::get_offset_of_U3CU3E2__current_1(),
	U3ConWaitU3Ed__10_t40593F43BE2EB83D23E24877C98CAE6EAC008FA1::get_offset_of_sec_2(),
	U3ConWaitU3Ed__10_t40593F43BE2EB83D23E24877C98CAE6EAC008FA1::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2768[7] = 
{
	Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5::get_offset_of_xDistance_4(),
	Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5::get_offset_of_minSpread_5(),
	Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5::get_offset_of_maxSpread_6(),
	Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5::get_offset_of_playerTransform_7(),
	Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5::get_offset_of_obstaclePrefab_8(),
	Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5::get_offset_of_xSpread_9(),
	Obstaclescube_tC10D5FE1BFB6542BCC7486CB6F71064F09E480E5::get_offset_of_lastXPos_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (ObstacleSpawning_t660785F865CF293ACE3FC388FE62969E5F4AFA08), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2769[5] = 
{
	ObstacleSpawning_t660785F865CF293ACE3FC388FE62969E5F4AFA08::get_offset_of_player_4(),
	ObstacleSpawning_t660785F865CF293ACE3FC388FE62969E5F4AFA08::get_offset_of_obstacle_5(),
	ObstacleSpawning_t660785F865CF293ACE3FC388FE62969E5F4AFA08::get_offset_of_amountOfObstacles_6(),
	ObstacleSpawning_t660785F865CF293ACE3FC388FE62969E5F4AFA08::get_offset_of_minX_7(),
	ObstacleSpawning_t660785F865CF293ACE3FC388FE62969E5F4AFA08::get_offset_of_maxX_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C), -1, sizeof(PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2770[17] = 
{
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_duration_4(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_turboParticleEffect_5(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_pickupEffect_6(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_explodeParticleEffect_7(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_explodepickupEffect_8(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_Player_9(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_playerScript_10(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_SFXM_11(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_GM_12(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C_StaticFields::get_offset_of_OldMoveSpeed_13(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_playerSR_14(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_cduration_15(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_t_16(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_playerBlankColor_17(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_playerIsFlashing_18(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_CCC_19(),
	PowerUP_t19FBBC933A32D6966BE54BF3B5E15BD3DA10B92C::get_offset_of_anim_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (U3CPickupSpeedU3Ed__19_tA800DD001CB87D6A9A63B9EE3A322F9191B3B575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2771[4] = 
{
	U3CPickupSpeedU3Ed__19_tA800DD001CB87D6A9A63B9EE3A322F9191B3B575::get_offset_of_U3CU3E1__state_0(),
	U3CPickupSpeedU3Ed__19_tA800DD001CB87D6A9A63B9EE3A322F9191B3B575::get_offset_of_U3CU3E2__current_1(),
	U3CPickupSpeedU3Ed__19_tA800DD001CB87D6A9A63B9EE3A322F9191B3B575::get_offset_of_U3CU3E4__this_2(),
	U3CPickupSpeedU3Ed__19_tA800DD001CB87D6A9A63B9EE3A322F9191B3B575::get_offset_of_player_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2772[7] = 
{
	U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316::get_offset_of_U3CU3E1__state_0(),
	U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316::get_offset_of_U3CU3E2__current_1(),
	U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316::get_offset_of_color_2(),
	U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316::get_offset_of_U3CU3E4__this_3(),
	U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316::get_offset_of_transitionTime_4(),
	U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316::get_offset_of_func_5(),
	U3CUpdateplayerSRcolorU3Ed__21_tB93250B2D169A83011CF5E039BA59363896FA316::get_offset_of_U3CtU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2773[14] = 
{
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112::get_offset_of_duration_4(),
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112::get_offset_of_InvicibleParticleEffect_5(),
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112::get_offset_of_pickupEffect_6(),
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112::get_offset_of_Player_7(),
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112::get_offset_of_playerScript_8(),
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112::get_offset_of_SFXM_9(),
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112::get_offset_of_GM_10(),
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112::get_offset_of_playerSR_11(),
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112::get_offset_of_cduration_12(),
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112::get_offset_of_t_13(),
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112::get_offset_of_playerBlankColor_14(),
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112::get_offset_of_playerIsFlashing_15(),
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112::get_offset_of_CCC_16(),
	PowerUpInvicible_t696BAC1110ED86CA29287FF9FD25AC5D63B26112::get_offset_of_anim_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (U3CPickupInvicibleU3Ed__16_tB872EDB503901CEBC9AC9477300A9978C925D9BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[4] = 
{
	U3CPickupInvicibleU3Ed__16_tB872EDB503901CEBC9AC9477300A9978C925D9BB::get_offset_of_U3CU3E1__state_0(),
	U3CPickupInvicibleU3Ed__16_tB872EDB503901CEBC9AC9477300A9978C925D9BB::get_offset_of_U3CU3E2__current_1(),
	U3CPickupInvicibleU3Ed__16_tB872EDB503901CEBC9AC9477300A9978C925D9BB::get_offset_of_U3CU3E4__this_2(),
	U3CPickupInvicibleU3Ed__16_tB872EDB503901CEBC9AC9477300A9978C925D9BB::get_offset_of_player_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[13] = 
{
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26::get_offset_of_multiplier_4(),
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26::get_offset_of_duration_5(),
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26::get_offset_of_MiniParticleEffect_6(),
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26::get_offset_of_pickupEffect_7(),
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26::get_offset_of_Player_8(),
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26::get_offset_of_playerScript_9(),
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26::get_offset_of_SFXM_10(),
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26::get_offset_of_GM_11(),
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26::get_offset_of_playerSR_12(),
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26::get_offset_of_cduration_13(),
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26::get_offset_of_t_14(),
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26::get_offset_of_playerBlankColor_15(),
	PowerUpMini_t5B4E4C8AC7A52B49D6D6107A3AADC98D4DC08A26::get_offset_of_playerIsFlashing_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (U3CPickupMiniU3Ed__15_t614D69EDEC901644A4F87BB119474E3312E6A250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[4] = 
{
	U3CPickupMiniU3Ed__15_t614D69EDEC901644A4F87BB119474E3312E6A250::get_offset_of_U3CU3E1__state_0(),
	U3CPickupMiniU3Ed__15_t614D69EDEC901644A4F87BB119474E3312E6A250::get_offset_of_U3CU3E2__current_1(),
	U3CPickupMiniU3Ed__15_t614D69EDEC901644A4F87BB119474E3312E6A250::get_offset_of_U3CU3E4__this_2(),
	U3CPickupMiniU3Ed__15_t614D69EDEC901644A4F87BB119474E3312E6A250::get_offset_of_player_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[7] = 
{
	U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC::get_offset_of_U3CU3E1__state_0(),
	U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC::get_offset_of_U3CU3E2__current_1(),
	U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC::get_offset_of_color_2(),
	U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC::get_offset_of_U3CU3E4__this_3(),
	U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC::get_offset_of_transitionTime_4(),
	U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC::get_offset_of_func_5(),
	U3CUpdateplayerSRcolorU3Ed__17_t4A8002D750E1D34C9F3EA751E6C9F9F2357047EC::get_offset_of_U3CtU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2778[14] = 
{
	PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB::get_offset_of_player_4(),
	PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB::get_offset_of_playerRb_5(),
	PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB::get_offset_of_Star_6(),
	PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB::get_offset_of_Star2_7(),
	PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB::get_offset_of_Star3_8(),
	PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB::get_offset_of_CoinMagnetObject_9(),
	PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB::get_offset_of_amountOfPowerUps_10(),
	PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB::get_offset_of_minX_11(),
	PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB::get_offset_of_maxX_12(),
	PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB::get_offset_of_start_13(),
	PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB::get_offset_of_GM_14(),
	PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB::get_offset_of_sPositionY_15(),
	PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB::get_offset_of_cPositionY_16(),
	PowerUpSpawning_tA1354036C94B3B391B27D8AA65890EFB811CC9BB::get_offset_of_spawnInterval_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (Shard_t91C7467F054A32443C2F199338B29BF534ECE0F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2779[4] = 
{
	Shard_t91C7467F054A32443C2F199338B29BF534ECE0F5::get_offset_of_SFXM_4(),
	Shard_t91C7467F054A32443C2F199338B29BF534ECE0F5::get_offset_of_player_5(),
	Shard_t91C7467F054A32443C2F199338B29BF534ECE0F5::get_offset_of_isCoinMagnetAffected_6(),
	Shard_t91C7467F054A32443C2F199338B29BF534ECE0F5::get_offset_of_rb_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (ShardSpawning_t4B2F40A0798829663C401DEB595C1D2FDE4ABBAA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2780[5] = 
{
	ShardSpawning_t4B2F40A0798829663C401DEB595C1D2FDE4ABBAA::get_offset_of_player_4(),
	ShardSpawning_t4B2F40A0798829663C401DEB595C1D2FDE4ABBAA::get_offset_of_Shard_5(),
	ShardSpawning_t4B2F40A0798829663C401DEB595C1D2FDE4ABBAA::get_offset_of_amountOfShards_6(),
	ShardSpawning_t4B2F40A0798829663C401DEB595C1D2FDE4ABBAA::get_offset_of_minX_7(),
	ShardSpawning_t4B2F40A0798829663C401DEB595C1D2FDE4ABBAA::get_offset_of_maxX_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (WallBounce_t84F81F6F5785B6BDB2F7FABA23BF8A72D8126EC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2781[2] = 
{
	WallBounce_t84F81F6F5785B6BDB2F7FABA23BF8A72D8126EC4::get_offset_of_anim_4(),
	WallBounce_t84F81F6F5785B6BDB2F7FABA23BF8A72D8126EC4::get_offset_of_PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (LaunchPreview_t6D5C9A5543C80FBD86CAB533D40A443035DE132D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2782[2] = 
{
	LaunchPreview_t6D5C9A5543C80FBD86CAB533D40A443035DE132D::get_offset_of_lineRenderer_4(),
	LaunchPreview_t6D5C9A5543C80FBD86CAB533D40A443035DE132D::get_offset_of_dragStartPoint_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89), -1, sizeof(GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2783[24] = 
{
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_gameHasEnded_4(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_restartDelay_5(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_DeathMenu_6(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_SFXM_7(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_PM_8(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_playerObject_9(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_player_10(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_Camera_11(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_camF_12(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_BG3_13(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_BG3F_14(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_moveSpeed_15(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_accel_16(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_n_17(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_DistanceY_18(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_paused_19(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_T_20(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_CyclicT_21(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_start_22(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89_StaticFields::get_offset_of_moveSpeedinitial_23(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_Turbo_24(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_TurboBoostMult_25(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_AM_26(),
	GameManager_tAC830B937D5E37F47803FE8AB44CAB0762B77B89::get_offset_of_GC_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2784[3] = 
{
	LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343::get_offset_of_loadingScreen_4(),
	LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343::get_offset_of_slider_5(),
	LevelLoader_t528DC9F40D2C5040043E386207C89B1400AE0343::get_offset_of_progressText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (U3CLoadAsynchronouslyU3Ed__4_t2440E89200DF7A02FD1803A18D9569C7912479A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2785[5] = 
{
	U3CLoadAsynchronouslyU3Ed__4_t2440E89200DF7A02FD1803A18D9569C7912479A0::get_offset_of_U3CU3E1__state_0(),
	U3CLoadAsynchronouslyU3Ed__4_t2440E89200DF7A02FD1803A18D9569C7912479A0::get_offset_of_U3CU3E2__current_1(),
	U3CLoadAsynchronouslyU3Ed__4_t2440E89200DF7A02FD1803A18D9569C7912479A0::get_offset_of_sceneIndex_2(),
	U3CLoadAsynchronouslyU3Ed__4_t2440E89200DF7A02FD1803A18D9569C7912479A0::get_offset_of_U3CU3E4__this_3(),
	U3CLoadAsynchronouslyU3Ed__4_t2440E89200DF7A02FD1803A18D9569C7912479A0::get_offset_of_U3CoperationU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2786[8] = 
{
	LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D::get_offset_of_currentCheckpoint_4(),
	LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D::get_offset_of_player_5(),
	LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D::get_offset_of_deathParticle_6(),
	LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D::get_offset_of_respawnParticle_7(),
	LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D::get_offset_of_pointPenaltyOnDeath_8(),
	LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D::get_offset_of_respawnDelay_9(),
	LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D::get_offset_of_gravityStore_10(),
	LevelManager_t4694E049F1814D6105F42EA77B02E163824A2B2D::get_offset_of_cam_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (U3CRespawnPlayerCoU3Ed__11_tA504E021A606B04BF0A6292CB74D8142ACC388E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2787[3] = 
{
	U3CRespawnPlayerCoU3Ed__11_tA504E021A606B04BF0A6292CB74D8142ACC388E6::get_offset_of_U3CU3E1__state_0(),
	U3CRespawnPlayerCoU3Ed__11_tA504E021A606B04BF0A6292CB74D8142ACC388E6::get_offset_of_U3CU3E2__current_1(),
	U3CRespawnPlayerCoU3Ed__11_tA504E021A606B04BF0A6292CB74D8142ACC388E6::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (MainMenu_t7CD5D54EA3EBFAE6ECFE46E095EFEBFD14C45105), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (OptionsMenu_t6FC54B44E11AEAB0BE9088478B24E686823BB4CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2789[3] = 
{
	OptionsMenu_t6FC54B44E11AEAB0BE9088478B24E686823BB4CD::get_offset_of_audioMixer_4(),
	OptionsMenu_t6FC54B44E11AEAB0BE9088478B24E686823BB4CD::get_offset_of_resolutions_5(),
	OptionsMenu_t6FC54B44E11AEAB0BE9088478B24E686823BB4CD::get_offset_of_resolutionDropdown_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791), -1, sizeof(PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2790[10] = 
{
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791_StaticFields::get_offset_of_GameIsPaused_4(),
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791::get_offset_of_pauseMenuUI_5(),
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791::get_offset_of_menuSceneName_6(),
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791::get_offset_of_paused_7(),
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791::get_offset_of_playing_8(),
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791::get_offset_of_canvas_9(),
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791::get_offset_of_AM_10(),
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791::get_offset_of_player_11(),
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791::get_offset_of_playerScript_12(),
	PauseMenu_tE6D6728ABA83B6BB731E1D06DC22A5A54656D791::get_offset_of_GM_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (U3ConPauseU3Ed__21_tF67253F1393DCC25954C5A6D351C6611E7F69551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[3] = 
{
	U3ConPauseU3Ed__21_tF67253F1393DCC25954C5A6D351C6611E7F69551::get_offset_of_U3CU3E1__state_0(),
	U3ConPauseU3Ed__21_tF67253F1393DCC25954C5A6D351C6611E7F69551::get_offset_of_U3CU3E2__current_1(),
	U3ConPauseU3Ed__21_tF67253F1393DCC25954C5A6D351C6611E7F69551::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (StartMenu_t16A2DA0B3CBABA283134C0B7AA0403F13F231016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2792[5] = 
{
	StartMenu_t16A2DA0B3CBABA283134C0B7AA0403F13F231016::get_offset_of_menuContainer_4(),
	StartMenu_t16A2DA0B3CBABA283134C0B7AA0403F13F231016::get_offset_of_smooth_5(),
	StartMenu_t16A2DA0B3CBABA283134C0B7AA0403F13F231016::get_offset_of_smoothSpeed_6(),
	StartMenu_t16A2DA0B3CBABA283134C0B7AA0403F13F231016::get_offset_of_menuPositions_7(),
	StartMenu_t16A2DA0B3CBABA283134C0B7AA0403F13F231016::get_offset_of_desiredPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (Move_t48FAC5147B90F0345C8CB913CDEF6D8B3FC63B35), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B), -1, sizeof(GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2794[4] = 
{
	GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B_StaticFields::get_offset_of_instance_4(),
	GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B::get_offset_of_gyro_5(),
	GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B::get_offset_of_rotation_6(),
	GyroManager_t42C5DCCDCEFF05359E0D0ECDD817C023BC2B9F7B::get_offset_of_gyroActive_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85), -1, sizeof(PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2795[43] = 
{
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_rb_4(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_movespeed_5(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_moveright_6(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_moveleft_7(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_moveup_8(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_movedown_9(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_movevelocity_10(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_movevelocity2_11(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_jump_12(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_jumpheight_13(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_RightWallCheck_14(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_LeftWallCheck_15(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_WallCheckRadius_16(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_whatIsWall_17(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_TouchingRightWall_18(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_TouchingLeftWall_19(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_startPosition_20(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_Shard_21(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_anim_22(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_fallSpeed_23(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_levelIndex_24(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_gameManager_25(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_position_26(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_SFXM_27(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_playerX_28(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_playerY_29(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85_StaticFields::get_offset_of_pauseCheck_30(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_isPaused_31(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_startDragPosition_32(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_endDragPosition_33(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_currentPositon_34(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_worldPosition_35(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_lastPositon_36(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_fallAcc_37(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_DeathParticle_38(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_DeathEffect_39(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_moveType1_40(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_moveType2_41(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_moveType3_42(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_joystick_43(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_horizontalMove_44(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_verticalMove_45(),
	PlayerController_t4CE339054014370D89B89922EDC0EA2766589C85::get_offset_of_cointMagnetOn_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (U3ConWaitU3Ed__50_t81D18DD5B09E7E2A1733614448B37B2F33623C67), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2796[3] = 
{
	U3ConWaitU3Ed__50_t81D18DD5B09E7E2A1733614448B37B2F33623C67::get_offset_of_U3CU3E1__state_0(),
	U3ConWaitU3Ed__50_t81D18DD5B09E7E2A1733614448B37B2F33623C67::get_offset_of_U3CU3E2__current_1(),
	U3ConWaitU3Ed__50_t81D18DD5B09E7E2A1733614448B37B2F33623C67::get_offset_of_sec_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2798[7] = 
{
	FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941::get_offset_of_textMesh_4(),
	FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941::get_offset_of_animSpeedInSec_5(),
	FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941::get_offset_of_keepAnimating_6(),
	FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941::get_offset_of_transitionTime_7(),
	FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941::get_offset_of_playerIsFlashing_8(),
	FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941::get_offset_of_GM_9(),
	FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941::get_offset_of_T_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2799[7] = 
{
	U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88::get_offset_of_U3CU3E1__state_0(),
	U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88::get_offset_of_U3CU3E2__current_1(),
	U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88::get_offset_of_U3CU3E4__this_2(),
	U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88::get_offset_of_U3CcurrentColorU3E5__2_3(),
	U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88::get_offset_of_U3CinvisibleColorU3E5__3_4(),
	U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88::get_offset_of_U3ColdAnimSpeedInSecU3E5__4_5(),
	U3CanimU3Ed__7_tEE266E252916FB7108641A4D24EF5C9F772D5E88::get_offset_of_U3CcounterU3E5__5_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
