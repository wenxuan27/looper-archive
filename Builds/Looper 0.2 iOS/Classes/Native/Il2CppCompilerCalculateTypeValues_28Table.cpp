﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// FlashingTMPro
struct FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941;
// Reaktion.ConstantMotion/TransformElement
struct TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>
struct Dictionary_2_tF0B057F97104E66AB1254AA29CE3110D741C1012;
// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>
struct Dictionary_2_t47C4D15CAB841519F5AB4F09CDA5851B051DB746;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.RenderTexture
struct RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B;
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E;
// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39;




#ifndef U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#define U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef CROSSPLATFORMINPUTMANAGER_T538BC5CB3009F635C0D8B708E339CE82E4C6152E_H
#define CROSSPLATFORMINPUTMANAGER_T538BC5CB3009F635C0D8B708E339CE82E4C6152E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager
struct  CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E  : public RuntimeObject
{
public:

public:
};

struct CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields
{
public:
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::activeInput
	VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * ___activeInput_0;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_TouchInput
	VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * ___s_TouchInput_1;
	// UnityStandardAssets.CrossPlatformInput.VirtualInput UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::s_HardwareInput
	VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * ___s_HardwareInput_2;

public:
	inline static int32_t get_offset_of_activeInput_0() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields, ___activeInput_0)); }
	inline VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * get_activeInput_0() const { return ___activeInput_0; }
	inline VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 ** get_address_of_activeInput_0() { return &___activeInput_0; }
	inline void set_activeInput_0(VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * value)
	{
		___activeInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___activeInput_0), value);
	}

	inline static int32_t get_offset_of_s_TouchInput_1() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields, ___s_TouchInput_1)); }
	inline VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * get_s_TouchInput_1() const { return ___s_TouchInput_1; }
	inline VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 ** get_address_of_s_TouchInput_1() { return &___s_TouchInput_1; }
	inline void set_s_TouchInput_1(VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * value)
	{
		___s_TouchInput_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_TouchInput_1), value);
	}

	inline static int32_t get_offset_of_s_HardwareInput_2() { return static_cast<int32_t>(offsetof(CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields, ___s_HardwareInput_2)); }
	inline VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * get_s_HardwareInput_2() const { return ___s_HardwareInput_2; }
	inline VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 ** get_address_of_s_HardwareInput_2() { return &___s_HardwareInput_2; }
	inline void set_s_HardwareInput_2(VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39 * value)
	{
		___s_HardwareInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_HardwareInput_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSPLATFORMINPUTMANAGER_T538BC5CB3009F635C0D8B708E339CE82E4C6152E_H
#ifndef VIRTUALAXIS_TE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B_H
#define VIRTUALAXIS_TE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct  VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::m_Value
	float ___m_Value_1;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B, ___U3CmatchWithInputManagerU3Ek__BackingField_2)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_2() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_2() { return &___U3CmatchWithInputManagerU3Ek__BackingField_2; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_2(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALAXIS_TE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B_H
#ifndef VIRTUALBUTTON_TD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA_H
#define VIRTUALBUTTON_TD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
struct  VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA  : public RuntimeObject
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::<matchWithInputManager>k__BackingField
	bool ___U3CmatchWithInputManagerU3Ek__BackingField_1;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_LastPressedFrame
	int32_t ___m_LastPressedFrame_2;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_ReleasedFrame
	int32_t ___m_ReleasedFrame_3;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::m_Pressed
	bool ___m_Pressed_4;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA, ___U3CmatchWithInputManagerU3Ek__BackingField_1)); }
	inline bool get_U3CmatchWithInputManagerU3Ek__BackingField_1() const { return ___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CmatchWithInputManagerU3Ek__BackingField_1() { return &___U3CmatchWithInputManagerU3Ek__BackingField_1; }
	inline void set_U3CmatchWithInputManagerU3Ek__BackingField_1(bool value)
	{
		___U3CmatchWithInputManagerU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_m_LastPressedFrame_2() { return static_cast<int32_t>(offsetof(VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA, ___m_LastPressedFrame_2)); }
	inline int32_t get_m_LastPressedFrame_2() const { return ___m_LastPressedFrame_2; }
	inline int32_t* get_address_of_m_LastPressedFrame_2() { return &___m_LastPressedFrame_2; }
	inline void set_m_LastPressedFrame_2(int32_t value)
	{
		___m_LastPressedFrame_2 = value;
	}

	inline static int32_t get_offset_of_m_ReleasedFrame_3() { return static_cast<int32_t>(offsetof(VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA, ___m_ReleasedFrame_3)); }
	inline int32_t get_m_ReleasedFrame_3() const { return ___m_ReleasedFrame_3; }
	inline int32_t* get_address_of_m_ReleasedFrame_3() { return &___m_ReleasedFrame_3; }
	inline void set_m_ReleasedFrame_3(int32_t value)
	{
		___m_ReleasedFrame_3 = value;
	}

	inline static int32_t get_offset_of_m_Pressed_4() { return static_cast<int32_t>(offsetof(VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA, ___m_Pressed_4)); }
	inline bool get_m_Pressed_4() const { return ___m_Pressed_4; }
	inline bool* get_address_of_m_Pressed_4() { return &___m_Pressed_4; }
	inline void set_m_Pressed_4(bool value)
	{
		___m_Pressed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_TD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef U3CANIMNONSTOPU3ED__8_T4E0A78064846E9A9854908125617F1A04908FABB_H
#define U3CANIMNONSTOPU3ED__8_T4E0A78064846E9A9854908125617F1A04908FABB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlashingTMPro/<animnonstop>d__8
struct  U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB  : public RuntimeObject
{
public:
	// System.Int32 FlashingTMPro/<animnonstop>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object FlashingTMPro/<animnonstop>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// FlashingTMPro FlashingTMPro/<animnonstop>d__8::<>4__this
	FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941 * ___U3CU3E4__this_2;
	// UnityEngine.Color FlashingTMPro/<animnonstop>d__8::<currentColor>5__2
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___U3CcurrentColorU3E5__2_3;
	// UnityEngine.Color FlashingTMPro/<animnonstop>d__8::<invisibleColor>5__3
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___U3CinvisibleColorU3E5__3_4;
	// System.Single FlashingTMPro/<animnonstop>d__8::<counter>5__4
	float ___U3CcounterU3E5__4_5;
	// System.Single FlashingTMPro/<animnonstop>d__8::<t>5__5
	float ___U3CtU3E5__5_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB, ___U3CU3E4__this_2)); }
	inline FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FlashingTMPro_tC949DD4F1DEAF186D2B2A43CF804CF3B90BCE941 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CcurrentColorU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB, ___U3CcurrentColorU3E5__2_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_U3CcurrentColorU3E5__2_3() const { return ___U3CcurrentColorU3E5__2_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_U3CcurrentColorU3E5__2_3() { return &___U3CcurrentColorU3E5__2_3; }
	inline void set_U3CcurrentColorU3E5__2_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___U3CcurrentColorU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CinvisibleColorU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB, ___U3CinvisibleColorU3E5__3_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_U3CinvisibleColorU3E5__3_4() const { return ___U3CinvisibleColorU3E5__3_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_U3CinvisibleColorU3E5__3_4() { return &___U3CinvisibleColorU3E5__3_4; }
	inline void set_U3CinvisibleColorU3E5__3_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___U3CinvisibleColorU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB, ___U3CcounterU3E5__4_5)); }
	inline float get_U3CcounterU3E5__4_5() const { return ___U3CcounterU3E5__4_5; }
	inline float* get_address_of_U3CcounterU3E5__4_5() { return &___U3CcounterU3E5__4_5; }
	inline void set_U3CcounterU3E5__4_5(float value)
	{
		___U3CcounterU3E5__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB, ___U3CtU3E5__5_6)); }
	inline float get_U3CtU3E5__5_6() const { return ___U3CtU3E5__5_6; }
	inline float* get_address_of_U3CtU3E5__5_6() { return &___U3CtU3E5__5_6; }
	inline void set_U3CtU3E5__5_6(float value)
	{
		___U3CtU3E5__5_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMNONSTOPU3ED__8_T4E0A78064846E9A9854908125617F1A04908FABB_H
#ifndef TRANSFORMMODE_T9211BB6207A27934EDC5BCF647487CDD7B17F796_H
#define TRANSFORMMODE_T9211BB6207A27934EDC5BCF647487CDD7B17F796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reaktion.ConstantMotion/TransformMode
struct  TransformMode_t9211BB6207A27934EDC5BCF647487CDD7B17F796 
{
public:
	// System.Int32 Reaktion.ConstantMotion/TransformMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransformMode_t9211BB6207A27934EDC5BCF647487CDD7B17F796, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMMODE_T9211BB6207A27934EDC5BCF647487CDD7B17F796_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef ACTIVEINPUTMETHOD_T809D9C23DD316DBC93963EAF0FAF0FA63CBF8C53_H
#define ACTIVEINPUTMETHOD_T809D9C23DD316DBC93963EAF0FAF0FA63CBF8C53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod
struct  ActiveInputMethod_t809D9C23DD316DBC93963EAF0FAF0FA63CBF8C53 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActiveInputMethod_t809D9C23DD316DBC93963EAF0FAF0FA63CBF8C53, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEINPUTMETHOD_T809D9C23DD316DBC93963EAF0FAF0FA63CBF8C53_H
#ifndef AXISOPTION_T2E4FAD7B3F55CE394680B6CEA9C2F01B92849786_H
#define AXISOPTION_T2E4FAD7B3F55CE394680B6CEA9C2F01B92849786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption
struct  AxisOption_t2E4FAD7B3F55CE394680B6CEA9C2F01B92849786 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisOption_t2E4FAD7B3F55CE394680B6CEA9C2F01B92849786, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_T2E4FAD7B3F55CE394680B6CEA9C2F01B92849786_H
#ifndef MAPPINGTYPE_TA5DC189BC7DB7B5AE049E710D502B88A32BA87B3_H
#define MAPPINGTYPE_TA5DC189BC7DB7B5AE049E710D502B88A32BA87B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType
struct  MappingType_tA5DC189BC7DB7B5AE049E710D502B88A32BA87B3 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MappingType_tA5DC189BC7DB7B5AE049E710D502B88A32BA87B3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGTYPE_TA5DC189BC7DB7B5AE049E710D502B88A32BA87B3_H
#ifndef AXISOPTIONS_T7D42CBC0AAF7DFE9DF7335044264BAF17BA6CC71_H
#define AXISOPTIONS_T7D42CBC0AAF7DFE9DF7335044264BAF17BA6CC71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions
struct  AxisOptions_t7D42CBC0AAF7DFE9DF7335044264BAF17BA6CC71 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisOptions_t7D42CBC0AAF7DFE9DF7335044264BAF17BA6CC71, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTIONS_T7D42CBC0AAF7DFE9DF7335044264BAF17BA6CC71_H
#ifndef AXISOPTION_TA48A0481101792CB4679A6C5D81F14DB1582FF11_H
#define AXISOPTION_TA48A0481101792CB4679A6C5D81F14DB1582FF11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption
struct  AxisOption_tA48A0481101792CB4679A6C5D81F14DB1582FF11 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisOption_tA48A0481101792CB4679A6C5D81F14DB1582FF11, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISOPTION_TA48A0481101792CB4679A6C5D81F14DB1582FF11_H
#ifndef CONTROLSTYLE_T60A34C5903F112F7E6982BB8161F9F7372669CAC_H
#define CONTROLSTYLE_T60A34C5903F112F7E6982BB8161F9F7372669CAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle
struct  ControlStyle_t60A34C5903F112F7E6982BB8161F9F7372669CAC 
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControlStyle_t60A34C5903F112F7E6982BB8161F9F7372669CAC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSTYLE_T60A34C5903F112F7E6982BB8161F9F7372669CAC_H
#ifndef VIRTUALINPUT_TCCB9F78E94F5E3C6E5162E7AA746292D827BAF39_H
#define VIRTUALINPUT_TCCB9F78E94F5E3C6E5162E7AA746292D827BAF39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct  VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::<virtualMousePosition>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CvirtualMousePositionU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualAxes
	Dictionary_2_tF0B057F97104E66AB1254AA29CE3110D741C1012 * ___m_VirtualAxes_1;
	// System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_VirtualButtons
	Dictionary_2_t47C4D15CAB841519F5AB4F09CDA5851B051DB746 * ___m_VirtualButtons_2;
	// System.Collections.Generic.List`1<System.String> UnityStandardAssets.CrossPlatformInput.VirtualInput::m_AlwaysUseVirtual
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___m_AlwaysUseVirtual_3;

public:
	inline static int32_t get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39, ___U3CvirtualMousePositionU3Ek__BackingField_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CvirtualMousePositionU3Ek__BackingField_0() const { return ___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CvirtualMousePositionU3Ek__BackingField_0() { return &___U3CvirtualMousePositionU3Ek__BackingField_0; }
	inline void set_U3CvirtualMousePositionU3Ek__BackingField_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CvirtualMousePositionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_VirtualAxes_1() { return static_cast<int32_t>(offsetof(VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39, ___m_VirtualAxes_1)); }
	inline Dictionary_2_tF0B057F97104E66AB1254AA29CE3110D741C1012 * get_m_VirtualAxes_1() const { return ___m_VirtualAxes_1; }
	inline Dictionary_2_tF0B057F97104E66AB1254AA29CE3110D741C1012 ** get_address_of_m_VirtualAxes_1() { return &___m_VirtualAxes_1; }
	inline void set_m_VirtualAxes_1(Dictionary_2_tF0B057F97104E66AB1254AA29CE3110D741C1012 * value)
	{
		___m_VirtualAxes_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualAxes_1), value);
	}

	inline static int32_t get_offset_of_m_VirtualButtons_2() { return static_cast<int32_t>(offsetof(VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39, ___m_VirtualButtons_2)); }
	inline Dictionary_2_t47C4D15CAB841519F5AB4F09CDA5851B051DB746 * get_m_VirtualButtons_2() const { return ___m_VirtualButtons_2; }
	inline Dictionary_2_t47C4D15CAB841519F5AB4F09CDA5851B051DB746 ** get_address_of_m_VirtualButtons_2() { return &___m_VirtualButtons_2; }
	inline void set_m_VirtualButtons_2(Dictionary_2_t47C4D15CAB841519F5AB4F09CDA5851B051DB746 * value)
	{
		___m_VirtualButtons_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_VirtualButtons_2), value);
	}

	inline static int32_t get_offset_of_m_AlwaysUseVirtual_3() { return static_cast<int32_t>(offsetof(VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39, ___m_AlwaysUseVirtual_3)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_m_AlwaysUseVirtual_3() const { return ___m_AlwaysUseVirtual_3; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_m_AlwaysUseVirtual_3() { return &___m_AlwaysUseVirtual_3; }
	inline void set_m_AlwaysUseVirtual_3(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___m_AlwaysUseVirtual_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlwaysUseVirtual_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALINPUT_TCCB9F78E94F5E3C6E5162E7AA746292D827BAF39_H
#ifndef TRANSFORMELEMENT_TEA1EC4D96832D8C69E0F7612210ACF741D2D01FC_H
#define TRANSFORMELEMENT_TEA1EC4D96832D8C69E0F7612210ACF741D2D01FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reaktion.ConstantMotion/TransformElement
struct  TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC  : public RuntimeObject
{
public:
	// Reaktion.ConstantMotion/TransformMode Reaktion.ConstantMotion/TransformElement::mode
	int32_t ___mode_0;
	// System.Single Reaktion.ConstantMotion/TransformElement::velocity
	float ___velocity_1;
	// UnityEngine.Vector3 Reaktion.ConstantMotion/TransformElement::arbitraryVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___arbitraryVector_2;
	// System.Single Reaktion.ConstantMotion/TransformElement::randomness
	float ___randomness_3;
	// UnityEngine.Vector3 Reaktion.ConstantMotion/TransformElement::randomVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___randomVector_4;
	// System.Single Reaktion.ConstantMotion/TransformElement::randomScalar
	float ___randomScalar_5;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_velocity_1() { return static_cast<int32_t>(offsetof(TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC, ___velocity_1)); }
	inline float get_velocity_1() const { return ___velocity_1; }
	inline float* get_address_of_velocity_1() { return &___velocity_1; }
	inline void set_velocity_1(float value)
	{
		___velocity_1 = value;
	}

	inline static int32_t get_offset_of_arbitraryVector_2() { return static_cast<int32_t>(offsetof(TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC, ___arbitraryVector_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_arbitraryVector_2() const { return ___arbitraryVector_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_arbitraryVector_2() { return &___arbitraryVector_2; }
	inline void set_arbitraryVector_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___arbitraryVector_2 = value;
	}

	inline static int32_t get_offset_of_randomness_3() { return static_cast<int32_t>(offsetof(TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC, ___randomness_3)); }
	inline float get_randomness_3() const { return ___randomness_3; }
	inline float* get_address_of_randomness_3() { return &___randomness_3; }
	inline void set_randomness_3(float value)
	{
		___randomness_3 = value;
	}

	inline static int32_t get_offset_of_randomVector_4() { return static_cast<int32_t>(offsetof(TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC, ___randomVector_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_randomVector_4() const { return ___randomVector_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_randomVector_4() { return &___randomVector_4; }
	inline void set_randomVector_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___randomVector_4 = value;
	}

	inline static int32_t get_offset_of_randomScalar_5() { return static_cast<int32_t>(offsetof(TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC, ___randomScalar_5)); }
	inline float get_randomScalar_5() const { return ___randomScalar_5; }
	inline float* get_address_of_randomScalar_5() { return &___randomScalar_5; }
	inline void set_randomScalar_5(float value)
	{
		___randomScalar_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMELEMENT_TEA1EC4D96832D8C69E0F7612210ACF741D2D01FC_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef MOBILEINPUT_T219DDF915FF54DBE73004E0163F8BFCB4DA3BA44_H
#define MOBILEINPUT_T219DDF915FF54DBE73004E0163F8BFCB4DA3BA44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
struct  MobileInput_t219DDF915FF54DBE73004E0163F8BFCB4DA3BA44  : public VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEINPUT_T219DDF915FF54DBE73004E0163F8BFCB4DA3BA44_H
#ifndef STANDALONEINPUT_T9BFD53669697FC88D0D237110D8D1DF76400AFEB_H
#define STANDALONEINPUT_T9BFD53669697FC88D0D237110D8D1DF76400AFEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
struct  StandaloneInput_t9BFD53669697FC88D0D237110D8D1DF76400AFEB  : public VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEINPUT_T9BFD53669697FC88D0D237110D8D1DF76400AFEB_H
#ifndef AXISMAPPING_T9B9CB140F322262AC1E126846B4BD1C92586923E_H
#define AXISMAPPING_T9B9CB140F322262AC1E126846B4BD1C92586923E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct  AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E  : public RuntimeObject
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping/MappingType UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::type
	int32_t ___type_0;
	// System.String UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::axisName
	String_t* ___axisName_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_axisName_1() { return static_cast<int32_t>(offsetof(AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E, ___axisName_1)); }
	inline String_t* get_axisName_1() const { return ___axisName_1; }
	inline String_t** get_address_of_axisName_1() { return &___axisName_1; }
	inline void set_axisName_1(String_t* value)
	{
		___axisName_1 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISMAPPING_T9B9CB140F322262AC1E126846B4BD1C92586923E_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef GLITCHEFFECT_T56F0423B1561DC48494DA08642884B0DAC895C83_H
#define GLITCHEFFECT_T56F0423B1561DC48494DA08642884B0DAC895C83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlitchEffect
struct  GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture2D GlitchEffect::displacementMap
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___displacementMap_4;
	// UnityEngine.Shader GlitchEffect::Shader
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___Shader_5;
	// System.Single GlitchEffect::intensity
	float ___intensity_6;
	// System.Single GlitchEffect::flipIntensity
	float ___flipIntensity_7;
	// System.Single GlitchEffect::colorIntensity
	float ___colorIntensity_8;
	// System.Single GlitchEffect::_glitchup
	float ____glitchup_9;
	// System.Single GlitchEffect::_glitchdown
	float ____glitchdown_10;
	// System.Single GlitchEffect::flicker
	float ___flicker_11;
	// System.Single GlitchEffect::_glitchupTime
	float ____glitchupTime_12;
	// System.Single GlitchEffect::_glitchdownTime
	float ____glitchdownTime_13;
	// System.Single GlitchEffect::_flickerTime
	float ____flickerTime_14;
	// UnityEngine.Material GlitchEffect::_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____material_15;

public:
	inline static int32_t get_offset_of_displacementMap_4() { return static_cast<int32_t>(offsetof(GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83, ___displacementMap_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_displacementMap_4() const { return ___displacementMap_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_displacementMap_4() { return &___displacementMap_4; }
	inline void set_displacementMap_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___displacementMap_4 = value;
		Il2CppCodeGenWriteBarrier((&___displacementMap_4), value);
	}

	inline static int32_t get_offset_of_Shader_5() { return static_cast<int32_t>(offsetof(GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83, ___Shader_5)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_Shader_5() const { return ___Shader_5; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_Shader_5() { return &___Shader_5; }
	inline void set_Shader_5(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___Shader_5 = value;
		Il2CppCodeGenWriteBarrier((&___Shader_5), value);
	}

	inline static int32_t get_offset_of_intensity_6() { return static_cast<int32_t>(offsetof(GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83, ___intensity_6)); }
	inline float get_intensity_6() const { return ___intensity_6; }
	inline float* get_address_of_intensity_6() { return &___intensity_6; }
	inline void set_intensity_6(float value)
	{
		___intensity_6 = value;
	}

	inline static int32_t get_offset_of_flipIntensity_7() { return static_cast<int32_t>(offsetof(GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83, ___flipIntensity_7)); }
	inline float get_flipIntensity_7() const { return ___flipIntensity_7; }
	inline float* get_address_of_flipIntensity_7() { return &___flipIntensity_7; }
	inline void set_flipIntensity_7(float value)
	{
		___flipIntensity_7 = value;
	}

	inline static int32_t get_offset_of_colorIntensity_8() { return static_cast<int32_t>(offsetof(GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83, ___colorIntensity_8)); }
	inline float get_colorIntensity_8() const { return ___colorIntensity_8; }
	inline float* get_address_of_colorIntensity_8() { return &___colorIntensity_8; }
	inline void set_colorIntensity_8(float value)
	{
		___colorIntensity_8 = value;
	}

	inline static int32_t get_offset_of__glitchup_9() { return static_cast<int32_t>(offsetof(GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83, ____glitchup_9)); }
	inline float get__glitchup_9() const { return ____glitchup_9; }
	inline float* get_address_of__glitchup_9() { return &____glitchup_9; }
	inline void set__glitchup_9(float value)
	{
		____glitchup_9 = value;
	}

	inline static int32_t get_offset_of__glitchdown_10() { return static_cast<int32_t>(offsetof(GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83, ____glitchdown_10)); }
	inline float get__glitchdown_10() const { return ____glitchdown_10; }
	inline float* get_address_of__glitchdown_10() { return &____glitchdown_10; }
	inline void set__glitchdown_10(float value)
	{
		____glitchdown_10 = value;
	}

	inline static int32_t get_offset_of_flicker_11() { return static_cast<int32_t>(offsetof(GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83, ___flicker_11)); }
	inline float get_flicker_11() const { return ___flicker_11; }
	inline float* get_address_of_flicker_11() { return &___flicker_11; }
	inline void set_flicker_11(float value)
	{
		___flicker_11 = value;
	}

	inline static int32_t get_offset_of__glitchupTime_12() { return static_cast<int32_t>(offsetof(GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83, ____glitchupTime_12)); }
	inline float get__glitchupTime_12() const { return ____glitchupTime_12; }
	inline float* get_address_of__glitchupTime_12() { return &____glitchupTime_12; }
	inline void set__glitchupTime_12(float value)
	{
		____glitchupTime_12 = value;
	}

	inline static int32_t get_offset_of__glitchdownTime_13() { return static_cast<int32_t>(offsetof(GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83, ____glitchdownTime_13)); }
	inline float get__glitchdownTime_13() const { return ____glitchdownTime_13; }
	inline float* get_address_of__glitchdownTime_13() { return &____glitchdownTime_13; }
	inline void set__glitchdownTime_13(float value)
	{
		____glitchdownTime_13 = value;
	}

	inline static int32_t get_offset_of__flickerTime_14() { return static_cast<int32_t>(offsetof(GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83, ____flickerTime_14)); }
	inline float get__flickerTime_14() const { return ____flickerTime_14; }
	inline float* get_address_of__flickerTime_14() { return &____flickerTime_14; }
	inline void set__flickerTime_14(float value)
	{
		____flickerTime_14 = value;
	}

	inline static int32_t get_offset_of__material_15() { return static_cast<int32_t>(offsetof(GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83, ____material_15)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__material_15() const { return ____material_15; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__material_15() { return &____material_15; }
	inline void set__material_15(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____material_15 = value;
		Il2CppCodeGenWriteBarrier((&____material_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLITCHEFFECT_T56F0423B1561DC48494DA08642884B0DAC895C83_H
#ifndef DIGITALGLITCH_T9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5_H
#define DIGITALGLITCH_T9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Kino.DigitalGlitch
struct  DigitalGlitch_t9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Kino.DigitalGlitch::_intensity
	float ____intensity_4;
	// UnityEngine.Shader Kino.DigitalGlitch::_shader
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ____shader_5;
	// UnityEngine.Material Kino.DigitalGlitch::_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____material_6;
	// UnityEngine.Texture2D Kino.DigitalGlitch::_noiseTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ____noiseTexture_7;
	// UnityEngine.RenderTexture Kino.DigitalGlitch::_trashFrame1
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____trashFrame1_8;
	// UnityEngine.RenderTexture Kino.DigitalGlitch::_trashFrame2
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____trashFrame2_9;

public:
	inline static int32_t get_offset_of__intensity_4() { return static_cast<int32_t>(offsetof(DigitalGlitch_t9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5, ____intensity_4)); }
	inline float get__intensity_4() const { return ____intensity_4; }
	inline float* get_address_of__intensity_4() { return &____intensity_4; }
	inline void set__intensity_4(float value)
	{
		____intensity_4 = value;
	}

	inline static int32_t get_offset_of__shader_5() { return static_cast<int32_t>(offsetof(DigitalGlitch_t9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5, ____shader_5)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get__shader_5() const { return ____shader_5; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of__shader_5() { return &____shader_5; }
	inline void set__shader_5(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		____shader_5 = value;
		Il2CppCodeGenWriteBarrier((&____shader_5), value);
	}

	inline static int32_t get_offset_of__material_6() { return static_cast<int32_t>(offsetof(DigitalGlitch_t9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5, ____material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__material_6() const { return ____material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__material_6() { return &____material_6; }
	inline void set__material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____material_6 = value;
		Il2CppCodeGenWriteBarrier((&____material_6), value);
	}

	inline static int32_t get_offset_of__noiseTexture_7() { return static_cast<int32_t>(offsetof(DigitalGlitch_t9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5, ____noiseTexture_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get__noiseTexture_7() const { return ____noiseTexture_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of__noiseTexture_7() { return &____noiseTexture_7; }
	inline void set__noiseTexture_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		____noiseTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&____noiseTexture_7), value);
	}

	inline static int32_t get_offset_of__trashFrame1_8() { return static_cast<int32_t>(offsetof(DigitalGlitch_t9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5, ____trashFrame1_8)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__trashFrame1_8() const { return ____trashFrame1_8; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__trashFrame1_8() { return &____trashFrame1_8; }
	inline void set__trashFrame1_8(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____trashFrame1_8 = value;
		Il2CppCodeGenWriteBarrier((&____trashFrame1_8), value);
	}

	inline static int32_t get_offset_of__trashFrame2_9() { return static_cast<int32_t>(offsetof(DigitalGlitch_t9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5, ____trashFrame2_9)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__trashFrame2_9() const { return ____trashFrame2_9; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__trashFrame2_9() { return &____trashFrame2_9; }
	inline void set__trashFrame2_9(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____trashFrame2_9 = value;
		Il2CppCodeGenWriteBarrier((&____trashFrame2_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGITALGLITCH_T9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5_H
#ifndef CONSTANTMOTION_T9BC470E76522142165F54735A47CE184FE38C640_H
#define CONSTANTMOTION_T9BC470E76522142165F54735A47CE184FE38C640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reaktion.ConstantMotion
struct  ConstantMotion_t9BC470E76522142165F54735A47CE184FE38C640  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Reaktion.ConstantMotion/TransformElement Reaktion.ConstantMotion::position
	TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC * ___position_4;
	// Reaktion.ConstantMotion/TransformElement Reaktion.ConstantMotion::rotation
	TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC * ___rotation_5;
	// System.Boolean Reaktion.ConstantMotion::useLocalCoordinate
	bool ___useLocalCoordinate_6;

public:
	inline static int32_t get_offset_of_position_4() { return static_cast<int32_t>(offsetof(ConstantMotion_t9BC470E76522142165F54735A47CE184FE38C640, ___position_4)); }
	inline TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC * get_position_4() const { return ___position_4; }
	inline TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC ** get_address_of_position_4() { return &___position_4; }
	inline void set_position_4(TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC * value)
	{
		___position_4 = value;
		Il2CppCodeGenWriteBarrier((&___position_4), value);
	}

	inline static int32_t get_offset_of_rotation_5() { return static_cast<int32_t>(offsetof(ConstantMotion_t9BC470E76522142165F54735A47CE184FE38C640, ___rotation_5)); }
	inline TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC * get_rotation_5() const { return ___rotation_5; }
	inline TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC ** get_address_of_rotation_5() { return &___rotation_5; }
	inline void set_rotation_5(TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC * value)
	{
		___rotation_5 = value;
		Il2CppCodeGenWriteBarrier((&___rotation_5), value);
	}

	inline static int32_t get_offset_of_useLocalCoordinate_6() { return static_cast<int32_t>(offsetof(ConstantMotion_t9BC470E76522142165F54735A47CE184FE38C640, ___useLocalCoordinate_6)); }
	inline bool get_useLocalCoordinate_6() const { return ___useLocalCoordinate_6; }
	inline bool* get_address_of_useLocalCoordinate_6() { return &___useLocalCoordinate_6; }
	inline void set_useLocalCoordinate_6(bool value)
	{
		___useLocalCoordinate_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTMOTION_T9BC470E76522142165F54735A47CE184FE38C640_H
#ifndef JITTERMOTION_T61F410943C49D9F8E4244D58A34A85DBCA17EB0A_H
#define JITTERMOTION_T61F410943C49D9F8E4244D58A34A85DBCA17EB0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reaktion.JitterMotion
struct  JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Reaktion.JitterMotion::positionFrequency
	float ___positionFrequency_4;
	// System.Single Reaktion.JitterMotion::rotationFrequency
	float ___rotationFrequency_5;
	// System.Single Reaktion.JitterMotion::positionAmount
	float ___positionAmount_6;
	// System.Single Reaktion.JitterMotion::rotationAmount
	float ___rotationAmount_7;
	// UnityEngine.Vector3 Reaktion.JitterMotion::positionComponents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positionComponents_8;
	// UnityEngine.Vector3 Reaktion.JitterMotion::rotationComponents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rotationComponents_9;
	// System.Int32 Reaktion.JitterMotion::positionOctave
	int32_t ___positionOctave_10;
	// System.Int32 Reaktion.JitterMotion::rotationOctave
	int32_t ___rotationOctave_11;
	// System.Single Reaktion.JitterMotion::timePosition
	float ___timePosition_12;
	// System.Single Reaktion.JitterMotion::timeRotation
	float ___timeRotation_13;
	// UnityEngine.Vector2[] Reaktion.JitterMotion::noiseVectors
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___noiseVectors_14;
	// UnityEngine.Vector3 Reaktion.JitterMotion::initialPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___initialPosition_15;
	// UnityEngine.Quaternion Reaktion.JitterMotion::initialRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___initialRotation_16;

public:
	inline static int32_t get_offset_of_positionFrequency_4() { return static_cast<int32_t>(offsetof(JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A, ___positionFrequency_4)); }
	inline float get_positionFrequency_4() const { return ___positionFrequency_4; }
	inline float* get_address_of_positionFrequency_4() { return &___positionFrequency_4; }
	inline void set_positionFrequency_4(float value)
	{
		___positionFrequency_4 = value;
	}

	inline static int32_t get_offset_of_rotationFrequency_5() { return static_cast<int32_t>(offsetof(JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A, ___rotationFrequency_5)); }
	inline float get_rotationFrequency_5() const { return ___rotationFrequency_5; }
	inline float* get_address_of_rotationFrequency_5() { return &___rotationFrequency_5; }
	inline void set_rotationFrequency_5(float value)
	{
		___rotationFrequency_5 = value;
	}

	inline static int32_t get_offset_of_positionAmount_6() { return static_cast<int32_t>(offsetof(JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A, ___positionAmount_6)); }
	inline float get_positionAmount_6() const { return ___positionAmount_6; }
	inline float* get_address_of_positionAmount_6() { return &___positionAmount_6; }
	inline void set_positionAmount_6(float value)
	{
		___positionAmount_6 = value;
	}

	inline static int32_t get_offset_of_rotationAmount_7() { return static_cast<int32_t>(offsetof(JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A, ___rotationAmount_7)); }
	inline float get_rotationAmount_7() const { return ___rotationAmount_7; }
	inline float* get_address_of_rotationAmount_7() { return &___rotationAmount_7; }
	inline void set_rotationAmount_7(float value)
	{
		___rotationAmount_7 = value;
	}

	inline static int32_t get_offset_of_positionComponents_8() { return static_cast<int32_t>(offsetof(JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A, ___positionComponents_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positionComponents_8() const { return ___positionComponents_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positionComponents_8() { return &___positionComponents_8; }
	inline void set_positionComponents_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positionComponents_8 = value;
	}

	inline static int32_t get_offset_of_rotationComponents_9() { return static_cast<int32_t>(offsetof(JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A, ___rotationComponents_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rotationComponents_9() const { return ___rotationComponents_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rotationComponents_9() { return &___rotationComponents_9; }
	inline void set_rotationComponents_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rotationComponents_9 = value;
	}

	inline static int32_t get_offset_of_positionOctave_10() { return static_cast<int32_t>(offsetof(JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A, ___positionOctave_10)); }
	inline int32_t get_positionOctave_10() const { return ___positionOctave_10; }
	inline int32_t* get_address_of_positionOctave_10() { return &___positionOctave_10; }
	inline void set_positionOctave_10(int32_t value)
	{
		___positionOctave_10 = value;
	}

	inline static int32_t get_offset_of_rotationOctave_11() { return static_cast<int32_t>(offsetof(JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A, ___rotationOctave_11)); }
	inline int32_t get_rotationOctave_11() const { return ___rotationOctave_11; }
	inline int32_t* get_address_of_rotationOctave_11() { return &___rotationOctave_11; }
	inline void set_rotationOctave_11(int32_t value)
	{
		___rotationOctave_11 = value;
	}

	inline static int32_t get_offset_of_timePosition_12() { return static_cast<int32_t>(offsetof(JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A, ___timePosition_12)); }
	inline float get_timePosition_12() const { return ___timePosition_12; }
	inline float* get_address_of_timePosition_12() { return &___timePosition_12; }
	inline void set_timePosition_12(float value)
	{
		___timePosition_12 = value;
	}

	inline static int32_t get_offset_of_timeRotation_13() { return static_cast<int32_t>(offsetof(JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A, ___timeRotation_13)); }
	inline float get_timeRotation_13() const { return ___timeRotation_13; }
	inline float* get_address_of_timeRotation_13() { return &___timeRotation_13; }
	inline void set_timeRotation_13(float value)
	{
		___timeRotation_13 = value;
	}

	inline static int32_t get_offset_of_noiseVectors_14() { return static_cast<int32_t>(offsetof(JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A, ___noiseVectors_14)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_noiseVectors_14() const { return ___noiseVectors_14; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_noiseVectors_14() { return &___noiseVectors_14; }
	inline void set_noiseVectors_14(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___noiseVectors_14 = value;
		Il2CppCodeGenWriteBarrier((&___noiseVectors_14), value);
	}

	inline static int32_t get_offset_of_initialPosition_15() { return static_cast<int32_t>(offsetof(JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A, ___initialPosition_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_initialPosition_15() const { return ___initialPosition_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_initialPosition_15() { return &___initialPosition_15; }
	inline void set_initialPosition_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___initialPosition_15 = value;
	}

	inline static int32_t get_offset_of_initialRotation_16() { return static_cast<int32_t>(offsetof(JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A, ___initialRotation_16)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_initialRotation_16() const { return ___initialRotation_16; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_initialRotation_16() { return &___initialRotation_16; }
	inline void set_initialRotation_16(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___initialRotation_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JITTERMOTION_T61F410943C49D9F8E4244D58A34A85DBCA17EB0A_H
#ifndef AXISTOUCHBUTTON_T124E142E5813D33A6407C920758816933327BEC4_H
#define AXISTOUCHBUTTON_T124E142E5813D33A6407C920758816933327BEC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
struct  AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisName
	String_t* ___axisName_4;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::axisValue
	float ___axisValue_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::responseSpeed
	float ___responseSpeed_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.AxisTouchButton::returnToCentreSpeed
	float ___returnToCentreSpeed_7;
	// UnityStandardAssets.CrossPlatformInput.AxisTouchButton UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_PairedWith
	AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4 * ___m_PairedWith_8;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.AxisTouchButton::m_Axis
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * ___m_Axis_9;

public:
	inline static int32_t get_offset_of_axisName_4() { return static_cast<int32_t>(offsetof(AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4, ___axisName_4)); }
	inline String_t* get_axisName_4() const { return ___axisName_4; }
	inline String_t** get_address_of_axisName_4() { return &___axisName_4; }
	inline void set_axisName_4(String_t* value)
	{
		___axisName_4 = value;
		Il2CppCodeGenWriteBarrier((&___axisName_4), value);
	}

	inline static int32_t get_offset_of_axisValue_5() { return static_cast<int32_t>(offsetof(AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4, ___axisValue_5)); }
	inline float get_axisValue_5() const { return ___axisValue_5; }
	inline float* get_address_of_axisValue_5() { return &___axisValue_5; }
	inline void set_axisValue_5(float value)
	{
		___axisValue_5 = value;
	}

	inline static int32_t get_offset_of_responseSpeed_6() { return static_cast<int32_t>(offsetof(AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4, ___responseSpeed_6)); }
	inline float get_responseSpeed_6() const { return ___responseSpeed_6; }
	inline float* get_address_of_responseSpeed_6() { return &___responseSpeed_6; }
	inline void set_responseSpeed_6(float value)
	{
		___responseSpeed_6 = value;
	}

	inline static int32_t get_offset_of_returnToCentreSpeed_7() { return static_cast<int32_t>(offsetof(AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4, ___returnToCentreSpeed_7)); }
	inline float get_returnToCentreSpeed_7() const { return ___returnToCentreSpeed_7; }
	inline float* get_address_of_returnToCentreSpeed_7() { return &___returnToCentreSpeed_7; }
	inline void set_returnToCentreSpeed_7(float value)
	{
		___returnToCentreSpeed_7 = value;
	}

	inline static int32_t get_offset_of_m_PairedWith_8() { return static_cast<int32_t>(offsetof(AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4, ___m_PairedWith_8)); }
	inline AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4 * get_m_PairedWith_8() const { return ___m_PairedWith_8; }
	inline AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4 ** get_address_of_m_PairedWith_8() { return &___m_PairedWith_8; }
	inline void set_m_PairedWith_8(AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4 * value)
	{
		___m_PairedWith_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_PairedWith_8), value);
	}

	inline static int32_t get_offset_of_m_Axis_9() { return static_cast<int32_t>(offsetof(AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4, ___m_Axis_9)); }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * get_m_Axis_9() const { return ___m_Axis_9; }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B ** get_address_of_m_Axis_9() { return &___m_Axis_9; }
	inline void set_m_Axis_9(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * value)
	{
		___m_Axis_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Axis_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISTOUCHBUTTON_T124E142E5813D33A6407C920758816933327BEC4_H
#ifndef BUTTONHANDLER_TA53C84C0B2F2670630A8B3B02001C0E6F773C631_H
#define BUTTONHANDLER_TA53C84C0B2F2670630A8B3B02001C0E6F773C631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.ButtonHandler
struct  ButtonHandler_tA53C84C0B2F2670630A8B3B02001C0E6F773C631  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.ButtonHandler::Name
	String_t* ___Name_4;

public:
	inline static int32_t get_offset_of_Name_4() { return static_cast<int32_t>(offsetof(ButtonHandler_tA53C84C0B2F2670630A8B3B02001C0E6F773C631, ___Name_4)); }
	inline String_t* get_Name_4() const { return ___Name_4; }
	inline String_t** get_address_of_Name_4() { return &___Name_4; }
	inline void set_Name_4(String_t* value)
	{
		___Name_4 = value;
		Il2CppCodeGenWriteBarrier((&___Name_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONHANDLER_TA53C84C0B2F2670630A8B3B02001C0E6F773C631_H
#ifndef INPUTAXISSCROLLBAR_T935C2FEF06530E3578BBC8CF2C9871CF2BBCF7F7_H
#define INPUTAXISSCROLLBAR_T935C2FEF06530E3578BBC8CF2C9871CF2BBCF7F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
struct  InputAxisScrollbar_t935C2FEF06530E3578BBC8CF2C9871CF2BBCF7F7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::axis
	String_t* ___axis_4;

public:
	inline static int32_t get_offset_of_axis_4() { return static_cast<int32_t>(offsetof(InputAxisScrollbar_t935C2FEF06530E3578BBC8CF2C9871CF2BBCF7F7, ___axis_4)); }
	inline String_t* get_axis_4() const { return ___axis_4; }
	inline String_t** get_address_of_axis_4() { return &___axis_4; }
	inline void set_axis_4(String_t* value)
	{
		___axis_4 = value;
		Il2CppCodeGenWriteBarrier((&___axis_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTAXISSCROLLBAR_T935C2FEF06530E3578BBC8CF2C9871CF2BBCF7F7_H
#ifndef JOYSTICK_T56ABC51716722ABB9F06ADACA11FE44B502E4300_H
#define JOYSTICK_T56ABC51716722ABB9F06ADACA11FE44B502E4300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.Joystick
struct  Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 UnityStandardAssets.CrossPlatformInput.Joystick::MovementRange
	int32_t ___MovementRange_4;
	// UnityStandardAssets.CrossPlatformInput.Joystick/AxisOption UnityStandardAssets.CrossPlatformInput.Joystick::axesToUse
	int32_t ___axesToUse_5;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::horizontalAxisName
	String_t* ___horizontalAxisName_6;
	// System.String UnityStandardAssets.CrossPlatformInput.Joystick::verticalAxisName
	String_t* ___verticalAxisName_7;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.Joystick::m_StartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_StartPos_8;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseX
	bool ___m_UseX_9;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.Joystick::m_UseY
	bool ___m_UseY_10;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_HorizontalVirtualAxis
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * ___m_HorizontalVirtualAxis_11;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.Joystick::m_VerticalVirtualAxis
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * ___m_VerticalVirtualAxis_12;

public:
	inline static int32_t get_offset_of_MovementRange_4() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___MovementRange_4)); }
	inline int32_t get_MovementRange_4() const { return ___MovementRange_4; }
	inline int32_t* get_address_of_MovementRange_4() { return &___MovementRange_4; }
	inline void set_MovementRange_4(int32_t value)
	{
		___MovementRange_4 = value;
	}

	inline static int32_t get_offset_of_axesToUse_5() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___axesToUse_5)); }
	inline int32_t get_axesToUse_5() const { return ___axesToUse_5; }
	inline int32_t* get_address_of_axesToUse_5() { return &___axesToUse_5; }
	inline void set_axesToUse_5(int32_t value)
	{
		___axesToUse_5 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_6() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___horizontalAxisName_6)); }
	inline String_t* get_horizontalAxisName_6() const { return ___horizontalAxisName_6; }
	inline String_t** get_address_of_horizontalAxisName_6() { return &___horizontalAxisName_6; }
	inline void set_horizontalAxisName_6(String_t* value)
	{
		___horizontalAxisName_6 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_6), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_7() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___verticalAxisName_7)); }
	inline String_t* get_verticalAxisName_7() const { return ___verticalAxisName_7; }
	inline String_t** get_address_of_verticalAxisName_7() { return &___verticalAxisName_7; }
	inline void set_verticalAxisName_7(String_t* value)
	{
		___verticalAxisName_7 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_7), value);
	}

	inline static int32_t get_offset_of_m_StartPos_8() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___m_StartPos_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_StartPos_8() const { return ___m_StartPos_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_StartPos_8() { return &___m_StartPos_8; }
	inline void set_m_StartPos_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_StartPos_8 = value;
	}

	inline static int32_t get_offset_of_m_UseX_9() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___m_UseX_9)); }
	inline bool get_m_UseX_9() const { return ___m_UseX_9; }
	inline bool* get_address_of_m_UseX_9() { return &___m_UseX_9; }
	inline void set_m_UseX_9(bool value)
	{
		___m_UseX_9 = value;
	}

	inline static int32_t get_offset_of_m_UseY_10() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___m_UseY_10)); }
	inline bool get_m_UseY_10() const { return ___m_UseY_10; }
	inline bool* get_address_of_m_UseY_10() { return &___m_UseY_10; }
	inline void set_m_UseY_10(bool value)
	{
		___m_UseY_10 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_11() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___m_HorizontalVirtualAxis_11)); }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * get_m_HorizontalVirtualAxis_11() const { return ___m_HorizontalVirtualAxis_11; }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B ** get_address_of_m_HorizontalVirtualAxis_11() { return &___m_HorizontalVirtualAxis_11; }
	inline void set_m_HorizontalVirtualAxis_11(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * value)
	{
		___m_HorizontalVirtualAxis_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_11), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_12() { return static_cast<int32_t>(offsetof(Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300, ___m_VerticalVirtualAxis_12)); }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * get_m_VerticalVirtualAxis_12() const { return ___m_VerticalVirtualAxis_12; }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B ** get_address_of_m_VerticalVirtualAxis_12() { return &___m_VerticalVirtualAxis_12; }
	inline void set_m_VerticalVirtualAxis_12(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * value)
	{
		___m_VerticalVirtualAxis_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T56ABC51716722ABB9F06ADACA11FE44B502E4300_H
#ifndef MOBILECONTROLRIG_TD348E6259B1A94E95D09BD393ADAC945B3515418_H
#define MOBILECONTROLRIG_TD348E6259B1A94E95D09BD393ADAC945B3515418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.MobileControlRig
struct  MobileControlRig_tD348E6259B1A94E95D09BD393ADAC945B3515418  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILECONTROLRIG_TD348E6259B1A94E95D09BD393ADAC945B3515418_H
#ifndef TILTINPUT_T51C59191ECAB38039EF5E5E19B0F04983EAC3D53_H
#define TILTINPUT_T51C59191ECAB38039EF5E5E19B0F04983EAC3D53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TiltInput
struct  TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping UnityStandardAssets.CrossPlatformInput.TiltInput::mapping
	AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E * ___mapping_4;
	// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisOptions UnityStandardAssets.CrossPlatformInput.TiltInput::tiltAroundAxis
	int32_t ___tiltAroundAxis_5;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::fullTiltAngle
	float ___fullTiltAngle_6;
	// System.Single UnityStandardAssets.CrossPlatformInput.TiltInput::centreAngleOffset
	float ___centreAngleOffset_7;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TiltInput::m_SteerAxis
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * ___m_SteerAxis_8;

public:
	inline static int32_t get_offset_of_mapping_4() { return static_cast<int32_t>(offsetof(TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53, ___mapping_4)); }
	inline AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E * get_mapping_4() const { return ___mapping_4; }
	inline AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E ** get_address_of_mapping_4() { return &___mapping_4; }
	inline void set_mapping_4(AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E * value)
	{
		___mapping_4 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_4), value);
	}

	inline static int32_t get_offset_of_tiltAroundAxis_5() { return static_cast<int32_t>(offsetof(TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53, ___tiltAroundAxis_5)); }
	inline int32_t get_tiltAroundAxis_5() const { return ___tiltAroundAxis_5; }
	inline int32_t* get_address_of_tiltAroundAxis_5() { return &___tiltAroundAxis_5; }
	inline void set_tiltAroundAxis_5(int32_t value)
	{
		___tiltAroundAxis_5 = value;
	}

	inline static int32_t get_offset_of_fullTiltAngle_6() { return static_cast<int32_t>(offsetof(TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53, ___fullTiltAngle_6)); }
	inline float get_fullTiltAngle_6() const { return ___fullTiltAngle_6; }
	inline float* get_address_of_fullTiltAngle_6() { return &___fullTiltAngle_6; }
	inline void set_fullTiltAngle_6(float value)
	{
		___fullTiltAngle_6 = value;
	}

	inline static int32_t get_offset_of_centreAngleOffset_7() { return static_cast<int32_t>(offsetof(TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53, ___centreAngleOffset_7)); }
	inline float get_centreAngleOffset_7() const { return ___centreAngleOffset_7; }
	inline float* get_address_of_centreAngleOffset_7() { return &___centreAngleOffset_7; }
	inline void set_centreAngleOffset_7(float value)
	{
		___centreAngleOffset_7 = value;
	}

	inline static int32_t get_offset_of_m_SteerAxis_8() { return static_cast<int32_t>(offsetof(TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53, ___m_SteerAxis_8)); }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * get_m_SteerAxis_8() const { return ___m_SteerAxis_8; }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B ** get_address_of_m_SteerAxis_8() { return &___m_SteerAxis_8; }
	inline void set_m_SteerAxis_8(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * value)
	{
		___m_SteerAxis_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SteerAxis_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTINPUT_T51C59191ECAB38039EF5E5E19B0F04983EAC3D53_H
#ifndef TOUCHPAD_TAD361C39C471A260BFA37D565706AADF3528BC11_H
#define TOUCHPAD_TAD361C39C471A260BFA37D565706AADF3528BC11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CrossPlatformInput.TouchPad
struct  TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.CrossPlatformInput.TouchPad/AxisOption UnityStandardAssets.CrossPlatformInput.TouchPad::axesToUse
	int32_t ___axesToUse_4;
	// UnityStandardAssets.CrossPlatformInput.TouchPad/ControlStyle UnityStandardAssets.CrossPlatformInput.TouchPad::controlStyle
	int32_t ___controlStyle_5;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::horizontalAxisName
	String_t* ___horizontalAxisName_6;
	// System.String UnityStandardAssets.CrossPlatformInput.TouchPad::verticalAxisName
	String_t* ___verticalAxisName_7;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Xsensitivity
	float ___Xsensitivity_8;
	// System.Single UnityStandardAssets.CrossPlatformInput.TouchPad::Ysensitivity
	float ___Ysensitivity_9;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_StartPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_StartPos_10;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousDelta
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PreviousDelta_11;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_JoytickOutput
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_JoytickOutput_12;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseX
	bool ___m_UseX_13;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_UseY
	bool ___m_UseY_14;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_HorizontalVirtualAxis
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * ___m_HorizontalVirtualAxis_15;
	// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.TouchPad::m_VerticalVirtualAxis
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * ___m_VerticalVirtualAxis_16;
	// System.Boolean UnityStandardAssets.CrossPlatformInput.TouchPad::m_Dragging
	bool ___m_Dragging_17;
	// System.Int32 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Id
	int32_t ___m_Id_18;
	// UnityEngine.Vector2 UnityStandardAssets.CrossPlatformInput.TouchPad::m_PreviousTouchPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PreviousTouchPos_19;
	// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.TouchPad::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_20;
	// UnityEngine.UI.Image UnityStandardAssets.CrossPlatformInput.TouchPad::m_Image
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___m_Image_21;

public:
	inline static int32_t get_offset_of_axesToUse_4() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___axesToUse_4)); }
	inline int32_t get_axesToUse_4() const { return ___axesToUse_4; }
	inline int32_t* get_address_of_axesToUse_4() { return &___axesToUse_4; }
	inline void set_axesToUse_4(int32_t value)
	{
		___axesToUse_4 = value;
	}

	inline static int32_t get_offset_of_controlStyle_5() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___controlStyle_5)); }
	inline int32_t get_controlStyle_5() const { return ___controlStyle_5; }
	inline int32_t* get_address_of_controlStyle_5() { return &___controlStyle_5; }
	inline void set_controlStyle_5(int32_t value)
	{
		___controlStyle_5 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_6() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___horizontalAxisName_6)); }
	inline String_t* get_horizontalAxisName_6() const { return ___horizontalAxisName_6; }
	inline String_t** get_address_of_horizontalAxisName_6() { return &___horizontalAxisName_6; }
	inline void set_horizontalAxisName_6(String_t* value)
	{
		___horizontalAxisName_6 = value;
		Il2CppCodeGenWriteBarrier((&___horizontalAxisName_6), value);
	}

	inline static int32_t get_offset_of_verticalAxisName_7() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___verticalAxisName_7)); }
	inline String_t* get_verticalAxisName_7() const { return ___verticalAxisName_7; }
	inline String_t** get_address_of_verticalAxisName_7() { return &___verticalAxisName_7; }
	inline void set_verticalAxisName_7(String_t* value)
	{
		___verticalAxisName_7 = value;
		Il2CppCodeGenWriteBarrier((&___verticalAxisName_7), value);
	}

	inline static int32_t get_offset_of_Xsensitivity_8() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___Xsensitivity_8)); }
	inline float get_Xsensitivity_8() const { return ___Xsensitivity_8; }
	inline float* get_address_of_Xsensitivity_8() { return &___Xsensitivity_8; }
	inline void set_Xsensitivity_8(float value)
	{
		___Xsensitivity_8 = value;
	}

	inline static int32_t get_offset_of_Ysensitivity_9() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___Ysensitivity_9)); }
	inline float get_Ysensitivity_9() const { return ___Ysensitivity_9; }
	inline float* get_address_of_Ysensitivity_9() { return &___Ysensitivity_9; }
	inline void set_Ysensitivity_9(float value)
	{
		___Ysensitivity_9 = value;
	}

	inline static int32_t get_offset_of_m_StartPos_10() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_StartPos_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_StartPos_10() const { return ___m_StartPos_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_StartPos_10() { return &___m_StartPos_10; }
	inline void set_m_StartPos_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_StartPos_10 = value;
	}

	inline static int32_t get_offset_of_m_PreviousDelta_11() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_PreviousDelta_11)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PreviousDelta_11() const { return ___m_PreviousDelta_11; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PreviousDelta_11() { return &___m_PreviousDelta_11; }
	inline void set_m_PreviousDelta_11(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PreviousDelta_11 = value;
	}

	inline static int32_t get_offset_of_m_JoytickOutput_12() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_JoytickOutput_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_JoytickOutput_12() const { return ___m_JoytickOutput_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_JoytickOutput_12() { return &___m_JoytickOutput_12; }
	inline void set_m_JoytickOutput_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_JoytickOutput_12 = value;
	}

	inline static int32_t get_offset_of_m_UseX_13() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_UseX_13)); }
	inline bool get_m_UseX_13() const { return ___m_UseX_13; }
	inline bool* get_address_of_m_UseX_13() { return &___m_UseX_13; }
	inline void set_m_UseX_13(bool value)
	{
		___m_UseX_13 = value;
	}

	inline static int32_t get_offset_of_m_UseY_14() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_UseY_14)); }
	inline bool get_m_UseY_14() const { return ___m_UseY_14; }
	inline bool* get_address_of_m_UseY_14() { return &___m_UseY_14; }
	inline void set_m_UseY_14(bool value)
	{
		___m_UseY_14 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalVirtualAxis_15() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_HorizontalVirtualAxis_15)); }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * get_m_HorizontalVirtualAxis_15() const { return ___m_HorizontalVirtualAxis_15; }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B ** get_address_of_m_HorizontalVirtualAxis_15() { return &___m_HorizontalVirtualAxis_15; }
	inline void set_m_HorizontalVirtualAxis_15(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * value)
	{
		___m_HorizontalVirtualAxis_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalVirtualAxis_15), value);
	}

	inline static int32_t get_offset_of_m_VerticalVirtualAxis_16() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_VerticalVirtualAxis_16)); }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * get_m_VerticalVirtualAxis_16() const { return ___m_VerticalVirtualAxis_16; }
	inline VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B ** get_address_of_m_VerticalVirtualAxis_16() { return &___m_VerticalVirtualAxis_16; }
	inline void set_m_VerticalVirtualAxis_16(VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B * value)
	{
		___m_VerticalVirtualAxis_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalVirtualAxis_16), value);
	}

	inline static int32_t get_offset_of_m_Dragging_17() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_Dragging_17)); }
	inline bool get_m_Dragging_17() const { return ___m_Dragging_17; }
	inline bool* get_address_of_m_Dragging_17() { return &___m_Dragging_17; }
	inline void set_m_Dragging_17(bool value)
	{
		___m_Dragging_17 = value;
	}

	inline static int32_t get_offset_of_m_Id_18() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_Id_18)); }
	inline int32_t get_m_Id_18() const { return ___m_Id_18; }
	inline int32_t* get_address_of_m_Id_18() { return &___m_Id_18; }
	inline void set_m_Id_18(int32_t value)
	{
		___m_Id_18 = value;
	}

	inline static int32_t get_offset_of_m_PreviousTouchPos_19() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_PreviousTouchPos_19)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PreviousTouchPos_19() const { return ___m_PreviousTouchPos_19; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PreviousTouchPos_19() { return &___m_PreviousTouchPos_19; }
	inline void set_m_PreviousTouchPos_19(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PreviousTouchPos_19 = value;
	}

	inline static int32_t get_offset_of_m_Center_20() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_Center_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_20() const { return ___m_Center_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_20() { return &___m_Center_20; }
	inline void set_m_Center_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_20 = value;
	}

	inline static int32_t get_offset_of_m_Image_21() { return static_cast<int32_t>(offsetof(TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11, ___m_Image_21)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_m_Image_21() const { return ___m_Image_21; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_m_Image_21() { return &___m_Image_21; }
	inline void set_m_Image_21(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___m_Image_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPAD_TAD361C39C471A260BFA37D565706AADF3528BC11_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2800[7] = 
{
	U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB::get_offset_of_U3CU3E1__state_0(),
	U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB::get_offset_of_U3CU3E2__current_1(),
	U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB::get_offset_of_U3CU3E4__this_2(),
	U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB::get_offset_of_U3CcurrentColorU3E5__2_3(),
	U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB::get_offset_of_U3CinvisibleColorU3E5__3_4(),
	U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB::get_offset_of_U3CcounterU3E5__4_5(),
	U3CanimnonstopU3Ed__8_t4E0A78064846E9A9854908125617F1A04908FABB::get_offset_of_U3CtU3E5__5_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2801[12] = 
{
	GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83::get_offset_of_displacementMap_4(),
	GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83::get_offset_of_Shader_5(),
	GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83::get_offset_of_intensity_6(),
	GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83::get_offset_of_flipIntensity_7(),
	GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83::get_offset_of_colorIntensity_8(),
	GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83::get_offset_of__glitchup_9(),
	GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83::get_offset_of__glitchdown_10(),
	GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83::get_offset_of_flicker_11(),
	GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83::get_offset_of__glitchupTime_12(),
	GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83::get_offset_of__glitchdownTime_13(),
	GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83::get_offset_of__flickerTime_14(),
	GlitchEffect_t56F0423B1561DC48494DA08642884B0DAC895C83::get_offset_of__material_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (DigitalGlitch_t9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2802[6] = 
{
	DigitalGlitch_t9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5::get_offset_of__intensity_4(),
	DigitalGlitch_t9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5::get_offset_of__shader_5(),
	DigitalGlitch_t9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5::get_offset_of__material_6(),
	DigitalGlitch_t9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5::get_offset_of__noiseTexture_7(),
	DigitalGlitch_t9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5::get_offset_of__trashFrame1_8(),
	DigitalGlitch_t9CCFC99B1C500EAFB55948BF9F69A6BB34315EA5::get_offset_of__trashFrame2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (ConstantMotion_t9BC470E76522142165F54735A47CE184FE38C640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2803[3] = 
{
	ConstantMotion_t9BC470E76522142165F54735A47CE184FE38C640::get_offset_of_position_4(),
	ConstantMotion_t9BC470E76522142165F54735A47CE184FE38C640::get_offset_of_rotation_5(),
	ConstantMotion_t9BC470E76522142165F54735A47CE184FE38C640::get_offset_of_useLocalCoordinate_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (TransformMode_t9211BB6207A27934EDC5BCF647487CDD7B17F796)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2804[7] = 
{
	TransformMode_t9211BB6207A27934EDC5BCF647487CDD7B17F796::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2805[6] = 
{
	TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC::get_offset_of_mode_0(),
	TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC::get_offset_of_velocity_1(),
	TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC::get_offset_of_arbitraryVector_2(),
	TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC::get_offset_of_randomness_3(),
	TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC::get_offset_of_randomVector_4(),
	TransformElement_tEA1EC4D96832D8C69E0F7612210ACF741D2D01FC::get_offset_of_randomScalar_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2806[13] = 
{
	JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A::get_offset_of_positionFrequency_4(),
	JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A::get_offset_of_rotationFrequency_5(),
	JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A::get_offset_of_positionAmount_6(),
	JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A::get_offset_of_rotationAmount_7(),
	JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A::get_offset_of_positionComponents_8(),
	JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A::get_offset_of_rotationComponents_9(),
	JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A::get_offset_of_positionOctave_10(),
	JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A::get_offset_of_rotationOctave_11(),
	JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A::get_offset_of_timePosition_12(),
	JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A::get_offset_of_timeRotation_13(),
	JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A::get_offset_of_noiseVectors_14(),
	JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A::get_offset_of_initialPosition_15(),
	JitterMotion_t61F410943C49D9F8E4244D58A34A85DBCA17EB0A::get_offset_of_initialRotation_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (U3CModuleU3E_tF157A75827DFDE1F9E89CA3CBB54B07FA9E227FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2808[6] = 
{
	AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4::get_offset_of_axisName_4(),
	AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4::get_offset_of_axisValue_5(),
	AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4::get_offset_of_responseSpeed_6(),
	AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4::get_offset_of_returnToCentreSpeed_7(),
	AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4::get_offset_of_m_PairedWith_8(),
	AxisTouchButton_t124E142E5813D33A6407C920758816933327BEC4::get_offset_of_m_Axis_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (ButtonHandler_tA53C84C0B2F2670630A8B3B02001C0E6F773C631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[1] = 
{
	ButtonHandler_tA53C84C0B2F2670630A8B3B02001C0E6F773C631::get_offset_of_Name_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E), -1, sizeof(CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2810[3] = 
{
	CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields::get_offset_of_activeInput_0(),
	CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields::get_offset_of_s_TouchInput_1(),
	CrossPlatformInputManager_t538BC5CB3009F635C0D8B708E339CE82E4C6152E_StaticFields::get_offset_of_s_HardwareInput_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (ActiveInputMethod_t809D9C23DD316DBC93963EAF0FAF0FA63CBF8C53)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2811[3] = 
{
	ActiveInputMethod_t809D9C23DD316DBC93963EAF0FAF0FA63CBF8C53::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2812[3] = 
{
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B::get_offset_of_m_Value_1(),
	VirtualAxis_tE5A7DABB0F0E4877C97C3C45615CCDE552A02A1B::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[5] = 
{
	VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA::get_offset_of_U3CnameU3Ek__BackingField_0(),
	VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA::get_offset_of_U3CmatchWithInputManagerU3Ek__BackingField_1(),
	VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA::get_offset_of_m_LastPressedFrame_2(),
	VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA::get_offset_of_m_ReleasedFrame_3(),
	VirtualButton_tD2E44C8028D8B58E3AA9D65D2F45CF8518022EEA::get_offset_of_m_Pressed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (InputAxisScrollbar_t935C2FEF06530E3578BBC8CF2C9871CF2BBCF7F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[1] = 
{
	InputAxisScrollbar_t935C2FEF06530E3578BBC8CF2C9871CF2BBCF7F7::get_offset_of_axis_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2815[9] = 
{
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_MovementRange_4(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_axesToUse_5(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_horizontalAxisName_6(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_verticalAxisName_7(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_m_StartPos_8(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_m_UseX_9(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_m_UseY_10(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_m_HorizontalVirtualAxis_11(),
	Joystick_t56ABC51716722ABB9F06ADACA11FE44B502E4300::get_offset_of_m_VerticalVirtualAxis_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (AxisOption_t2E4FAD7B3F55CE394680B6CEA9C2F01B92849786)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2816[4] = 
{
	AxisOption_t2E4FAD7B3F55CE394680B6CEA9C2F01B92849786::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (MobileControlRig_tD348E6259B1A94E95D09BD393ADAC945B3515418), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2818[5] = 
{
	TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53::get_offset_of_mapping_4(),
	TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53::get_offset_of_tiltAroundAxis_5(),
	TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53::get_offset_of_fullTiltAngle_6(),
	TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53::get_offset_of_centreAngleOffset_7(),
	TiltInput_t51C59191ECAB38039EF5E5E19B0F04983EAC3D53::get_offset_of_m_SteerAxis_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (AxisOptions_t7D42CBC0AAF7DFE9DF7335044264BAF17BA6CC71)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2819[3] = 
{
	AxisOptions_t7D42CBC0AAF7DFE9DF7335044264BAF17BA6CC71::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2820[2] = 
{
	AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E::get_offset_of_type_0(),
	AxisMapping_t9B9CB140F322262AC1E126846B4BD1C92586923E::get_offset_of_axisName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (MappingType_tA5DC189BC7DB7B5AE049E710D502B88A32BA87B3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2821[5] = 
{
	MappingType_tA5DC189BC7DB7B5AE049E710D502B88A32BA87B3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2822[18] = 
{
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_axesToUse_4(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_controlStyle_5(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_horizontalAxisName_6(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_verticalAxisName_7(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_Xsensitivity_8(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_Ysensitivity_9(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_StartPos_10(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_PreviousDelta_11(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_JoytickOutput_12(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_UseX_13(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_UseY_14(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_HorizontalVirtualAxis_15(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_VerticalVirtualAxis_16(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_Dragging_17(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_Id_18(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_PreviousTouchPos_19(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_Center_20(),
	TouchPad_tAD361C39C471A260BFA37D565706AADF3528BC11::get_offset_of_m_Image_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (AxisOption_tA48A0481101792CB4679A6C5D81F14DB1582FF11)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2823[4] = 
{
	AxisOption_tA48A0481101792CB4679A6C5D81F14DB1582FF11::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (ControlStyle_t60A34C5903F112F7E6982BB8161F9F7372669CAC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2824[4] = 
{
	ControlStyle_t60A34C5903F112F7E6982BB8161F9F7372669CAC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2825[4] = 
{
	VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39::get_offset_of_U3CvirtualMousePositionU3Ek__BackingField_0(),
	VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39::get_offset_of_m_VirtualAxes_1(),
	VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39::get_offset_of_m_VirtualButtons_2(),
	VirtualInput_tCCB9F78E94F5E3C6E5162E7AA746292D827BAF39::get_offset_of_m_AlwaysUseVirtual_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (MobileInput_t219DDF915FF54DBE73004E0163F8BFCB4DA3BA44), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (StandaloneInput_t9BFD53669697FC88D0D237110D8D1DF76400AFEB), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
